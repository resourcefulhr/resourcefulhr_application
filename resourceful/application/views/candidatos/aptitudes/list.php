<section class="content">
    <h1>
        Aptitudes
        <small>Poner el nombre del cv que le queres modificar su aptitud</small>
    </h1>
    <div class="card">
        <div class="body">
            <div class="input-group">
                <a href="#" class="btn btn-success waves-effect"><i class=material-icons>add_box
                    </i> Agregar Aptitudes</a>
            </div>
            <hr>
            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th>Modificar</th>
                            <th>Borrar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($Aptitudes)) : ?>
                            <?php foreach ($Aptitudes as $Aptitud) : ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <div class="btn-gruop">
                                            <button type="button" id="" class="btn btn-info waves-effect" data-toggle="modal" data-target="#defaultModal" value="">
                                                <i class=material-icons>search</i>
                                            </button>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-danger btn-remove waves-effect"><i class=material-icons>delete_forever</i></a>
                                        </div>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>