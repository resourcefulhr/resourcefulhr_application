<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CalendarioFeriados_model extends CI_Model
{
    public function GetFeriados()
    {
        $this->db->select("*");
        $this->db->from("feriados");
        $resultado = $this->db->get();
        return $resultado->result();
    }

    public function isFeriado($fecha)
    {
        $feriado = $this->db->select("*")
                        ->from("feriados")
                        ->where("desde >= '$fecha'")
                        ->where("hasta <= '$fecha'")
                        ->get()
                        ->row();
        return $feriado ? true : false;
    }

    public function EliminarFeriado($id) {
        return $this->db->delete('feriados', array('id' => $id));
    }

    public function EditarFeriado($id, $nombre, $desde, $hasta)
    {
        $info = array(
            "nombre" => $nombre,
            "desde" => DateTime::createFromFormat('Y-m-d', $desde)->format('Y-m-d'),
            "hasta" => DateTime::createFromFormat('Y-m-d', $hasta)->format('Y-m-d')
        );
        $this->db->where("feriados.id", $id);
        return $this->db->update("feriados", $info);
    }

    public function GuardarFeriado($nombre, $desde, $hasta)
    {
        $info = array(
            "nombre" => $nombre,
            "desde" => DateTime::createFromFormat('Y-m-d', $desde)->format('Y-m-d'),
            "hasta" => DateTime::createFromFormat('Y-m-d', $hasta)->format('Y-m-d')
        );
        return $this->db->insert('feriados', $info);
    }
}