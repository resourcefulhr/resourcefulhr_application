<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organigrama_model extends CI_Model
{
    public function GetAreas()
    {
        $this->load->model("Areas_model");
        return $this->Areas_model->getAreas();
    }

    public function GetLegajosPorArea($idArea)
    {       
        $this->db->select("id")
        ->from("cargos")
        ->where("id_area", $idArea);
        $cargos = $this->db->get_compiled_select();
        
        $this->db->select("l.num_legajo, l.nombre, l.apellido, l.id_cargo, c.cargo")
        ->from("legajos l")
        ->where("l.estado", "1")
        ->where("l.id_cargo in(" . $cargos . ")")
        ->join("cargos c", "l.id_cargo = c.id")
        ->order_by('l.id_cargo', 'ASC');
        
        $resultado = $this->db->get();
        return $resultado->result();
    }

    public function GetLegajo($numLegajo)
    {
        $this->db->select("*")
        ->from("legajos l")
        ->where("l.estado", "1")
        ->where("l.num_legajo", $numLegajo);
        
        $resultado = $this->db->get();
        return $resultado->row();
    }
}
