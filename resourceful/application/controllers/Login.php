<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent:: __construct();
        $this->load->model("login_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            redirect(base_url()."control/Novedades");
        } else {
            $this->load->view('login');
        }
    }

    public function login()
    {
        $usuario = $this->input->post("usuario");
        $contraseña = $this->input->post("contraseña");

        $loginData = $this->login_model->login($usuario, $contraseña);

        $this->form_validation->set_rules("usuario", "Nombre de Usuario", "required");
        $this->form_validation->set_rules("contraseña", "Contraseña", "required");

        if ($this->form_validation->run()) {
            if(isset($loginData->error)) {
                $this->session->set_flashdata("data_invalida", $loginData->error);
                $this->index();
            } else {
                $data = array(
                    "id" => $loginData->usuario->id,
                    "nombre" => $loginData->usuario->usuario,
                    "permisos_leer" => $loginData->permisos_leer,
                    "permisos_modificar" => $loginData->permisos_modificar,
                    "permisos_insertar" => $loginData->permisos_insertar,
                    "permisos_baja" => $loginData->permisos_baja,
                    "login" => TRUE
                );
                $this->session->set_userdata($data);
                redirect(base_url()."control/Novedades");
            }
        } else {
            // Recargo la pagina, los errores de validation de campo seran cargados
            // automaticamente en session->flashdata
            $this->index();
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->load->view('login');
    }

    public function signup()
    {
        if ($this->session->userdata('login')) {
            redirect(base_url()."control/Novedades");
        } else {
            $this->load->view('sign-up');
        }
    }

    public function signup_store()
    {
        $usuario = $this->input->POST("usuario");
        $this->form_validation->set_rules("usuario", "usuario", "is_unique[usuarios.usuario]");
        if ($this->form_validation->run()) {
            $contraseña = $this->input->post('password');
            $pregunta_recuperacion = $this->input->post('pregunta');
            $respuesta_recuperacion = $this->input->post('respuesta');


            if ($this->login_model->StoreUsuario($usuario, $contraseña, $pregunta_recuperacion, $respuesta_recuperacion)) {
                $this->load->view('login');
            } else {
                $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                $this->load->view('sign-up');
            }
        } else {
            $this->load->view('sign-up');
        }
    }

    public function CambiarPass()
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $idUsuario = $this->session->userdata("id");
        $password = $this->input->post("password");

        $this->login_model->CambiarPass($idUsuario, $password);
        $this->logout();
    }

    public function recover()
    {
        if ($this->session->userdata('login')) {
            redirect(base_url()."control/Novedades");
        } else {
            $this->load->view('recover-password');
        }
    }

    public function GetPregunta()
    {
        $usuario = $this->input->post("usuario");

        $pregunta = $this->login_model->GetPregunta($usuario);

        if ($pregunta != NULL) {
            echo $pregunta->pregunta_recuperacion;
        }
    }

    public function RecuperarPass()
    {
        $usuario = $this->input->post("usuario");
        $respuesta = $this->input->post("respuesta");
        $contraseña = $this->input->post("password");

        echo $this->login_model->RecuperarPass($usuario, $respuesta, $contraseña);
    }
}
