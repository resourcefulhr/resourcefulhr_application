<section class="content">
    <h1>
        Aptitudes
        <small>Legajo</small>
    </h1>
    <form class="form-line" action="<?php echo base_url(); ?>reclutamiento/aptitudes/storeAptitudLegajo" method="POST">
        <input type="hidden" id="idlegajo" name="idlegajo" class="form-control" value="<?php echo $idlegajo; ?>">
        <div class="card">
            <div class="body">
                <table id="tabla_aptitudes" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th class="col-md-1">Cancelar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th class="col-md-1">#</th>
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($aptitudes)) : ?>
                            <?php foreach ($aptitudes as $aptitud) : ?>
                                <tr>
                                    <td><?php echo $aptitud->id; ?></td>
                                    <td>
                                        <?php echo $aptitud->aptitud; ?>
                                        <input type="hidden" id="aptitud<?php echo $aptitud->id; ?>" class="form-control" value="<?php echo $aptitud->aptitud; ?>">
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <select name="nivel<?php echo $aptitud->id; ?>" id="nivel<?php echo $aptitud->id; ?>" class="form-control">
                                                <option value="1">Basico</option>
                                                <option value="2">Normal</option>
                                                <option value="3">Avanzado</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <button id="btn-agregar-aptitud" type="button" class="btn btn-success waves-effect pull-right" onclick="cargarApt(`<?php echo $aptitud->id; ?>`);"><i class=material-icons>check
                                                </i>Agregar</button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
            <a href="<?php echo base_url(); ?>areas/legajo" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                </i> Cancelar</a>
        </div>
    </form>
</section>
<script>
    var apt_agregadas = Array();

    function cargarApt(idApt) {
        let apt = {
            id: idApt,
            aptitud: $("#aptitud" + idApt).val(),
            nivel: $("#nivel" + idApt).val(),
        }
        var agregada = false;
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idApt) {
                agregada = true;
                break;
            }
        }
        if (!agregada) {
            apt_agregadas.push(apt);
            regenerarTabla();
        } else {
            alert("La aptitud ya esta cargada");
        }
    }

    function regenerarTabla() {
        var html = new String();
        for (let i = 0; i < apt_agregadas.length; i++) {
            let cargada = apt_agregadas[i];
            let lvl = (cargada.nivel == 1 ? 'Basico' : (cargada.nivel == 2 ? 'Normal' : 'Avanzado'))
            html += "<tr>";
            html += "<td><input type='hidden' name='aptitud_car[]' value='" + cargada.id + "'>" +
                cargada.aptitud + "</td>";
            html += "<td><input type='hidden' name='nivel_car[]' value='" + cargada.nivel + "'>" +
                lvl + "</td>";
            html +=
                "<td><button type='button' class='btn btn-danger' onclick='deleteAptitud(" + cargada.id + ");' ><i class=material-icons>delete_forever</i></button></td>";
            html += "</tr>";
        }
        let tabla = document.getElementById("tabla_aptitudes");
        tabla.tBodies[0].innerHTML = html;
    }

    function deleteAptitud(idDelete) {
        let flag = (-1);
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idDelete) {
                flag = i;
                break;
            }
        }
        if (flag !== -1) {
            apt_agregadas.splice(flag, 1);
        }
        regenerarTabla();
    }
</script>