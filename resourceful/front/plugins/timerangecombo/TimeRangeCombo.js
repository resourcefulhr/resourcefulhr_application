class TimeRangeCombo {
    constructor(startSelectId, endSelectId, initialStartTime, initialEndTime) {
        var start = startSelectId;
        var end = endSelectId;

        $('#' + start).append(new Option('Seleccione...', ''));
        // Start time select initialization
        for (let i = 0; i < 24; i++) {
            let hour = (i < 10) ? '0' + i + ':00' : i + ':00';
            let hourAndAHalf = (i < 10) ? '0' + i + ':30' : i + ':30';
            $('#' + start).append(new Option(hour, hour));
            $('#' + start).append(new Option(hourAndAHalf, hourAndAHalf));
        }
        $('#' + start).selectpicker('refresh');

        // Hook to update the end time select when the start select changes
        $('#' + start).change(function() {
            let startDate = new Date(1970, 0, 1, $('#' + start).val().split(':')[0], $('#' + start).val().split(':')[1]);
            if ($('#' + end).val()) {
                // If there is a time selected in the end select we need to retain it to set it back later, but only
                // if the start time select was dialed back and not forward!!
                let endDate = new Date(1970, 0, 1, $('#' + end).val().split(':')[0], $('#' + end).val().split(':')[1]);
                let retainTime = false;
                if (startDate.getTime() < endDate.getTime()) {
                    retainTime = true;
                }

                // Set times for the end time select
                setSelectTimes(startDate, end);

                // If the start select was dialed back, then we have a retained time to set as end time.
                if (retainTime) {
                    $('#' + end).val(getFormattedHours(endDate) + ":" + getFormattedMinutes(endDate)).change();
                }
            } else {
                // The end time select is empty. Intialize it.
                setSelectTimes(startDate, end);
            }
        });

        var setSelectTimes = function(startDate, selectId) {
            $('#' + selectId).html('');
            while (startDate.getDate() == '1') {
                // Increment the time in 30 minutes each step
                startDate.setMinutes(startDate.getMinutes() + 30);

                // Format the hours and minutes
                let hour = getFormattedHours(startDate);
                let minutes = getFormattedMinutes(startDate);

                if (hour == '00' && minutes == '00') {
                    hour = '23';
                    minutes = '59';
                }
                // Add new values to the end
                $('#' + selectId).append(new Option(hour + ":" + minutes, hour + ":" + minutes));
            }
            $('#' + selectId).selectpicker('refresh');
        }

        var getFormattedHours = function(date) {
            return (date.getHours() < 10) ? ('0' + date.getHours()) : date.getHours();
        }

        var getFormattedMinutes = function(date) {
            return (date.getMinutes() < 10) ? ('0' + date.getMinutes()) : date.getMinutes();
        }

        if (initialStartTime) {
            $('#' + start).val(initialStartTime).change();
            $('#' + end).val(initialEndTime).change();
        }
    }
}