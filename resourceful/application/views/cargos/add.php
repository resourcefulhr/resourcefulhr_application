<section class="content">
    <h1>
        Cargos
        <small>Nuevo</small>
    </h1>
    <form class="form-line" action="<?php echo base_url(); ?>areas/Cargos/Store" method="POST">
        <div class="card">
            <div class="body">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('cargo') == true ? ' error' : '' ?>">
                        <label>Cargo:</label>
                        <input type="text" name="cargo" class="form-control" required value="<?php echo set_value("cargo"); ?>">
                    </div>
                    <?php echo form_error("cargo", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <label>Descripcion:</label>
                    <div class="form-line focused">
                        <input type="text" name="descripcion" class="form-control" required value="<?php echo set_value("descripcion"); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Tareas:</label>
                    <div class="form-line focused">
                        <input type="text" name="tareas" class="form-control" required value="<?php echo set_value("tareas"); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Area:</label>
                    <select name="area" id="area" class="form-control">
                        <option value="">Seleccione...</option>
                        <?php foreach ($areas as $area) : ?>
                            <option value="<?php echo $area->id ?>" <?php echo set_select("area", $area->id) ?>>
                                <?php echo $area->nombre_area; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_error("nombre", "<span class='help-block'>", "</span>"); ?>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div>
                    <h2>APTITUDES</h2>
                    <hr>
                </div>

                <table id="tabla_aptitudes" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th class="col-md-2">Excluyente</th>
                            <th class="col-md-1">Cancelar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <br>
                <hr><br>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th class="col-md-1">#</th>
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th class="col-md-2">Excluyente</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($aptitudes)) : ?>
                            <?php foreach ($aptitudes as $aptitud) : ?>
                                <tr>
                                    <td><?php echo $aptitud->id; ?></td>
                                    <td>
                                        <?php echo $aptitud->aptitud; ?>
                                        <input type="hidden" id="aptitud<?php echo $aptitud->id; ?>" class="form-control" value="<?php echo $aptitud->aptitud; ?>">
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <select name="nivel<?php echo $aptitud->id; ?>" id="nivel<?php echo $aptitud->id; ?>" class="form-control">
                                                <option value="0">Basico</option>
                                                <option value="1">Normal</option>
                                                <option value="2">Avanzado</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <select name="excluyente<?php echo $aptitud->id; ?>" id="excluyente<?php echo $aptitud->id; ?>" class="form-control">
                                                <option value="0">NO</option>
                                                <option value="1">SI</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <button id="btn-agregar-aptitud" type="button" class="btn btn-success waves-effect pull-right" onclick="cargarApt(`<?php echo $aptitud->id; ?>`);"><i class=material-icons>check
                                                </i>Agregar</button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
            <a href="<?php echo base_url(); ?>areas/cargos" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                </i> Cancelar</a>
        </div>
    </form>
</section>
<script>
    var apt_agregadas = Array();

    function cargarApt(idApt) {
        let apt = {
            id: idApt,
            aptitud: $("#aptitud" + idApt).val(),
            nivel: $("#nivel" + idApt).val(),
            excluyente: $("#excluyente" + idApt).val()
        }
        var agregada = false;
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idApt) {
                agregada = true;
                break;
            }
        }
        if (!agregada) {
            apt_agregadas.push(apt);
            regenerarTabla();
        } else {
            alert("No se agregara la aptitud porque ya esta en el listado")
        }
    }

    function regenerarTabla() {
        var html = new String();
        for (let i = 0; i < apt_agregadas.length; i++) {
            let cargada = apt_agregadas[i];
            let exclu = (cargada.excluyente == 1 ? 'SI' : 'NO');
            let lvl = (cargada.nivel == 0 ? 'Basico' : (cargada.nivel == 1 ? 'Normal' : 'Avanzado'))
            html += "<tr>";
            html += "<td><input type='hidden' name='aptitud_car[]' value='" + cargada.id + "'>" +
                cargada.aptitud + "</td>";
            html += "<td><input type='hidden' name='nivel_car[]' value='" + cargada.nivel + "'>" +
                lvl + "</td>";
            html += "<td><input type='hidden' name='excluyente_car[]' value='" + cargada.excluyente + "'>" +
                exclu +
                "</td>";
            html +=
                "<td><button type='button' class='btn btn-danger' onclick='deleteAptitud(" + cargada.id + ");' ><i class=material-icons>delete_forever</i></button></td>";
            html += "</tr>";
        }
        let tabla = document.getElementById("tabla_aptitudes");
        tabla.tBodies[0].innerHTML = html;
    }

    function deleteAptitud(idDelete) {
        let flag=(-1);
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idDelete) {
                flag = i;
                break;
            }
        }
        if (flag !== -1) {
            apt_agregadas.splice(flag, 1);
        }
        regenerarTabla();
    }
</script>