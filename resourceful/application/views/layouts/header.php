<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Resourceful</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>front/images/favicon.png" type="image/png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>front/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>front/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>front/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>front/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(); ?>front/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Morris ccs lib para generar graficos -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/plugins/morris.js-0.5.1/morris.css">

    <!-- boostrap picker y swwtalert -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/plugins/bootstrap-datepicker-1.8.0/bootstrap-datepicker.standalone.min.css" />
    <link href="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.css" rel="stylesheet">

    <!---------------------------------------------------SCRIPTS------------------------------------------------------------------------>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Morris ccs lib para generar graficos -->
    <script src="<?php echo base_url(); ?>front/plugins/morris.js-0.5.1/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>front/plugins/morris.js-0.5.1/morris.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery print pdf -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-print/jQuery.print.min.js"></script>
    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/node-waves/waves.js"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>front/js/admin.js"></script>
    <!-- Datatables  -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>front/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- boostrap picker y swwtalert -->
    <script src="<?php echo base_url(); ?>front/plugins/bootstrap-datepicker-1.8.0/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.min.js"></script>
</head>

<body class="theme-light-blue">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="">Resourceful - Aplication</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a type="button" title="Ayuda en linea/ Manual de usuario" href="<?php echo base_url();?>UserManual/ResourcefulHR.html" target="_blank"><i class="material-icons">help</i></a></li>
                </ul>
            </div>
        </div>
    </nav>