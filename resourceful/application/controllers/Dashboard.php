<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->helper('download');
        $this->load->library('zip');
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('body');
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function generateBackupBD()
    {
        $this->load->dbutil();
        $db_format = array('format' => 'zip', 'filename' => 'resurceful_BACKUP.sql');
        $backup = &$this->dbutil->backup($db_format);
        $dbname = 'backup-on-' . date('Y-m-d') . '.zip';
        $save = 'archivos/' . $dbname;
        force_download($dbname, $backup);
    }
}
