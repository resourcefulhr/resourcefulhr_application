<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Novedades extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $ayer = new DateTime("today");
        $un_dia = new DateInterval('P1D');
        $ayer->sub($un_dia);

        $info = array(
            "desde" => $this->Novedades_model->GetUltimaGeneracion()->fecha,
            "hasta" => $ayer->format('Y-m-d'),
            "legajos" => $this->Novedades_model->GetLegajos()
        );

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        if (in_array("Control -> Novedades", $this->session->userdata("permisos_leer"))) {
            $this->load->view('control/novedades/dashboard', $info);
        }
        $this->load->view('layouts/footer');
    }

    public function ModalCerrarNovedad()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $id = $this->input->GET("id");

        $novedad = $this->Novedades_model->GetNovedad($id);
        $info = array(
            "novedad" => $novedad,
            "registros" => $this->Novedades_model->GetRegistros($id),
            "historial" => $this->Novedades_model->GetNovedades(NULL, NULL, array($novedad->num_legajo), NULL, NULL)
        );

        $this->load->view("control/novedades/cerrar-novedad", $info);
    }

    public function ModalInfoNovedad()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $id = $this->input->GET("id");

        $novedad = $this->Novedades_model->GetNovedad($id);
        $info = array(
            "novedad" => $novedad,
            "registros" => $this->Novedades_model->GetRegistros($id),
        );

        $this->load->view("control/novedades/info-novedad", $info);
    }

    public function ReporteNovedades()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $tipo = json_decode($this->input->POST("tipo"));
        $estado = json_decode($this->input->POST("estado"));
        $legajos = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $info = array(
            "info" => $this->Novedades_model->GetDatosReporte($tipo, $estado, $legajos, $desde, $hasta),
            "desde" => $desde,
            "hasta" => $hasta,
            "tipo" => $tipo,
            "estado" => $estado
        );

        if (sizeOf($info['info']) > 0) {
            $this->load->view("control/novedades/reporte-novedades", $info);
        }
    }

    public function CerrarNovedad()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $id = $this->input->POST("id");
        $estado = $this->input->POST("resolucion");
        $ingreso_ajustado = $this->input->POST("ingreso_ajustado");
        $egreso_ajustado = $this->input->POST("egreso_ajustado");

        $this->Novedades_model->CerrarNovedad($id, $estado, $ingreso_ajustado, $egreso_ajustado);
    }

    public function GetNovedades()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $tipo = json_decode($this->input->POST("tipo"));
        $estado = json_decode($this->input->POST("estado"));
        $legajos = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $info = array(
            "novedades" => $this->Novedades_model->GetNovedades($tipo, $estado, $legajos, $desde, $hasta)
        );

        if (sizeOf($info['novedades']) > 0) {
            $this->load->view("control/novedades/card_novedad", $info);
        }
    }

    public function GetNovedadesAlmuerzo()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $tipo = json_decode($this->input->POST("tipo_almuerzo"));
        $estado = json_decode($this->input->POST("estado_almuerzo"));
        $legajos = json_decode($this->input->POST("legajos_almuerzo"));
        $desde = $this->input->POST("desde_almuerzo");
        $hasta = $this->input->POST("hasta_almuerzo");

        $info = array(
            "novedades" => $this->Novedades_model->GetNovedadesAlmuerzo($tipo, $estado, $legajos, $desde, $hasta)
        );

        if (sizeOf($info['novedades']) > 0) {
            $this->load->view("control/novedades/card_novedad_almuerzo", $info);
        }
    }

    public function CountNovedadesAbiertas()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        echo current($this->Novedades_model->CountNovedadesAbiertas());
    }

    public function CountNovedadesAlmuerzoAbiertas()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        echo current($this->Novedades_model->CountNovedadesAlmuerzoAbiertas());
    }

    public function JustificarNovedadAlmuerzo()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $id = $this->input->POST("id");

        $this->Novedades_model->JustificarNovedadAlmuerzo($id);
    }

    public function InjustificarNovedadAlmuerzo()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $id = $this->input->POST("id");

        $this->Novedades_model->InustificarNovedadAlmuerzo($id, $ajuste);
    }

    /* ------------------------GENERACION DE NOVEDADES------------------------ */
    public function GenerarNovedades()
    {
        if (!($this->session->userdata('login'))) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");
        $dia_controlado = DateTime::createFromFormat('Y-m-d', $this->Novedades_model->GetUltimaGeneracion()->fecha);

        $hoy = new DateTime("today");
        $legajos = $this->Novedades_model->GetLegajos();
        while ($dia_controlado < $hoy) {
            $isFeriado = $this->Novedades_model->isFeriado($dia_controlado->format('Y-m-d'));
            if ($isFeriado) {
                // Es feriado, no controlo este dia
                $dia_controlado = $dia_controlado->add(new DateInterval('P1D'));
                $this->Novedades_model->ActualizarUltimaGeneration($dia_controlado->format('Y-m-d'));
                continue;
            }

            // Controlo legajo por legajo por inconsistencias
            foreach ($legajos as $legajo) {
                $fecha_ingreso_legajo = DateTime::createFromFormat('Y-m-d', $legajo->fecha_ingreso);
                $isDiaExcepcion = $this->Novedades_model->IsDiaExcepcion($dia_controlado->format('Y-m-d'), $legajo->num_legajo);
                if (($dia_controlado < $fecha_ingreso_legajo) || $isDiaExcepcion) {
                    // El legajo no trabaja el dia de hoy
                    continue;
                }

                $this->controlarPorNovedades($legajo, $dia_controlado);

                $this->controlarAlmuerzo($legajo, $dia_controlado);
            }

            $dia_controlado = $dia_controlado->add(new DateInterval('P1D'));
            $this->Novedades_model->ActualizarUltimaGeneration($dia_controlado->format('Y-m-d'));
        }

        $this->Novedades_model->CleanUltimaGeneracion();
    }

    private function controlarPorNovedades($legajo, $dia_controlado)
    {
        $dia_de_la_semana = $this->traducirDia($dia_controlado->format("l"));
        // El modelo devuelve arreglo de objetos aca
        $ingreso = $this->Novedades_model->GetIngreso($dia_controlado->format('Y-m-d'), $legajo->num_legajo);
        $egreso = $this->Novedades_model->GetEgreso($dia_controlado->format('Y-m-d'), $legajo->num_legajo);

        // El modelo devuelve un objeto con solo una propiedad aca. Hago current para tomar el valor de la propiedad.
        $ingreso_teorico = current($this->Novedades_model->GetIngresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
        $egreso_teorico = current($this->Novedades_model->GetEgresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
        $tolerancia_min = current($this->Novedades_model->GetTolerancia($legajo->id_franja_horaria));

        if (($ingreso_teorico == NULL) && ($egreso_teorico == NULL)) {
            // Persona de franco
            return;
        }

        if (empty($ingreso) && empty($egreso)) {
            $this->Novedades_model->GenerarNovedad(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $ingreso,
                $egreso,
                $ingreso_teorico,
                $egreso_teorico,
                $tolerancia_min,
                "INASISTENCIA");
            return;
        }

        if ((sizeOf($ingreso) != 1) || (sizeOf($egreso) != 1)) {
            $this->Novedades_model->GenerarNovedad(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $ingreso,
                $egreso,
                $ingreso_teorico,
                $egreso_teorico,
                $tolerancia_min,
                "REGISTRO INCONSISTENTE");
            return;
        }

        // si llegamos hasta aca, quiere decir q hay solo 1 ingreso y solo 1 egreso. controlamos q sean correctos
        // transformo las horas en objetos DateTime para que sean comparables
        $ingreso_datetime = DateTime::createFromFormat("H:i:s", current($ingreso[0]));
        $egreso_datetime = DateTime::createFromFormat("H:i:s", current($egreso[0]));
        $ingreso_teorico_datetime = DateTime::createFromFormat("H:i:s", $ingreso_teorico);
        $egreso_teorico_datetime = DateTime::createFromFormat("H:i:s", $egreso_teorico);

        // Calculo la diferencia entre el ingreso y el egreso, en segundos, valor absoluto (siempre positivo)
        // Si la diferencia es mayor a la tolerancia, hay tabla!
        $diff_ingreso = abs($ingreso_datetime->GetTimestamp() - $ingreso_teorico_datetime->GetTimestamp());
        $diff_egreso = abs($egreso_datetime->GetTimestamp() - $egreso_teorico_datetime->GetTimestamp());

        if ($diff_ingreso > ($tolerancia_min * 60) || $diff_egreso > ($tolerancia_min * 60)) {
            $this->Novedades_model->GenerarNovedad(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $ingreso,
                $egreso,
                $ingreso_teorico,
                $egreso_teorico,
                $tolerancia_min,
                "HORARIO INCONSISTENTE");
            return;
        }
    }

    private function controlarAlmuerzo($legajo, $dia_controlado)
    {
        $dia_de_la_semana = $this->traducirDia($dia_controlado->format("l"));

        // El modelo devuelve arreglo de objetos aca
        $ingreso = $this->Novedades_model->GetIngreso($dia_controlado->format('Y-m-d'), $legajo->num_legajo);
        $egreso = $this->Novedades_model->GetEgreso($dia_controlado->format('Y-m-d'), $legajo->num_legajo);

        // El modelo devuelve un objeto con solo una propiedad aca. Hago current para tomar el valor de la propiedad.
        $ingreso_teorico = current($this->Novedades_model->GetIngresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
        $egreso_teorico = current($this->Novedades_model->GetEgresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));

        if (empty($ingreso) && empty($egreso)) {
            // Persona con inasistencia
            return;
        }

        if (($ingreso_teorico == NULL) && ($egreso_teorico == NULL)) {
            // Persona de franco
            return;
        }

        $registros_almuerzo = $this->Novedades_model->GetAlmuerzos($dia_controlado->format('Y-m-d'), $legajo->num_legajo);
        $tiempo_almuerzo = current($this->Novedades_model->GetTiempoAlmuerzo($legajo->id_franja_horaria));
        if (empty($registros_almuerzo)) {
            $this->Novedades_model->GenerarNovedadAlmuerzo(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $registros_almuerzo,
                $tiempo_almuerzo,
                "INASISTENCIA");
            return;
        }

        if (sizeOf($registros_almuerzo) != 2) {
            $this->Novedades_model->GenerarNovedadAlmuerzo(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $registros_almuerzo,
                $tiempo_almuerzo,
                "REGISTRO INCONSISTENTE");
            return;
        }

        $ingreso_datetime = DateTime::createFromFormat("H:i:s", current($registros_almuerzo[0]));
        $egreso_datetime = DateTime::createFromFormat("H:i:s", current($registros_almuerzo[1]));
        $tiempo_almuerzo_tomado = abs($ingreso_datetime->GetTimestamp() - $egreso_datetime->GetTimestamp());

        if ($tiempo_almuerzo_tomado > ($tiempo_almuerzo * 60)) {
            $this->Novedades_model->GenerarNovedadAlmuerzo(
                $legajo->num_legajo,
                $dia_controlado->format('Y-m-d'),
                $registros_almuerzo,
                $tiempo_almuerzo,
                "HORARIO INCONSISTENTE",
                $tiempo_almuerzo_tomado / 60);
            return;
        }
    }

    private function traducirDia($dia)
    {
        switch ($dia) {
            case "Monday":
                return "lunes";
            case "Tuesday":
                return "martes";
            case "Wednesday":
                return "miercoles";
            case "Thursday":
                return "jueves";
            case "Friday":
                return "viernes";
            case "Saturday":
                return "sabado";
            case "Sunday":
                return "domingo";
        }
    }
}
