<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Area extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Areas_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'areas' => $this->Areas_model->GetAreas()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('areas/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function DeleteArea($id)
    {
        if ($this->session->userdata('login')) {
            $verificacion = $this->Areas_model->PoseeHijos($id);
            if ($verificacion) {
                $this->session->set_flashdata("ERROR", "No se puede borrar porque existe un area asociada");
                $this->index();
            } else {
                $data = array(
                    "estado" => "0",
                );
                $this->Areas_model->Update($id, $data);
                $this->session->set_flashdata("ERROR", NULL);
                redirect("areas/area");
            }
        } else {
            $this->load->view('login');
        }
    }
    public function Add()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'areas' => $this->Areas_model->GetAreas()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('areas/add', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Store()
    {
        if ($this->session->userdata('login')) {
            $area = $this->input->POST("area");
            $descripcion = $this->input->POST("descripcion");
            $dependencia = $this->input->POST("depencdencia");
            $this->form_validation->set_rules("area", "area", "required|is_unique[areas.nombre_area]");
            if ($this->form_validation->run()) {
                $info = array(
                    'nombre_area' => $area,
                    'descripcion' => $descripcion,
                    'estado' => '1',
                    'id_area' => $dependencia
                );
                if ($this->Areas_model->Save($info)) {
                    redirect(base_url() . "areas/area");
                } else {
                    $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                    redirect(base_url() . "areas/area/add");
                }
            } else {
                $this->Add();
            }
        } else {
            $this->load->view('login');
        }
    }
    public function Edit($id)
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'area' => $this->Areas_model->GetArea($id),
                'areas' => $this->Areas_model->GetAreas()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('areas/edit', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Update()
    {
        if ($this->session->userdata('login')) {
            $idarea = $this->input->POST("idarea");
            $area = $this->input->POST("area");
            $descripcion = $this->input->POST("descripcion");
            $dependencia = $this->input->POST("depencdencia");
            $areaActual = $this->Areas_model->GetArea($idarea);
            if ($area == $areaActual->nombre_area) {
                $unique = '';
            } else {
                $unique = '|is_unique[areas.nombre_area]';
            }
            $this->form_validation->set_rules("area", "area", "required" . $unique);
            if ($this->form_validation->run()) {
                $info = array(
                    'nombre_area' => $area,
                    'descripcion' => $descripcion,
                    'id_area' => $dependencia
                );
                if ($this->Areas_model->Update($idarea, $info)) {
                    redirect(base_url() . "areas/area");
                } else {
                    $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                    redirect(base_url() . "areas/area/add");
                }
            } else {
                $this->Edit($idarea);
            }
        } else {
            $this->load->view('login');
        }
    }
}
