$(function () {
    $('#sign_up').validate({
        rules: {
            'usuario': {
                required: true
            },
            'password': {
                required: true,
                minlength: 6
            },
            'confirm': {
                required: true,
                equalTo: '[name="password"]'
            },
            'pregunta': {
                required: true
            },
            'respuesta': {
                required: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        },
        messages : {
            'usuario': {
                required: 'Debe ingresar un nombre de usuario'
            },
            'password': {
                required: "Debe ingresar una contraseña",
                minlength: "La contraseña debe tener al menos 6 caracteres"
            },
            'confirm': {
                required: "Debe ingresar una contraseña",
                equalTo : 'Los campos de contraseña deben coincidir',
            },
            'pregunta': {
                required: "Debe ingresar una pregunta de recuperacion"
            },
            'respuesta': {
                required: "Debe ingresar una respuesta para la pregunta de recuperacion"
            }
        }
    });
});