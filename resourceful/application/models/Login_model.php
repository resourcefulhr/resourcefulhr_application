<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function login($username, $password)
    {
        $this->db->where("usuario",$username);
        $resultados = $this->db->get("usuarios");
        $data = (Object) array(
            "error" => NULL,
            "usuario" => NULL,
            "permisos_leer" => NULL,
            "permisos_modificar" => NULL,
            "permisos_insertar" => NULL,
            "permisos_baja" => NULL
        );

        if ($resultados->num_rows() == 0) {
            $data->error = "El nombre de usuario ingresado es incorrecto.";
        } else if (password_verify($password, $resultados->row()->contraseña)) {
            $data->usuario = $resultados->row();
            $data->permisos_leer = $this->GetPermisosLeer($data->usuario->id_rol);
            $data->permisos_modificar = $this->GetPermisosModificar($data->usuario->id_rol);
            $data->permisos_insertar = $this->GetPermisosInsertar($data->usuario->id_rol);
            $data->permisos_baja = $this->GetPermisosBaja($data->usuario->id_rol);
        } else {
            $data->error = "La contraseña ingresada no es correcta.";
        }

        return $data;
    }

    private function GetPermisosLeer($idRol)
    {
        $resultado = $this->db->select("s.link as seccion")
                ->from("permisos p")
                ->join("secciones s", "p.id_seccion = s.id")
                ->where("p.id_rol", $idRol)
                ->where("p.leer", 1)
                ->get()
                ->result();
        $array = array();
        foreach ($resultado as $seccion) {
            array_push($array, $seccion->seccion);
        }

        return $array;
    }

    private function GetPermisosModificar($idRol)
    {
        $resultado = $this->db->select("s.link as seccion")
                ->from("permisos p")
                ->join("secciones s", "p.id_seccion = s.id")
                ->where("p.id_rol", $idRol)
                ->where("p.modificar", 1)
                ->get()
                ->result();
        $array = array();
        foreach ($resultado as $seccion) {
            array_push($array, $seccion->seccion);
        }

        return $array;
    }

    private function GetPermisosInsertar($idRol)
    {
        $resultado = $this->db->select("s.link as seccion")
                ->from("permisos p")
                ->join("secciones s", "p.id_seccion = s.id")
                ->where("p.id_rol", $idRol)
                ->where("p.insertar", 1)
                ->get()
                ->result();
        $array = array();
        foreach ($resultado as $seccion) {
            array_push($array, $seccion->seccion);
        }

        return $array;
    }

    private function GetPermisosBaja($idRol)
    {
        $resultado = $this->db->select("s.link as seccion")
                ->from("permisos p")
                ->join("secciones s", "p.id_seccion = s.id")
                ->where("p.id_rol", $idRol)
                ->where("p.baja", 1)
                ->get()
                ->result();
        $array = array();
        foreach ($resultado as $seccion) {
            array_push($array, $seccion->seccion);
        }

        return $array;
    }

    public function StoreUsuario($usuario, $contraseña, $pregunta_recuperacion, $respuesta_recuperacion)
    {
        $hash = password_hash($contraseña, PASSWORD_BCRYPT, ['cost'=>10]);
        $hash_respuesta = password_hash($respuesta_recuperacion, PASSWORD_BCRYPT, ['cost'=>10]);

        $info = array(
            'usuario' => $usuario,
            'contraseña' => $hash,
            'pregunta_recuperacion' => $pregunta_recuperacion,
            'respuesta_recuperacion' => $hash_respuesta,
            'estado' => '1',
            'id_rol' => '2'
        );
        return $this->db->insert('usuarios', $info);
    }

    public function CambiarPass($id, $contraseña)
    {
        $hash = password_hash($contraseña, PASSWORD_BCRYPT, ['cost'=>10]);
        $info = array(
            "contraseña" => $hash
        );
        $this->db->where("id", $id);
        return $this->db->update("usuarios", $info);
    }

    public function GetPregunta($usuario)
    {
        return $this->db->select("pregunta_recuperacion")
                        ->from("usuarios")
                        ->where("usuario", $usuario)
                        ->get()
                        ->row();
    }

    public function RecuperarPass($usuario, $respuesta, $contraseña)
    {
        $validacion = $this->db->select("respuesta_recuperacion")
                        ->from("usuarios")
                        ->where("usuario", $usuario)
                        ->get()
                        ->row();

        if (password_verify($respuesta, $validacion->respuesta_recuperacion)) {
            $hash = password_hash($contraseña, PASSWORD_BCRYPT, ['cost'=>10]);
            $info = array(
                "contraseña" => $hash
            );
            $this->db->where("usuario", $usuario)
                    ->update("usuarios", $info);

            return true;
        } else {
            return false;
        }
    }
}
