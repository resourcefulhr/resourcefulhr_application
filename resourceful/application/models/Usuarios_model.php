<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios_model extends CI_Model
{
    //trae todos los usuarios con los nombres de los roles
    public function GetUsuarios()
    {
        $this->db->select("u.*, r.nombre as rolName")
            ->from("usuarios u")
            ->join("roles r", "u.id_rol = r.id")
            ->where("u.estado", "1");
        $resultados = $this->db->get();
        return $resultados->result();
    }
    public function GetRoles()
    {
        $resultados = $this->db->get("roles");
        return $resultados->result();
    }
    public function Update($id, $info)
    {
        $this->db->where("id", $id);
        return $this->db->update("usuarios", $info);
    }
    //trae unicamente 1 solo usuario
    public function GetUsuario($id)
    {
        $this->db->where("id", $id);
        $resultados = $this->db->get("usuarios");
        return $resultados->row();
    }
}
