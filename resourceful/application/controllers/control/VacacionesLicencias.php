<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VacacionesLicencias extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index($idLegajo = null) 
    {
        if (!isset($idLegajo)) {
            return;
        }
        $this->load->model("Legajos_model");
        $info = array(
            "legajo" => $this->Legajos_model->GetLegajo($idLegajo)
        );

        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/VacacionesLicencias', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function GetEventos()
    {
        $this->load->model("VacacionesLicencias_model");
        $idLegajo = $this->input->POST("legajo_id");

        echo json_encode($this->VacacionesLicencias_model->GetEventos($idLegajo));
    }

    public function GuardarEvento()
    {
        $this->load->model("VacacionesLicencias_model");
        $idLegajo = $this->input->POST("legajo_id");
        $tipo = $this->input->POST("tipo");
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $this->VacacionesLicencias_model->GuardarEvento($idLegajo, $tipo, $desde, $hasta);

        echo json_encode($this->VacacionesLicencias_model->GetEventos($idLegajo));
    }

    public function EditarEvento()
    {
        $this->load->model("VacacionesLicencias_model");
        $id = $this->input->POST("id");
        $idLegajo = $this->input->POST("legajo_id");
        $tipo = $this->input->POST("tipo");
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $this->VacacionesLicencias_model->EditarEvento($id, $tipo, $desde, $hasta);

        echo json_encode($this->VacacionesLicencias_model->GetEventos($idLegajo));
    }

    public function EliminarEvento() {
        $this->load->model("VacacionesLicencias_model");
        
        $id = $this->input->POST("id");
        $idLegajo = $this->input->POST("legajo_id");
        
        $this->VacacionesLicencias_model->EliminarEvento($id);
        
        echo json_encode($this->VacacionesLicencias_model->GetEventos($idLegajo));
    }
}
