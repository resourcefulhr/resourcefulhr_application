<section class="content">
    <h1>
        Usuarios
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>#</th>
                            <th>Nombre de Usuario</th>
                            <th>Rol</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($usuarios)) : ?>
                            <?php foreach ($usuarios as $usuario) : ?>
                                <tr>
                                    <td><?php echo $usuario->id; ?></td>
                                    <td><?php echo $usuario->usuario; ?></td>
                                    <td><?php echo $usuario->rolName; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php if(in_array("Seguridad -> Usuarios", $this->session->userdata("permisos_modificar"))): ?>
                                            <a href="<?php echo ($usuario->id == 1) ? "#" : base_url() . "seguridad/usuarios/Edit/" . $usuario->id; ?>" class="btn btn-warning waves-effect" <?php if($usuario->id == 1) echo "disabled"; ?>><i class=material-icons>mode_edit</i></a>
                                            <?php endif; ?>

                                            <?php if(in_array("Seguridad -> Usuarios", $this->session->userdata("permisos_baja"))): ?>
                                            <a href="<?php echo ($usuario->id == 1) ? "#" : base_url() . "seguridad/usuarios/Delete/" . $usuario->id; ?>" class="btn btn-danger btn-remove waves-effect" <?php if($usuario->id == 1) echo "disabled"; ?>><i class=material-icons>delete_forever</i></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
    </div>
</section>