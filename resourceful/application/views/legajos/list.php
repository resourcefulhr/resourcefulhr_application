    <section class="content">
        <h1>
            Legajos
            <small>Listado</small>
        </h1>
        <!-- Default box -->
        <div class="card">
            <div class="body">
                <?php if(in_array("Aplicacion -> Legajos", $this->session->userdata("permisos_insertar"))): ?>
                <div class="input-group">
                    <div class="input-group">
                        <a href="<?php echo base_url(); ?>areas/legajos/add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                            </i> Agregar Legajo</a>
                    </div>
                </div>
                <hr>
                <?php endif; ?>

                <div>
                    <table id="dataTable" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr class="bg-blue-grey">
                                <th>Legajo</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>Mail</th>
                                <th>Domicilio</th>
                                <th>Tel</th>
                                <th>Cargo</th>
                                <th class="col-md-2">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($legajos)) : ?>
                                <?php foreach ($legajos as $legajo) : ?>
                                    <tr>
                                        <td><?php echo $legajo->num_legajo; ?></td>
                                        <td><?php echo $legajo->apellido; ?></td>
                                        <td><?php echo $legajo->nombre; ?></td>
                                        <td><?php echo $legajo->mail; ?></td>
                                        <td><?php echo $legajo->domicilio; ?></td>
                                        <td><?php echo $legajo->telefono; ?></td>
                                        <td><?php echo $legajo->cargo; ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a type="button" class="btn btn-block btn-lg bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                                                        Opciones</span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li><a type="button" class="btn btn-block bg-cyan waves-effect" data-toggle="modal" data-target="#modalReport" onclick="generateReport('<?php echo $legajo->num_legajo; ?>');"><i class=material-icons>search</i>Mostrar</a></li>
                                                    <?php if(in_array("Aplicacion -> Legajos", $this->session->userdata("permisos_modificar"))): ?>
                                                    <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesLegajo/<?php echo $legajo->id; ?>" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                                                    <li><a href="<?php echo base_url(); ?>areas/legajos/Edit/<?php echo $legajo->num_legajo; ?>" class="btn btn-block btn-lg bg-amber waves-effect"><i class="material-icons">mode_edit</i>Editar</a></li>
                                                    <li><a href="<?php echo base_url(); ?>areas/legajos/getFamilia/<?php echo $legajo->num_legajo; ?>" class="btn btn-block btn-lg bg-orange waves-effect"><i class="material-icons">edit group</i>Grupo familiar</a></li>
                                                    <li><a href="<?php echo base_url(); ?>control/VacacionesLicencias/index/<?php echo $legajo->num_legajo; ?>" class="btn btn-block btn-lg bg-deep-orange waves-effect"><i class="material-icons">calendar_today</i>Vacaciones y Lic.</a></li>
                                                    <?php endif; ?>

                                                    <?php if(in_array("Aplicacion -> Legajos", $this->session->userdata("permisos_baja"))): ?>
                                                    <li><a href="<?php echo base_url(); ?>areas/legajos/Delete/<?php echo $legajo->num_legajo; ?>" class="btn btn-block btn-lg bg-red waves-effect"><i class="material-icons">delete_forever</i>Borrar</a></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- modal -->
    <div class="modal fade" id="modalReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                </div>
                <hr>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-print">Descargar</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>
    <script>
        function generateReport(num_leg) {
            $.ajax({
                url: "legajos/GetReport/" + num_leg,
                type: "POST",
                dataType: "html",
                success: function(data) {
                    $("#modalReport .modal-body").html(data);
                }
            })
        }

        $(document).on("click", ".btn-print", function() {
            $("#modalReport .modal-body").print({});
        });
    </script>