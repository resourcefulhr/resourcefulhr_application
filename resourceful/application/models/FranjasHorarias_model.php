<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FranjasHorarias_model extends CI_Model
{
    public function GetFranjas()
    {
        return $this->db->select("*")
        ->from("franjas_horarias")
        ->where("estado = 1")
        ->get()
        ->result();
    }

    public function GetFranja($idFranja)
    {
        return $this->db->select("*")
        ->from("franjas_horarias")
        ->where("id", $idFranja)
        ->get()
        ->row();
    }

    public function Insert($info)
    {
        return $this->db->insert("franjas_horarias", $info);
    }

    public function Update($id, $info)
    {
        $this->db->where("id", $id);
        return $this->db->update("franjas_horarias", $info);
    }

    public function GetLegajosPorFranja($idFranja)
    {
        return $this->db->select("l.num_legajo, l.nombre, l.apellido, l.tipo_documento, l.numero_documento, a.nombre_area, c.cargo")
        ->from("legajos l")
        ->where("id_franja_horaria", $idFranja)
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id")
        ->get()
        ->result();
    }

    public function GetLegajos()
    {
        return $this->db->select("l.num_legajo, l.nombre, l.apellido, l.tipo_documento, l.numero_documento, a.nombre_area, c.cargo")
        ->from("legajos l")
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id")
        ->get()
        ->result();
    }

    public function AsignarFranjas($idFranja, $legajos)
    {
        foreach ($legajos as $legajo) {
            $info = array(
                "id_franja_horaria" => $idFranja
            );
            $this->db->where("num_legajo", $legajo);
            return $this->db->update("legajos", $info);
        }
    }
}