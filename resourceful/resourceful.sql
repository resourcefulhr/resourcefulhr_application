-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2019 a las 20:43:42
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `resourceful`
--
CREATE DATABASE IF NOT EXISTS `resourceful` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `resourceful`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptitudes`
--

CREATE TABLE `aptitudes` (
  `id` int(11) NOT NULL,
  `aptitud` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptitudes`
--

INSERT INTO `aptitudes` (`id`, `aptitud`) VALUES
(2, 'español'),
(1, 'Ingles'),
(4, 'Java'),
(6, 'JIRA'),
(7, 'movie maker'),
(8, 'Paint'),
(5, 'PHP'),
(10, 'Scrum Master'),
(3, 'work office');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptitudes_cargos`
--

CREATE TABLE `aptitudes_cargos` (
  `id_aptitud` int(11) NOT NULL,
  `id_cargo` int(11) NOT NULL,
  `nivel` int(1) NOT NULL,
  `excluyente` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptitudes_cargos`
--

INSERT INTO `aptitudes_cargos` (`id_aptitud`, `id_cargo`, `nivel`, `excluyente`) VALUES
(1, 1, 0, 0),
(3, 1, 0, 0),
(5, 1, 0, 0),
(2, 2, 2, 1),
(2, 3, 2, 1),
(1, 3, 2, 1),
(3, 3, 1, 0),
(10, 3, 1, 0),
(6, 3, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptitudes_curriculums`
--

CREATE TABLE `aptitudes_curriculums` (
  `id_aptitud` int(11) NOT NULL,
  `id_curriculum` int(11) NOT NULL,
  `nivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptitudes_curriculums`
--

INSERT INTO `aptitudes_curriculums` (`id_aptitud`, `id_curriculum`, `nivel`) VALUES
(8, 2, 3),
(6, 2, 3),
(5, 2, 3),
(4, 2, 3),
(3, 2, 3),
(2, 2, 3),
(1, 2, 3),
(4, 4, 2),
(1, 4, 2),
(2, 4, 3),
(3, 4, 1),
(6, 4, 1),
(8, 4, 1),
(1, 6, 2),
(10, 6, 2),
(8, 6, 2),
(7, 6, 2),
(6, 6, 2),
(5, 6, 2),
(4, 6, 2),
(3, 6, 2),
(2, 6, 2),
(10, 1, 1),
(8, 1, 1),
(7, 1, 1),
(6, 1, 1),
(5, 1, 1),
(4, 1, 1),
(3, 1, 1),
(2, 1, 1),
(1, 1, 1),
(10, 5, 3),
(8, 5, 2),
(7, 5, 2),
(6, 5, 3),
(5, 5, 2),
(4, 5, 1),
(3, 5, 3),
(2, 5, 1),
(1, 5, 2),
(10, 3, 1),
(8, 3, 1),
(7, 3, 2),
(6, 3, 1),
(5, 3, 3),
(4, 3, 2),
(3, 3, 1),
(2, 3, 2),
(1, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aptitudes_legajos`
--

CREATE TABLE `aptitudes_legajos` (
  `id_aptitud` int(11) NOT NULL,
  `id_legajo` int(11) NOT NULL,
  `nivel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aptitudes_legajos`
--

INSERT INTO `aptitudes_legajos` (`id_aptitud`, `id_legajo`, `nivel`) VALUES
(8, 1, 3),
(6, 1, 3),
(5, 1, 3),
(4, 1, 3),
(3, 1, 3),
(2, 1, 3),
(1, 1, 3),
(10, 2, 2),
(8, 2, 2),
(7, 2, 2),
(6, 2, 2),
(5, 2, 2),
(4, 2, 2),
(3, 2, 2),
(2, 2, 2),
(1, 2, 2),
(10, 3, 1),
(8, 3, 1),
(7, 3, 1),
(6, 3, 1),
(5, 3, 1),
(4, 3, 1),
(3, 3, 1),
(2, 3, 1),
(1, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `nombre_area` varchar(256) NOT NULL,
  `descripcion` varchar(256) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_area` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `nombre_area`, `descripcion`, `estado`, `id_area`) VALUES
(1, 'Alquileres Argentina S.A', 'Empresa', 1, 1),
(2, 'Gerencia General', 'Toma de decisiones generalizadas', 1, 1),
(4, 'Gerente de compras', 'Toma de decisiones en área de compras', 1, 2),
(5, 'Ventas', '', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidatos`
--

CREATE TABLE `candidatos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(54) NOT NULL,
  `apellido` varchar(54) NOT NULL,
  `sexo` varchar(60) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `fecha_cv` date DEFAULT NULL,
  `puesto` varchar(256) NOT NULL,
  `telefono` int(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `url_cv` varchar(256) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `candidatos`
--

INSERT INTO `candidatos` (`id`, `nombre`, `apellido`, `sexo`, `fecha_nacimiento`, `fecha_cv`, `puesto`, `telefono`, `email`, `url_cv`, `observaciones`) VALUES
(1, 'Max', 'Szmid', 'Masculino', '1997-03-03', '2019-10-29', 'Analista de sistemas', 2147483647, 'maxszmid@gmail.com', 'archivos/curriculums/Max_Szmid.pdf', 'Alto chabon clave'),
(2, 'Matias', 'Mansilla', 'Masculino', '1998-02-08', NULL, 'Diseñador grafico', 2147483647, 'MatiasMansilla@gmail.com', '', ''),
(3, 'Antonia', 'Delmasa', 'Femenino', '1980-03-07', NULL, 'Analista de riesgos', 2147483647, 'DelmasaAntonia@hotmail.com', '', ''),
(4, 'Carla', 'Gonzales', 'Femenino', '2019-12-02', NULL, 'Contadora', 2147483647, 'CarlaGonzales@gmail.com', '', ''),
(5, 'Marcos', 'Diaz', 'Masculino', '2019-12-02', NULL, 'Coordinador de área', 2147483647, 'MarcosDiaz@yahoo.com.ar', '', ''),
(6, 'Leo', 'Ceballos', 'Otro', '1992-03-17', NULL, 'Programador', 2147483647, 'LeoCeballos@gmail.com', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

CREATE TABLE `cargos` (
  `id` int(11) NOT NULL,
  `cargo` varchar(56) NOT NULL,
  `descripcion` varchar(256) NOT NULL,
  `tareas` text NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_area` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `cargo`, `descripcion`, `tareas`, `estado`, `id_area`) VALUES
(1, 'Operario', 'Operador telefonico y/o atencion al cliente', 'Realiza operaciones básicas de administración y gestión', 1, 5),
(2, 'Repartidor', 'Repartir Insumos', 'Localizar los insumos y distribuirlos en areas a principio de cada mes', 1, 5),
(3, 'Gerente de compras', 'Toma de decisiones en área de compras', 'Realiza compras, evalúa y decide el camino de su área', 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(4) NOT NULL,
  `ciudad_nombre` varchar(60) NOT NULL,
  `cp` int(4) NOT NULL,
  `provincia_id` smallint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `ciudad_nombre`, `cp`, `provincia_id`) VALUES
(1, 'Villa carlos paz ', 5443, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_busqueda`
--

CREATE TABLE `detalle_busqueda` (
  `id_generacion_estadisticas_busqueda` int(11) NOT NULL,
  `aptitud` int(11) NOT NULL,
  `nivel` tinyint(4) NOT NULL,
  `prioridad` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_busqueda`
--

INSERT INTO `detalle_busqueda` (`id_generacion_estadisticas_busqueda`, `aptitud`, `nivel`, `prioridad`) VALUES
(219, 2, 3, '0'),
(220, 2, 3, '0'),
(221, 2, 3, '0'),
(222, 2, 3, '0'),
(223, 2, 3, '0'),
(224, 3, 1, '0'),
(224, 2, 1, '0'),
(224, 1, 1, '0'),
(225, 1, 1, '0'),
(225, 3, 1, '0'),
(225, 5, 1, '0'),
(226, 1, 1, '1'),
(226, 2, 1, '0'),
(226, 3, 1, '0'),
(227, 1, 0, '0'),
(227, 3, 0, '0'),
(227, 5, 0, '0'),
(228, 1, 0, '0'),
(228, 3, 0, '0'),
(228, 5, 0, '0'),
(229, 1, 1, '0'),
(229, 2, 1, '0'),
(229, 3, 1, '0'),
(230, 1, 1, '0'),
(230, 2, 1, '0'),
(230, 3, 1, '0'),
(231, 2, 2, '1'),
(232, 2, 2, '1'),
(233, 1, 0, '0'),
(233, 3, 0, '0'),
(233, 5, 0, '0'),
(234, 2, 2, '1'),
(235, 2, 2, '1'),
(236, 1, 0, '0'),
(236, 3, 0, '0'),
(236, 5, 0, '0'),
(237, 11, 1, '0'),
(237, 10, 1, '0'),
(237, 8, 1, '0'),
(237, 7, 1, '0'),
(237, 6, 1, '0'),
(237, 5, 1, '0'),
(237, 4, 1, '0'),
(237, 2, 1, '0'),
(238, 2, 1, '0'),
(238, 1, 1, '0'),
(238, 4, 1, '0'),
(238, 5, 1, '0'),
(238, 6, 1, '0'),
(238, 7, 1, '0'),
(238, 11, 1, '0'),
(238, 10, 1, '0'),
(239, 1, 0, '0'),
(239, 3, 0, '0'),
(239, 5, 0, '0'),
(240, 1, 0, '0'),
(240, 3, 0, '0'),
(240, 5, 0, '0'),
(241, 2, 2, '1'),
(242, 2, 2, '1'),
(243, 2, 2, '1'),
(244, 2, 2, '1'),
(244, 1, 2, '1'),
(244, 3, 1, '0'),
(244, 10, 1, '0'),
(244, 6, 2, '1'),
(245, 2, 2, '1'),
(245, 1, 2, '1'),
(245, 3, 1, '0'),
(245, 10, 1, '0'),
(245, 6, 2, '1'),
(246, 2, 2, '1'),
(247, 1, 0, '0'),
(247, 3, 0, '0'),
(247, 5, 0, '0'),
(248, 1, 1, '0'),
(248, 2, 1, '0'),
(248, 3, 1, '0'),
(248, 6, 1, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feriados`
--

CREATE TABLE `feriados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `feriados`
--

INSERT INTO `feriados` (`id`, `nombre`, `desde`, `hasta`) VALUES
(1, 'Año Nuevo', '2019-01-01', '2019-01-01'),
(2, 'Navidad', '2019-12-25', '2019-12-25'),
(3, 'Carnaval', '2019-03-04', '2019-03-05'),
(4, 'Día del Veterano y de los Caídos en la Guerra de Malvinas.', '2019-04-02', '2019-04-02'),
(5, 'Viernes Santo', '2019-04-19', '2019-04-19'),
(6, 'Dia del Trabajador', '2019-05-01', '2019-05-01'),
(7, 'Día Nacional de la Memoria por la Verdad y la Justicia', '2019-03-24', '2019-03-24'),
(8, 'Día de la Revolución de Mayo', '2019-05-25', '2019-05-25'),
(9, 'Día Paso a la Inmortalidad del General Martín Miguel de Güemes', '2019-06-17', '2019-06-17'),
(10, 'Día Paso a la Inmortalidad del General Manuel Belgrano', '2019-06-20', '2019-06-20'),
(11, 'Día de la Independencia', '2019-07-09', '2019-07-09'),
(12, 'Paso a la Inmortalidad del General José de San Martín', '2019-08-17', '2019-08-17'),
(13, 'Día del Respeto a la Diversidad Cultural', '2019-10-12', '2019-10-12'),
(14, 'Día de la Soberanía Nacional', '2019-11-18', '2019-11-18'),
(15, 'Inmaculada Concepción de María', '2019-12-08', '2019-12-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `franjas_horarias`
--

CREATE TABLE `franjas_horarias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `lunes_ingreso` time DEFAULT NULL,
  `lunes_egreso` time DEFAULT NULL,
  `martes_ingreso` time DEFAULT NULL,
  `martes_egreso` time DEFAULT NULL,
  `miercoles_ingreso` time DEFAULT NULL,
  `miercoles_egreso` time DEFAULT NULL,
  `jueves_ingreso` time DEFAULT NULL,
  `jueves_egreso` time DEFAULT NULL,
  `viernes_ingreso` time DEFAULT NULL,
  `viernes_egreso` time DEFAULT NULL,
  `sabado_ingreso` time DEFAULT NULL,
  `sabado_egreso` time DEFAULT NULL,
  `domingo_ingreso` time DEFAULT NULL,
  `domingo_egreso` time DEFAULT NULL,
  `tiempo_almuerzo` int(11) NOT NULL DEFAULT '60',
  `tolerancia` int(11) NOT NULL DEFAULT '10',
  `estado` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `franjas_horarias`
--

INSERT INTO `franjas_horarias` (`id`, `nombre`, `lunes_ingreso`, `lunes_egreso`, `martes_ingreso`, `martes_egreso`, `miercoles_ingreso`, `miercoles_egreso`, `jueves_ingreso`, `jueves_egreso`, `viernes_ingreso`, `viernes_egreso`, `sabado_ingreso`, `sabado_egreso`, `domingo_ingreso`, `domingo_egreso`, `tiempo_almuerzo`, `tolerancia`, `estado`) VALUES
(1, 'Default', '09:00:00', '18:00:00', '09:00:00', '18:00:00', '09:00:00', '18:00:00', '09:00:00', '18:00:00', '09:00:00', '18:00:00', NULL, NULL, NULL, NULL, 60, 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generacion_estadisticas_busqueda`
--

CREATE TABLE `generacion_estadisticas_busqueda` (
  `id` int(11) NOT NULL,
  `tipo_busqueda` varchar(20) NOT NULL,
  `tipo_reclutamiento` varchar(20) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `generacion_estadisticas_busqueda`
--

INSERT INTO `generacion_estadisticas_busqueda` (`id`, `tipo_busqueda`, `tipo_reclutamiento`, `fecha`) VALUES
(177, 'Por Cargo', 'Interno', '2019-12-04'),
(178, 'Por Cargo', 'Externo', '2019-01-04'),
(179, 'Manual', 'Interno', '2019-02-04'),
(180, 'Por Cargo', 'Interno', '2019-03-04'),
(181, 'Manual', 'Externo', '2019-04-04'),
(182, 'Por Cargo', 'Externo', '2019-05-04'),
(183, 'Manual', 'Interno', '2019-06-04'),
(184, 'Por Cargo', 'Interno', '2019-07-04'),
(185, 'Manual', 'Interno', '2019-08-04'),
(186, 'Por Cargo', 'Interno', '2019-09-04'),
(187, 'Manual', 'Externo', '2019-10-04'),
(188, 'Por Cargo', 'Externo', '2019-11-04'),
(189, 'Manual', 'Interno', '2019-10-12'),
(190, 'Por Cargo', 'Interno', '2019-10-12'),
(191, 'Manual', 'Externo', '2019-10-12'),
(192, 'Por Cargo', 'Externo', '2019-10-12'),
(193, 'Manual', 'Externo', '2019-10-12'),
(194, 'Por Cargo', 'Externo', '2019-11-15'),
(195, 'Manual', 'Externo', '2019-11-15'),
(196, 'Por Cargo', 'Externo', '2019-11-15'),
(197, 'Por Cargo', 'Interno', '2019-11-15'),
(198, 'Por Cargo', 'Interno', '2019-11-15'),
(199, 'Por Cargo', 'Interno', '2019-11-15'),
(200, 'Por Cargo', 'Interno', '2019-11-15'),
(201, 'Por Cargo', 'Interno', '2019-11-15'),
(202, 'Por Cargo', 'Interno', '2019-11-15'),
(203, 'Por Cargo', 'Externo', '2019-11-09'),
(204, 'Por Cargo', 'Interno', '2019-11-09'),
(205, 'Por Cargo', 'Interno', '2019-11-09'),
(206, 'Por Cargo', 'Interno', '2019-11-09'),
(207, 'Por Cargo', 'Interno', '2019-11-09'),
(208, 'Por Cargo', 'Externo', '2019-11-04'),
(209, 'Por Cargo', 'Externo', '2019-11-04'),
(210, 'Por Cargo', 'Interno', '2019-11-04'),
(211, 'Por Cargo', 'Interno', '2019-11-04'),
(212, 'Por Cargo', 'Externo', '2019-12-23'),
(213, 'Por Cargo', 'Externo', '2019-12-23'),
(214, 'Por Cargo', 'Externo', '2019-12-23'),
(215, 'Por Cargo', 'Interno', '2019-12-17'),
(216, 'Por Cargo', 'Externo', '2019-12-17'),
(217, 'Por Cargo', 'Externo', '2019-12-17'),
(218, 'Por Cargo', 'Interno', '2019-12-17'),
(219, 'Manual', 'Interno', '2019-12-17'),
(220, 'Manual', 'Interno', '2019-12-17'),
(221, 'Manual', 'Interno', '2019-12-15'),
(222, 'Manual', 'Interno', '2019-12-15'),
(223, 'Manual', 'Externo', '2019-12-09'),
(224, 'Manual', 'Externo', '2019-12-09'),
(225, 'Manual', 'Externo', '2019-12-09'),
(226, 'Manual', 'Externo', '2019-12-09'),
(227, 'Por Cargo', 'Externo', '2019-12-09'),
(228, 'Por Cargo', 'Interno', '2019-12-09'),
(229, 'Manual', 'Interno', '2019-12-09'),
(230, 'Manual', 'Externo', '2019-12-09'),
(231, 'Por Cargo', 'Externo', '2019-12-05'),
(232, 'Por Cargo', 'Externo', '2019-12-06'),
(233, 'Por Cargo', 'Interno', '2019-03-06'),
(234, 'Por Cargo', 'Interno', '2019-12-03'),
(235, 'Por Cargo', 'Externo', '2019-12-03'),
(236, 'Por Cargo', 'Externo', '2019-12-03'),
(237, 'Manual', 'Interno', '2019-12-06'),
(238, 'Manual', 'Interno', '2019-12-06'),
(239, 'Por Cargo', 'Interno', '2019-12-06'),
(240, 'Por Cargo', 'Externo', '2019-12-06'),
(241, 'Por Cargo', 'Externo', '2019-12-06'),
(242, 'Por Cargo', 'Interno', '2019-12-06'),
(243, 'Por Cargo', 'Interno', '2019-12-06'),
(244, 'Por Cargo', 'Interno', '2019-12-09'),
(245, 'Por Cargo', 'Externo', '2019-12-09'),
(246, 'Por Cargo', 'Externo', '2019-12-09'),
(247, 'Por Cargo', 'Externo', '2019-12-09'),
(248, 'Manual', 'Externo', '2019-12-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_familiar`
--

CREATE TABLE `grupo_familiar` (
  `id` int(11) NOT NULL,
  `parentesco` varchar(48) NOT NULL,
  `nombre` varchar(48) NOT NULL,
  `apellido` varchar(48) NOT NULL,
  `dni` int(8) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `domicilio` varchar(48) NOT NULL,
  `id_legajo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupo_familiar`
--

INSERT INTO `grupo_familiar` (`id`, `parentesco`, `nombre`, `apellido`, `dni`, `fecha_nacimiento`, `domicilio`, `id_legajo`) VALUES
(1, 'Hijo/a', 'Maximo', 'Szmid', 123, '2019-10-02', '123', 1),
(2, 'Padre', 'Marcelo', 'Concha', 28345432, '1981-02-12', 'La havana 300', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `legajos`
--

CREATE TABLE `legajos` (
  `id` int(11) NOT NULL,
  `num_legajo` int(11) NOT NULL,
  `nombre` varchar(48) NOT NULL,
  `apellido` varchar(58) NOT NULL,
  `tipo_documento` varchar(48) NOT NULL,
  `numero_documento` varchar(48) NOT NULL,
  `cuil` varchar(13) NOT NULL,
  `id_nacionalidad` int(11) NOT NULL,
  `id_provincia` smallint(2) NOT NULL,
  `id_ciudad` int(64) NOT NULL,
  `domicilio` varchar(124) NOT NULL,
  `estado_civil` varchar(48) NOT NULL,
  `carnet_conducir` tinyint(1) NOT NULL,
  `sindicato` varchar(48) DEFAULT NULL,
  `obra_social` varchar(64) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `fecha_egreso` date DEFAULT NULL,
  `carga_horaria` int(2) NOT NULL,
  `telefono` int(24) NOT NULL,
  `mail` varchar(76) DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `sexo` varchar(48) NOT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `cbu` int(22) DEFAULT NULL,
  `banco` varchar(48) NOT NULL,
  `copia_dni` tinyint(1) NOT NULL,
  `partida_nacimiento` tinyint(1) NOT NULL,
  `acta_matrimonio` tinyint(1) NOT NULL,
  `sumario_convivencia` tinyint(1) NOT NULL,
  `constancia_cuil` tinyint(1) NOT NULL,
  `f572_rig` tinyint(1) NOT NULL,
  `f649_rig` tinyint(1) NOT NULL,
  `examen_pre_ocupacional` tinyint(1) NOT NULL,
  `contrato` tinyint(1) NOT NULL,
  `afi_sindicato` tinyint(1) NOT NULL,
  `libreta_trabajo` tinyint(1) NOT NULL,
  `seguro_vida` tinyint(1) NOT NULL,
  `ps261` tinyint(1) NOT NULL,
  `carnet_sanitario` tinyint(1) NOT NULL,
  `fecha_sanitario` date DEFAULT NULL,
  `id_franja_horaria` int(11) NOT NULL DEFAULT '1',
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `legajos`
--

INSERT INTO `legajos` (`id`, `num_legajo`, `nombre`, `apellido`, `tipo_documento`, `numero_documento`, `cuil`, `id_nacionalidad`, `id_provincia`, `id_ciudad`, `domicilio`, `estado_civil`, `carnet_conducir`, `sindicato`, `obra_social`, `fecha_ingreso`, `fecha_egreso`, `carga_horaria`, `telefono`, `mail`, `fecha_nacimiento`, `sexo`, `id_cargo`, `cbu`, `banco`, `copia_dni`, `partida_nacimiento`, `acta_matrimonio`, `sumario_convivencia`, `constancia_cuil`, `f572_rig`, `f649_rig`, `examen_pre_ocupacional`, `contrato`, `afi_sindicato`, `libreta_trabajo`, `seguro_vida`, `ps261`, `carnet_sanitario`, `fecha_sanitario`, `id_franja_horaria`, `estado`) VALUES
(1, 1, 'Juan', 'Gonzales', 'dni', '34444567', '213213213213', 1, 1, 1, 'Buenos aires 350', 'Soltero/a', 0, 'Empleados Administrativos', 'Sancor Salud', '2019-10-02', NULL, 9, 35874698, 'JuanGonzales@gmail.com', '1996-02-02', 'Masculino', 1, 2147483647, 'Macro', 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, NULL, 1, 1),
(2, 2, 'Marta', 'Aguirre', 'dni', '28564213', '23285642136', 1, 1, 1, 'La havana 300', 'Casado/a', 1, '', 'Sancor Salud', '2018-01-03', NULL, 9, 2147483647, 'MartaAguirre', '1982-02-19', 'Femenino', 1, 2147483647, 'Santander Rio', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-12-01', 1, 1),
(3, 3, 'Nicolas', 'Bongiorno', 'dni', '40293453', '20402934536', 1, 1, 1, 'San nicolas s/n', 'Soltero/a', 1, 'Sindicato de motocadetes de Córdoba', 'Sancor Salud', '2019-11-01', NULL, 9, 2147483647, 'NicolasBongiorno@yahoo.com.ar', '1995-04-16', 'Otro', 2, 2147483647, 'Santander dio', 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE `nacionalidad` (
  `id` int(11) NOT NULL,
  `PAIS_NAC` varchar(48) NOT NULL,
  `GENTILICIO_NAC` varchar(48) NOT NULL,
  `ISO_NAC` varchar(48) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`id`, `PAIS_NAC`, `GENTILICIO_NAC`, `ISO_NAC`) VALUES
(1, 'Argentina', 'ARG', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE `novedades` (
  `id` int(11) NOT NULL,
  `num_legajo` int(11) NOT NULL,
  `dia` date NOT NULL,
  `ingreso_teorico` time NOT NULL,
  `egreso_teorico` time NOT NULL,
  `tolerancia` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `ingreso_ajustado` time DEFAULT NULL,
  `egreso_ajustado` time DEFAULT NULL,
  `descuenta_dia` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`id`, `num_legajo`, `dia`, `ingreso_teorico`, `egreso_teorico`, `tolerancia`, `tipo`, `estado`, `ingreso_ajustado`, `egreso_ajustado`, `descuenta_dia`) VALUES
(1, 1, '2019-12-02', '09:00:00', '18:00:00', 10, 'INASISTENCIA', 'ABIERTA', NULL, NULL, 0),
(2, 1, '2019-12-03', '09:00:00', '18:00:00', 10, 'INASISTENCIA', 'ABIERTA', NULL, NULL, 0),
(3, 1, '2019-12-04', '09:00:00', '18:00:00', 10, 'INASISTENCIA', 'ABIERTA', NULL, NULL, 0),
(4, 1, '2019-12-05', '09:00:00', '18:00:00', 10, 'HORARIO INCONSISTENTE', 'ABIERTA', NULL, NULL, 0),
(5, 1, '2019-12-06', '09:00:00', '18:00:00', 10, 'INASISTENCIA', 'ABIERTA', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades_almuerzo`
--

CREATE TABLE `novedades_almuerzo` (
  `id` int(11) NOT NULL,
  `num_legajo` int(11) NOT NULL,
  `dia` date NOT NULL,
  `tiempo_almuerzo` int(11) NOT NULL,
  `tiempo_almuerzo_tomado` int(11) DEFAULT NULL,
  `tipo` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades_almuerzo`
--

INSERT INTO `novedades_almuerzo` (`id`, `num_legajo`, `dia`, `tiempo_almuerzo`, `tiempo_almuerzo_tomado`, `tipo`, `estado`) VALUES
(1, 1, '2019-12-05', 60, NULL, 'REGISTRO INCONSISTENTE', 'ABIERTA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades_almuerzo_registros`
--

CREATE TABLE `novedades_almuerzo_registros` (
  `id` int(11) NOT NULL,
  `id_novedad_almuerzo` int(11) NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades_almuerzo_registros`
--

INSERT INTO `novedades_almuerzo_registros` (`id`, `id_novedad_almuerzo`, `hora`) VALUES
(1, 1, '16:18:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades_registros`
--

CREATE TABLE `novedades_registros` (
  `id` int(11) NOT NULL,
  `id_novedad` int(11) NOT NULL,
  `ingreso` time DEFAULT NULL,
  `egreso` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades_registros`
--

INSERT INTO `novedades_registros` (`id`, `id_novedad`, `ingreso`, `egreso`) VALUES
(1, 4, '16:18:00', NULL),
(2, 4, NULL, '16:18:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades_ultima_generacion`
--

CREATE TABLE `novedades_ultima_generacion` (
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades_ultima_generacion`
--

INSERT INTO `novedades_ultima_generacion` (`fecha`) VALUES
('2019-12-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_rol` int(11) NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `insertar` tinyint(4) NOT NULL,
  `baja` tinyint(4) NOT NULL,
  `modificar` tinyint(4) NOT NULL,
  `leer` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_rol`, `id_seccion`, `insertar`, `baja`, `modificar`, `leer`) VALUES
(1, 1, 1, 1, 1, 1),
(1, 2, 1, 1, 1, 1),
(1, 3, 1, 1, 1, 1),
(1, 4, 1, 1, 1, 1),
(1, 5, 1, 1, 1, 1),
(1, 6, 1, 1, 1, 1),
(1, 7, 1, 1, 1, 1),
(1, 8, 1, 1, 1, 1),
(1, 9, 1, 1, 1, 1),
(1, 10, 1, 1, 1, 1),
(1, 11, 1, 1, 1, 1),
(1, 12, 1, 1, 1, 1),
(2, 1, 0, 0, 0, 0),
(2, 2, 0, 0, 0, 0),
(2, 3, 0, 0, 0, 0),
(2, 4, 0, 0, 0, 0),
(2, 5, 0, 0, 0, 0),
(2, 6, 0, 0, 0, 0),
(2, 7, 0, 0, 0, 0),
(2, 8, 0, 0, 0, 0),
(2, 9, 0, 0, 0, 0),
(2, 10, 0, 0, 0, 0),
(2, 11, 0, 0, 0, 0),
(2, 12, 0, 0, 0, 0),
(1, 13, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` smallint(2) NOT NULL,
  `provincia_nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `provincia_nombre`) VALUES
(1, 'Cordoba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroshorarios`
--

CREATE TABLE `registroshorarios` (
  `id` int(11) NOT NULL,
  `id_legajo` int(11) NOT NULL,
  `ingreso` datetime NOT NULL,
  `egreso` datetime NOT NULL,
  `almuerzo` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registroshorarios`
--

INSERT INTO `registroshorarios` (`id`, `id_legajo`, `ingreso`, `egreso`, `almuerzo`) VALUES
(1, 1, '2019-12-05 16:18:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-12-05 16:18:25'),
(3, 1, '0000-00-00 00:00:00', '2019-12-05 16:18:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(54) NOT NULL,
  `descripcion` varchar(254) NOT NULL,
  `estado` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `descripcion`, `estado`) VALUES
(1, 'Admin', 'Permiso Total', 1),
(2, 'Sin Permisos', 'Rol por Defecto. Sin Permisos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `id` int(11) NOT NULL,
  `link` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`id`, `link`) VALUES
(1, 'Seguridad -> Usuarios'),
(2, 'Seguridad -> Roles'),
(3, 'Aplicacion -> Cargos'),
(4, 'Aplicacion -> Areas'),
(5, 'Aplicacion -> Legajos'),
(6, 'Aplicacion -> Organigrama'),
(7, 'Control -> Novedades'),
(8, 'Control -> Registrar Ingreso y Egreso'),
(9, 'Control -> Calendario de Feriados'),
(10, 'Control -> Franjas Horarias'),
(11, 'Reclutamiento -> Candidatos'),
(12, 'Reclutamiento -> Aptitudes'),
(13, 'Reclutamiento -> Busquedas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temp_busqueda_manual`
--

CREATE TABLE `temp_busqueda_manual` (
  `id_aptitud` int(11) NOT NULL,
  `id_busqueda_manual` int(48) NOT NULL,
  `nivel` int(12) NOT NULL,
  `excluyente` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(24) DEFAULT NULL,
  `contraseña` text,
  `pregunta_recuperacion` varchar(255) NOT NULL,
  `respuesta_recuperacion` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `contraseña`, `pregunta_recuperacion`, `respuesta_recuperacion`, `estado`, `id_rol`) VALUES
(1, 'admin', '$2y$12$FijCrc2CPDXOqrpN3UmGe.FBR9.GLaj0mzK6XCnxou6sSAjcFmghy', 'cual fue el nombre de tu primer perro', 'monino', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacaciones_licencias`
--

CREATE TABLE `vacaciones_licencias` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `id_legajo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aptitudes`
--
ALTER TABLE `aptitudes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aptitud` (`aptitud`);

--
-- Indices de la tabla `aptitudes_cargos`
--
ALTER TABLE `aptitudes_cargos`
  ADD KEY `id_aptitud` (`id_aptitud`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- Indices de la tabla `aptitudes_curriculums`
--
ALTER TABLE `aptitudes_curriculums`
  ADD KEY `id_aptitud` (`id_aptitud`),
  ADD KEY `id_curriculum` (`id_curriculum`);

--
-- Indices de la tabla `aptitudes_legajos`
--
ALTER TABLE `aptitudes_legajos`
  ADD KEY `id_legajo` (`id_legajo`),
  ADD KEY `id_aptitud` (`id_aptitud`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cargo` (`id_area`),
  ADD KEY `id_area` (`id_area`);

--
-- Indices de la tabla `candidatos`
--
ALTER TABLE `candidatos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_area` (`id_area`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cp` (`cp`);

--
-- Indices de la tabla `detalle_busqueda`
--
ALTER TABLE `detalle_busqueda`
  ADD KEY `id_generacion_estadisticas_busqueda` (`id_generacion_estadisticas_busqueda`);

--
-- Indices de la tabla `feriados`
--
ALTER TABLE `feriados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `franjas_horarias`
--
ALTER TABLE `franjas_horarias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `generacion_estadisticas_busqueda`
--
ALTER TABLE `generacion_estadisticas_busqueda`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `grupo_familiar`
--
ALTER TABLE `grupo_familiar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_legajo` (`id_legajo`);

--
-- Indices de la tabla `legajos`
--
ALTER TABLE `legajos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cargo` (`id_cargo`),
  ADD KEY `id_provincia` (`id_provincia`,`id_ciudad`),
  ADD KEY `id_ciudad` (`id_ciudad`),
  ADD KEY `id_nacionalidad_2` (`id_nacionalidad`),
  ADD KEY `id_franja_horaria` (`id_franja_horaria`),
  ADD KEY `num_legajo` (`num_legajo`) USING BTREE;

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_legajo` (`num_legajo`);

--
-- Indices de la tabla `novedades_almuerzo`
--
ALTER TABLE `novedades_almuerzo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_legajos` (`num_legajo`);

--
-- Indices de la tabla `novedades_almuerzo_registros`
--
ALTER TABLE `novedades_almuerzo_registros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_novedad_almuerzo` (`id_novedad_almuerzo`);

--
-- Indices de la tabla `novedades_registros`
--
ALTER TABLE `novedades_registros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_novedad` (`id_novedad`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_seccion` (`id_seccion`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registroshorarios`
--
ALTER TABLE `registroshorarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_legajo` (`id_legajo`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `legajo` (`usuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- Indices de la tabla `vacaciones_licencias`
--
ALTER TABLE `vacaciones_licencias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_legajo` (`id_legajo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aptitudes`
--
ALTER TABLE `aptitudes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `candidatos`
--
ALTER TABLE `candidatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `feriados`
--
ALTER TABLE `feriados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `franjas_horarias`
--
ALTER TABLE `franjas_horarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `generacion_estadisticas_busqueda`
--
ALTER TABLE `generacion_estadisticas_busqueda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT de la tabla `grupo_familiar`
--
ALTER TABLE `grupo_familiar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `legajos`
--
ALTER TABLE `legajos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `novedades`
--
ALTER TABLE `novedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `novedades_almuerzo`
--
ALTER TABLE `novedades_almuerzo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `novedades_almuerzo_registros`
--
ALTER TABLE `novedades_almuerzo_registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `novedades_registros`
--
ALTER TABLE `novedades_registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `registroshorarios`
--
ALTER TABLE `registroshorarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `vacaciones_licencias`
--
ALTER TABLE `vacaciones_licencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aptitudes_cargos`
--
ALTER TABLE `aptitudes_cargos`
  ADD CONSTRAINT `aptitudes_cargos_ibfk_1` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aptitudes_cargos_ibfk_2` FOREIGN KEY (`id_aptitud`) REFERENCES `aptitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `aptitudes_curriculums`
--
ALTER TABLE `aptitudes_curriculums`
  ADD CONSTRAINT `aptitudes_curriculums_ibfk_1` FOREIGN KEY (`id_aptitud`) REFERENCES `aptitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aptitudes_curriculums_ibfk_2` FOREIGN KEY (`id_curriculum`) REFERENCES `candidatos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `aptitudes_legajos`
--
ALTER TABLE `aptitudes_legajos`
  ADD CONSTRAINT `aptitudes_legajos_ibfk_1` FOREIGN KEY (`id_aptitud`) REFERENCES `aptitudes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aptitudes_legajos_ibfk_2` FOREIGN KEY (`id_legajo`) REFERENCES `legajos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `detalle_busqueda`
--
ALTER TABLE `detalle_busqueda`
  ADD CONSTRAINT `detalle_busqueda_ibfk_1` FOREIGN KEY (`id_generacion_estadisticas_busqueda`) REFERENCES `generacion_estadisticas_busqueda` (`id`);

--
-- Filtros para la tabla `grupo_familiar`
--
ALTER TABLE `grupo_familiar`
  ADD CONSTRAINT `grupo_familiar_ibfk_1` FOREIGN KEY (`id_legajo`) REFERENCES `legajos` (`num_legajo`);

--
-- Filtros para la tabla `legajos`
--
ALTER TABLE `legajos`
  ADD CONSTRAINT `legajos_ibfk_1` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id`),
  ADD CONSTRAINT `legajos_ibfk_5` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudad` (`id`),
  ADD CONSTRAINT `legajos_ibfk_6` FOREIGN KEY (`id_provincia`) REFERENCES `provincia` (`id`),
  ADD CONSTRAINT `legajos_ibfk_7` FOREIGN KEY (`id_nacionalidad`) REFERENCES `nacionalidad` (`id`);

--
-- Filtros para la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD CONSTRAINT `novedades_ibfk_1` FOREIGN KEY (`num_legajo`) REFERENCES `legajos` (`num_legajo`);

--
-- Filtros para la tabla `novedades_almuerzo`
--
ALTER TABLE `novedades_almuerzo`
  ADD CONSTRAINT `novedades_almuerzo_ibfk_1` FOREIGN KEY (`num_legajo`) REFERENCES `legajos` (`num_legajo`);

--
-- Filtros para la tabla `novedades_almuerzo_registros`
--
ALTER TABLE `novedades_almuerzo_registros`
  ADD CONSTRAINT `novedades_almuerzo_registros_ibfk_1` FOREIGN KEY (`id_novedad_almuerzo`) REFERENCES `novedades_almuerzo` (`id`);

--
-- Filtros para la tabla `novedades_registros`
--
ALTER TABLE `novedades_registros`
  ADD CONSTRAINT `novedades_registros_ibfk_1` FOREIGN KEY (`id_novedad`) REFERENCES `novedades` (`id`);

--
-- Filtros para la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`id_seccion`) REFERENCES `secciones` (`id`);

--
-- Filtros para la tabla `registroshorarios`
--
ALTER TABLE `registroshorarios`
  ADD CONSTRAINT `registroshorarios_ibfk_1` FOREIGN KEY (`id_legajo`) REFERENCES `legajos` (`num_legajo`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `vacaciones_licencias`
--
ALTER TABLE `vacaciones_licencias`
  ADD CONSTRAINT `vacaciones_licencias_ibfk_1` FOREIGN KEY (`id_legajo`) REFERENCES `legajos` (`num_legajo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
