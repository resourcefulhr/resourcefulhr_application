<!-- jQuery-UI. Solo se usa en esta vista, no tiene sentido que este en el header -->
<script type="text/javascript" src="<?php echo base_url(); ?>front/plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>front/plugins/jquery-ui/jquery-ui.min.css" />

<script src="<?php echo base_url(); ?>front/plugins/popper/popper.min.js"></script>

<script src="<?php echo base_url(); ?>front/plugins/bootstrap-datepicker-1.8.0/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/plugins/bootstrap-datepicker-1.8.0/bootstrap-datepicker.standalone.min.css" />

<script src="<?php echo base_url(); ?>front/plugins/js-year-calendar/js-year-calendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/plugins/js-year-calendar/js-year-calendar.min.css" />

<section class="content">
    <h1>
        Vacaciones y Licencias
        <small><b>Legajo Numero:</b> <?php echo $legajo->num_legajo; ?></small>
        <small><b>Nombre:</b> <?php echo $legajo->apellido . ", " . $legajo->nombre; ?></small>
    </h1>

    <div class="card">
        <div class="body">
            <div id="calendar"></div>

            <div class="modal fade" id="event-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Vacaciones y Feriados</h5>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="event-index">
                            <form class="form-line">
                                <div class="form-line">
                                    <label for="event-name" class="col-sm-4 control-label">Tipo</label>
                                    <div class="col-sm-8">
                                        <select name="event-name" type="text" class="form-control">
                                            <option value="VACACIONES">VACACIONES</option>
                                            <option value="LICENCIA MEDICA">LICENCIA MEDICA</option>
                                            <option value="LICENCIA C/GOZE">LICENCIA C/GOZE</option>
                                            <option value="LICENCIA S/GOZE">LICENCIA S/GOZE</option>
                                            <option value="OUT OF OFFICE">OUT OF OFFICE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-line">
                                    <label for="min-date" class="col-sm-4 control-label">Fecha</label>
                                    <div class="col-sm-8" id="datepicker-container">
                                        <div class="input-group input-daterange" data-provide="datepicker">
                                            <div class="input-group-addon">Desde</div>
                                            <input id="min-date" name="event-start-date" type="text" class="form-control" data-date-container='#datepicker-container'>
                                            <div class="input-group-addon">Hasta</div>
                                            <input id="max-date" name="event-end-date" type="text" class="form-control" data-date-container='#datepicker-container'>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
                            <input type="button" class="btn btn-primary" id="save-event" value="Guardar">
                            <input type="button" class="btn btn-danger" id="delete-event" name="delete-event" value="Eliminar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    let calendar = null;
    var base_url = "<?php echo base_url(); ?>";

    function editEvent(event) {
        $('#event-modal input[name="event-index"]').val(event.db_id);

        if (event.name) {
            $('#event-modal select[name="event-name"]').val(event.name).change();
        } else {
            $('#event-modal select[name="event-name"]').val('VACACIONES').change();
        }

        $('#event-modal input[name="event-start-date"]').datepicker({format: 'dd/mm/yyyy'});
        $('#event-modal input[name="event-start-date"]').datepicker('update', event.startDate);

        $('#event-modal input[name="event-end-date"]').datepicker({format: 'dd/mm/yyyy'});
        $('#event-modal input[name="event-end-date"]').datepicker('update', event.endDate);

        if (event.db_id) {
            $('#event-modal input[name="delete-event"]').show();
        } else {
            $('#event-modal input[name="delete-event"]').hide();
        }

        $('#event-modal').modal();
    }

    function deleteEvent() {
        $.ajax({
            url: base_url + "control/VacacionesLicencias/EliminarEvento",
            dataType: "json",
            data: {
                id: $('#event-modal input[name="event-index"]').val(),
                legajo_id: <?php echo $legajo->num_legajo; ?>,
            },
            type: "POST",
            success: function(result) {
                var eventos = generarDataSet(result);
                calendar.setDataSource(eventos);
            }
        });

        $('#event-modal').modal('hide');
    }

    function saveEvent() {
        var desde_completo = $('#event-modal input[name="event-start-date"]').datepicker('getDate');
        var hasta_completo = $('#event-modal input[name="event-end-date"]').datepicker('getDate');
        var evento_id = $('#event-modal input[name="event-index"]').val();

        var url_consolidado = evento_id ? base_url + "control/VacacionesLicencias/EditarEvento" : base_url + "control/VacacionesLicencias/GuardarEvento";

        $.ajax({
            url: url_consolidado,
            dataType: "json",
            data: {
                id: evento_id,
                legajo_id: <?php echo $legajo->num_legajo; ?>,
                tipo: $('#event-modal select[name="event-name"] option:selected').text(),
                desde: desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2),
                hasta: hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2)
            },
            type: "POST",
            success: function(result) {
                var eventos = generarDataSet(result);
                calendar.setDataSource(eventos);
            }
        });

        $('#event-modal').modal('hide');
    }

    function generarCalendario(data) {
        calendar = new Calendar('#calendar', {
            enableContextMenu: false,
            enableRangeSelection: false,
            mouseOnDay: function(e) {
                if (e.events.length > 0) {
                    var content = '';

                    for (var i in e.events) {
                        content += '<div class="event-tooltip-content">' +
                            '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>' +
                            '</div>';
                    }

                    $(e.element).popover({
                        trigger: 'manual',
                        container: 'body',
                        html: true,
                        content: content
                    });

                    $(e.element).popover('show');
                }
            },
            mouseOutDay: function(e) {
                if (e.events.length > 0) {
                    $(e.element).popover('hide');
                }
            },
            dataSource: data
        });

        $('#save-event').click(function() {
            saveEvent();
        });

        $('#delete-event').click(function() {
            deleteEvent();
        });
    }

    // Click en dia
    document.querySelector('#calendar').addEventListener('clickDay', function(e) {
        if (e.events.length > 0) {
            // Editar evento existente
            editEvent(e.events[0]);
        } else {
            // Crear Nuevo Evento
            editEvent({
                    startDate: e.date,
                    endDate: e.date
            });
        }
    });

    // Genera el calendario inicialmente
    $(function() {
        $.ajax({
            url: base_url + "control/VacacionesLicencias/GetEventos",
            data: {
                legajo_id: <?php echo $legajo->num_legajo; ?>
            },
            dataType: "json",
            type: "POST",
            success: function(result) {
                let eventos = generarDataSet(result);
                generarCalendario(eventos);
            }
        });
    });

    // Genera el dataset para el calendario en base a la data q viene la DB
    function generarDataSet(data_cruda) {
        let eventos = new Array();
        if (Object.keys(data_cruda).length !== 0) {
            jQuery.each(data_cruda, function(index, value) {
                item = {
                    id: index,
                    db_id: value.id,
                    name: value.tipo,
                    location: '',
                    startDate: new Date(Date.parse(value.desde.replace('-','/','g'))),
                    endDate: new Date(Date.parse(value.hasta.replace('-','/','g')))
                };
                eventos.push(item);
            });
        }

        return eventos;
    }
</script>