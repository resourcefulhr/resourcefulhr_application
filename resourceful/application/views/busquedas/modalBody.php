<style>
    .centrar {
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<section>
    <div class="row">
        <div class="col-xs-12 text-center bg-light-blue">
            <div class="bg-light-blue">
                <h1 class="text-center"><b>Perfil del cargo actual</b></h1>
            </div>
        </div>
    </div>
</section>
<hr>
<section>
    <table id="dataTable" class="table table-bordered table-striped table-hover">
        <thead>
            <tr class="bg-blue-grey">
                <th>Aptitud</th>
                <th>Nivel</th>
                <th class="col-md-2">Excluyente</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($aptitudescs)) : ?>
                <?php foreach ($aptitudescs as $aptitudc) : ?>
                    <tr>
                        <td>
                            <?php echo $aptitudc->aptitud; ?>
                        </td>
                        <td>
                            <?php echo ($aptitudc->nivel == 1 ? 'Basico' : ($aptitudc->nivel == 2 ? 'Normal' : 'Avanzado')) ?>
                        </td>
                        <td>
                            <?php echo ($aptitudc->excluyente == 1 ? 'SI' : 'NO') ?>
                        </td>
                        <!--<td class="col-sm-3">
                            <div>
                                <a type="button" style="width:40px;" id="edit-aptitud" class="btn btn-warning waves-effect edit-aptitud" data-toggle="modal" data-target="#defaultModal1" value="<?php echo $aptitudc->id_aptitud; ?>">
                                    <i class=material-icons>mode_edit</i>
                                </a></li>
                                <a type="button" style="width:40px;" id="delete-aptitud" class="btn btn-danger waves-effect" onclick="deleteAptitud(`<?php echo $aptitudc->id_aptitud; ?>`);">
                                    <i class=material-icons>delete_forever</i>
                                </a>


                            </div>
                        </td> -->
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</section>
<button class="btn btn-block wave-effect" role="link" onclick="window.location='<?php echo base_url();?>reclutamiento/aptitudes/getAptitudesCargo/<?php echo $cargoID;?>'">Modificar el perfil de cargo</button>