<?php
defined('BASEPATH') or exit('No direct script access allowed');

class IngresoEgreso extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/IngresoEgreso');
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function Store()
    {
        $this->load->model("IngresoEgreso_model");

        $legajo = $this->input->POST("num_legajo");

        if (null !== $this->input->POST("ingreso")) {
            $info = array(
                'id_legajo' => $legajo,
                'ingreso' => date('Y-m-d H:i'),
            );
        } else if (null !== $this->input->POST("egreso")) {
            $info = array(
                'id_legajo' => $legajo,
                'egreso' => date('Y-m-d H:i:s'),
            );
        } else {
            $info = array(
                'id_legajo' => $legajo,
                'almuerzo' => date('Y-m-d H:i:s'),
            );
        }
        if ($this->IngresoEgreso_model->Save($info)) {
            redirect(base_url() . "control/IngresoEgreso");
        } else {
            $this->session->set_flashdata("error", "No se pudo guardar la informacion");
            redirect(base_url() . "control/IngresoEgreso");
        }
    }

    public function validate($num_legajo)
    {
        $this->load->model("Legajos_model");
        $this->load->model("VacacionesLicencias_model");
        $this->load->model("CalendarioFeriados_model");
        $this->load->model("Novedades_model");

        $legajo = $this->Legajos_model->GetLegajo($num_legajo);
        if (!$this->Legajos_model->GetLegajo($num_legajo)) {
            echo "El legajo ingresado es inexistente.";
        } else {
            if ($this->CalendarioFeriados_model->isFeriado(date('Y-m-d'))) {
                echo "No es posible realizar registros horarios. El dia es feriado.";
                return;
            }

            $vacacion_licencia = $this->VacacionesLicencias_model->GetEventosConFecha($num_legajo, date('Y-m-d'), date('Y-m-d'));
            if ($vacacion_licencia) {
                echo "No es posible realizar registros horarios. Motivo: Persona de " . $vacacion_licencia->tipo;
                return;
            }

            $fecha_ingreso_legajo = DateTime::createFromFormat('Y-m-d', $legajo->fecha_ingreso);
            $hoy = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
            if ($hoy < $fecha_ingreso_legajo) {
                echo "El legajo ingresado todavia no comenzo a trabajar. Su fecha de ingreso es: " . $fecha_ingreso_legajo->format('Y-m-d');
                return;
            }

            $dia_de_la_semana = $this->traducirDia($hoy->format("l"));
            $ingreso_teorico = current($this->Novedades_model->GetIngresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
            $egreso_teorico = current($this->Novedades_model->GetEgresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
            if (($ingreso_teorico == NULL) && ($egreso_teorico == NULL)) {
                echo "No es posible realizar registros horarios. Motivo: Persona de franco.";
                return;
            }
        }
    }

    private function traducirDia($dia)
    {
        switch ($dia) {
            case "Monday":
                return "lunes";
            case "Tuesday":
                return "martes";
            case "Wednesday":
                return "miercoles";
            case "Thursday":
                return "jueves";
            case "Friday":
                return "viernes";
            case "Saturday":
                return "sabado";
            case "Sunday":
                return "domingo";
        }
    }
}
