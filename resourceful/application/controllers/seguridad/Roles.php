<?php
defined('BASEPATH') or exit('No direct script access allowed');

class roles extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("RolesModel");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'roles' => $this->RolesModel->GetRoles()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('roles/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function DeleteRoles($id)
    {
        $data = array(
            "estado" => "0",
        );
        $this->RolesModel->Update($id, $data);
        echo "seguridad/roles";
    }

    public function MostrarPermisos()
    {
        $idrol = $this->input->post("idRol");

        $permisos = $this->RolesModel->GetPermisos($idrol);
        $html = "";

        foreach ($permisos as $permiso) {
            $html .= "<tr>
            <td>" . str_replace('/', ' ⇒ ', $permiso->seccion) . "</td>
            <td><i class=material-icons>". ($permiso->leer == 1 ? 'check_box' : 'check_box_outline_blank') ."</i></td>
            <td><i class=material-icons>". ($permiso->insertar == 1 ? 'check_box' : 'check_box_outline_blank') ."</i></td>
            <td><i class=material-icons>". ($permiso->modificar == 1 ? 'check_box' : 'check_box_outline_blank') ."</i></td>
            <td><i class=material-icons>". ($permiso->baja == 1 ? 'check_box' : 'check_box_outline_blank') ."</i></td>";
        }
        $html .= "</tr>";

        echo $html;
    }
    public function Add()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'secciones' => $this->RolesModel->GetSecciones()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('roles/add', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Guardar()
    {
        $secciones = $this->RolesModel->GetSecciones();
        $rol = $this->input->post("rol");
        $descripcion = $this->input->post("descripcion");
        $this->form_validation->set_rules("rol", "rol", "required|is_unique[roles.nombre]");
        if ($this->form_validation->run()) {
            $info1 = array(
                "nombre" => $rol,
                "descripcion" => $descripcion,
                "estado" => 1
            );
            //agrega el nuevo rol y obtine su id
            $ultimorol = $this->RolesModel->SaveRol($info1);
            //METODO PARA CARGAR NUEVOS PERMISOS 
            foreach ($secciones as $seccion) {
                $info2 = array(
                    "id_rol" => $ultimorol,
                    "id_seccion" => $seccion->id,
                    "leer" => ($this->input->post("leer" . $seccion->id) == null) ? 0 : 1,
                    "insertar" => ($this->input->post("insertar" . $seccion->id) == null) ? 0 : 1,
                    "modificar" => ($this->input->post("modificar" . $seccion->id) == null) ? 0 : 1,
                    "baja" => ($this->input->post("baja" . $seccion->id) == null) ? 0 : 1
                );
                $this->RolesModel->SavePermisos($info2);
            }

            redirect(base_url() . "seguridad/roles");
        } else {
            $this->add();
        }
    }
    public function Edit($id)
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'roles' => $this->RolesModel->GetRol($id),
                'secciones' => $this->RolesModel->GetSecciones(),
                'permisos' => $this->RolesModel->GetPermisos($id)
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('roles/edit', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Update()
    {
        $secciones = $this->RolesModel->GetSecciones();
        $rol = $this->input->post("rol");
        $idrol = $this->input->post("idrol");
        $descripcion = $this->input->post("descripcion");
        // ARREGLAR ESTA VALIDACION URGENTE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        $rolActual = $this->RolesModel->GetRol($idrol);
        if ($rol == $rolActual->nombre) {
            $unique = '';
        } else {
            $unique = '|is_unique[roles.nombre]';
        }
        $this->form_validation->set_rules("rol", "rol", "required" . $unique);
        if ($this->form_validation->run()) {
            $info1 = array(
                "nombre" => $rol,
                "descripcion" => $descripcion
            );

            $this->RolesModel->Update($idrol, $info1);
            $this->RolesModel->DeletePermisos($idrol);
            //METODO PARA CARGAR NUEVOS PERMISOS 
            foreach ($secciones as $seccion) {
                $info2 = array(
                    "id_rol" => $idrol,
                    "id_seccion" => $seccion->id,
                    "leer" => ($this->input->post("leer" . $seccion->id) == null) ? 0 : 1,
                    "insertar" => ($this->input->post("insertar" . $seccion->id) == null) ? 0 : 1,
                    "modificar" => ($this->input->post("modificar" . $seccion->id) == null) ? 0 : 1,
                    "baja" => ($this->input->post("baja" . $seccion->id) == null) ? 0 : 1
                );
                $this->RolesModel->SavePermisos($info2);
            }

            redirect(base_url() . "seguridad/roles");
        } else {
            $this->Edit($idrol);
        }
    }
}
