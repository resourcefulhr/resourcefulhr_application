<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cargos_model extends CI_Model
{
    public function GetCargos()
    {
        $this->db->select("c.*, a.nombre_area as areaname")
            ->from("cargos c")
            ->join("areas a", "c.id_area = a.id")
            ->where("c.estado", "1");
        $resultados = $this->db->get();
        return $resultados->result();
    }
    public function Update($id, $info)
    {
        $this->db->where("id", $id);
        return $this->db->update("cargos", $info);
    }
    public function GetCargo($id)
    {
        $this->db->select("*")
            ->from("cargos")
            ->where("estado", "1")
            ->where("id", $id);
        $resultados = $this->db->get();
        return $resultados->row();
    }
    public function Save($info)
    {
        $this->db->insert('cargos', $info);
        return $this->db->insert_id();
    }
}
