<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Bienvenido a Resourceful</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>/front/images/favicon.png" type="image/png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>front/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>front/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>front/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>front/css/style.css" rel="stylesheet">
</head>

<body class="login-page login-bg">
    <div class="login-box">
        <div class="logo">
            <a href="#">Bienvendidos a <br><b>Resourceful</b></a>
            <small>sistema de gestion de recursos humanos</small>
        </div>
        <div class="card">
            <div class="body">
                <form action="<?php echo base_url(); ?>login/login" method="POST">
                    <div class="msg">¿Listo para iniciar sesion?</div>
                    <?php if ($this->session->flashdata("data_invalida")) : ?>
                        <div class="alert alert-danger">
                            <p><?php echo $this->session->flashdata("data_invalida") ?></p>
                        </div>
                    <?php endif; ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line focused<?php echo form_error("usuario") ? ' error' : ''; ?>">
                            <input type="text" class="form-control" name="usuario" placeholder="Usuario" required autofocus value="<?php echo set_value("usuario"); ?>"> </div>
                    </div>
                    <?php echo form_error("usuario", "<span class='help-block'>", "</span>"); ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line focused<?php echo form_error("contraseña") ? 'has-error' : ''; ?>">
                            <input type="password" class="form-control" name="contraseña" placeholder="Contraseña" required autofocus value="<?php echo set_value("contraseña"); ?>">
                        </div>
                        <?php echo form_error("contraseña", "<span class='help-block'>", "</span>"); ?>
                    </div>
                    <div class="input-group">
                        <button class="btn btn-block btn-login waves-effect" type="submit"><b>Iniciar sesion</b></button>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="<?php echo base_url(); ?>login/signup">Registrate!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="<?php echo base_url(); ?>login/recover">Recuperar Contraseña</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>front/js/admin.js"></script>
</body>

</html>