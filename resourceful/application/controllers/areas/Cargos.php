<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cargos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("Cargos_model");
        $this->load->model("Areas_model");
        $this->load->model("Aptitudes_model");
    }
    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                "cargos" => $this->Cargos_model->GetCargos()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('cargos/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Delete($id)
    {
        $info  = array(
            'estado' => "0"
        );
        $this->Cargos_model->Update($id, $info);
        echo "areas/cargos";
    }

    public function Add()
    {
        $info = array(
            //'cargos' => $this->Cargos_model->GetCargo(1),
            'areas' => $this->Areas_model->GetAreas(),
            'aptitudes' => $this->Aptitudes_model->GetAptitudes()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('cargos/add', $info);
        $this->load->view('layouts/footer');
    }
    public function Edit($id)
    {
        $info = array(
            'cargo' => $this->Cargos_model->GetCargo($id),
            'areas' => $this->Areas_model->GetAreas()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('cargos/edit', $info);
        $this->load->view('layouts/footer');
    }
    //MODIFICAR DE ACA PARA ABAJO
    public function Store()
    {
        $cargo = $this->input->POST("cargo");
        $descripcion = $this->input->POST("descripcion");
        $tareas = $this->input->POST("tareas");
        ///     APTITUDES
        $idAptitud = $this->input->POST("aptitud_car");
        $nivelAptitud = $this->input->POST("nivel_car");
        $excluyente = $this->input->POST("excluyente_car");
        $this->form_validation->set_rules("cargo", "Cargo", "required|is_unique[cargos.cargo]");
        if ($this->form_validation->run()) {
            $info = array(
                'cargo' => $cargo,
                'descripcion' => $descripcion,
                'tareas' => $tareas,
                'estado' => '1',
                'id_area' => $this->input->POST("area")
            );
            $idUltimoCargo = $this->Cargos_model->Save($info);
            $this->save_aptitud($idAptitud, $nivelAptitud, $excluyente, $idUltimoCargo);
            redirect(base_url() . "areas/cargos");
        } else {
            $this->Add();
        }
    }
    public function save_aptitud($idAptitud, $nivelAptitud, $excluyente, $idUltimoCargo)
    {
        for ($i = 0; $i < count($idAptitud); $i++) {
            $data  = array(
                'id_aptitud' => $idAptitud[$i],
                'id_cargo' => $idUltimoCargo,
                'nivel' => $nivelAptitud[$i],
                'excluyente' => $excluyente[$i],
            );
            $this->Aptitudes_model->saveAptitudCargo($data);
        }
    }
    public function Update()
    {
        $idCargo = $this->input->POST("id_cargo");
        $cargo = $this->input->POST("cargo");
        $descripcion = $this->input->POST("descripcion");
        $tareas = $this->input->POST("tareas");
        $this->form_validation->set_rules("area", "area", "required");

        $cargoActual = $this->Cargos_model->GetCargo($idCargo);

        if ($cargo == $cargoActual->cargo) {
            $unique = '';
        } else {
            $unique = '|is_unique[cargos.cargo]';
        }
        $this->form_validation->set_rules("cargo", "cargo", "required" . $unique);
        if ($this->form_validation->run()) {
            $info1 = array(
                "cargo" => $cargo,
                "descripcion" => $descripcion,
                "tareas" => $tareas,
                'estado' => '1',
                'id_area' => $this->input->POST("area")
            );
            $this->Cargos_model->Update($idCargo, $info1);
            redirect(base_url() . "areas/cargos");
        } else {
            $this->Edit($idCargo);
        }
    }
}
