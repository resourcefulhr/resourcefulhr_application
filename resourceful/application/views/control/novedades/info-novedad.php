<div class="modal-body">
    <section>
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="bg-light-blue">
                    <img src="../front/images/aa/1.png" alt="">
                    <br>
                    <b>Alquileres Argentia S.A</b><br>
                    <b>Informe de Novedad</b><br>
                    <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
                    <br>
                </div>
            </div>
        </div>
        <hr>
    </section>
    <section>
        <!-- COMIENZO INFORMACION DE NOVEDAD -->
        <div class="row text-center">
            <h3><b>Informacion</b></h3>
            <div class="col-xs-4">
                <label class="input-group ">Numero de Legajo:
                    <br>
                    <p><?php echo $novedad->num_legajo; ?></p>
                </label>
                <label class="input-group ">DNI:
                    <br>
                    <p><?php echo $novedad->numero_documento; ?></p>
                </label>
                <label class="input-group ">Tipo de Novedad:
                    <br>
                    <p><?php echo $novedad->tipo; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">Nombre:
                    <br>
                    <p><?php echo $novedad->nombre; ?></p>
                </label>
                <label class="input-group ">Area:
                    <br>
                    <p><?php echo $novedad->nombre_area; ?></p>
                </label>
                <label class="input-group ">Estado:
                    <br>
                    <p><?php echo $novedad->estado; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">Apellido:
                    <br>
                    <p><?php echo $novedad->apellido; ?></p>
                </label>
                <label class="input-group ">Cargo:
                    <br>
                    <p><?php echo $novedad->cargo; ?></p>
                </label>
                <label class="input-group ">Dia:
                    <br>
                    <p><?php echo $novedad->dia; ?></p>
                </label>
            </div>
        </div>
        <!-- FIN INFORMACION DE NOVEDAD -->
        <hr>
        <!-- COMIENZO HORARIOS -->
        <div class="row text-center">
            <div class="col-xs-6 text-center" style="border-right: 1px solid #eee;">
                <h3><b>Horario Teorico</b></h3>
                <div class="row">
                    <div class="col-xs-6">
                        <label for="ingreso_teorico">Ingreso
                            <br>
                            <p id="ingreso_teorico"><?php echo $novedad->ingreso_teorico; ?></p>
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <label for="egreso_teorico">Egreso
                            <br>
                            <p id="egreso_teorico"><?php echo $novedad->egreso_teorico; ?></p>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 text-center">
                <h3><b>Horario Real</b></h3>
                <div class="row">
                    <div class="col-xs-6">
                        <label for="ingreso_real">Ingreso
                            <br>
                            <?php foreach($registros as $registro): ?>
                            <?php if ($registro->ingreso != NULL): ?>
                            <p><?php echo $registro->ingreso; ?></p>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <label for="egreso_real">Egreso
                            <br>
                            <?php foreach($registros as $registro): ?>
                            <?php if ($registro->egreso != NULL): ?>
                            <p><?php echo $registro->egreso; ?></p>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN HORARIOS -->
        <hr>
        <!-- INICIO DE RESOLUCION -->
        <?php if($novedad->estado != "ABIERTA"): ?>
        <div class="row">
            <h3 class="text-center"><b>Resolucion</b></h3>
            <div class="col-xs-6">
                <label for="resolucion">Resolucion: </label>
                <input class="form-control" disabled value="<?php echo $novedad->estado; ?>">
            </div>
            <div class="col-xs-6">
                <label>Horario Ajustado: </label>
                <?php if(!$novedad->descuenta_dia): ?>
                <div class="row" id="ajuste-horario">
                    <div class="col-xs-6">
                        <input type="text" class="form-control" disabled value='Ingreso: <?php echo $novedad->ingreso_ajustado; ?>'></input>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" disabled value='Egreso: <?php echo $novedad->egreso_ajustado; ?>'></input>
                    </div>
                </div>
                <?php else: ?>
                <div class="row" id="ajuste-horario">
                    <div class="col-xs-12 text-center">
                        <input id="ingreso-ajutado" type="text" class="form-control" disabled value="DIA DESCONTADO"></input>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
        <!-- FIN DE RESOLUCION -->
    </section>
</div>
<div class="modal-footer">
    <button type="submit" onclick="imprimir()" class="btn btn-lg btn-success btn-assign pull-left">Imprimir</button>
    <button type="button" class="btn btn-lg btn-danger pull-right" data-dismiss="modal">Cancelar</button>
</div>

<script>
function imprimir() {
    $("#modal-novedades .modal-body").print({});
    $("#modal-novedades").modal("hide");
}
</script>