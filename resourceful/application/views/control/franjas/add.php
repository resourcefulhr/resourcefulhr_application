<script type="text/javascript" src="<?php echo base_url(); ?>front/plugins/timerangecombo/TimeRangeCombo.js"></script>
<section class="content">
    <h1>
        Franjas Horarias
        <small>Nueva</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>control/FranjasHorarias/Store" method="POST">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('nombre') == true ? ' error' : '' ?>">
                        <label>Nombre:</label>
                        <input type="text" name="nombre" class="form-control" required value="<?php echo set_value("nombre"); ?>">
                    </div>
                    <?php echo form_error("nombre", "<span class='help-block'>", "</span>"); ?>
                </div>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Lunes</th>
                            <th>Martes</th>
                            <th>Miercoles</th>
                            <th>Jueves</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label for="lunes_ingreso">Ingreso:</label>
                                <select class="form-control" id="lunes_ingreso" name="lunes_ingreso"></select>
                                <label for="lunes_egreso">Egreso:</label>
                                <select class="form-control" id="lunes_egreso" name="lunes_egreso"></select>
                            </td>
                            <td>
                                <label for="martes_ingreso">Ingreso:</label>
                                <select class="form-control" id="martes_ingreso" name="martes_ingreso"></select>
                                <label for="martes_egreso">Egreso:</label>
                                <select class="form-control" id="martes_egreso" name="martes_egreso"></select>
                            </td>
                            <td>
                                <label for="miercoles_ingreso">Ingreso:</label>
                                <select class="form-control" id="miercoles_ingreso" name="miercoles_ingreso"></select>
                                <label for="miercoles_egreso">Egreso:</label>
                                <select class="form-control" id="miercoles_egreso" name="miercoles_egreso"></select>
                            </td>
                            <td>
                                <label for="jueves_ingreso">Ingreso:</label>
                                <select class="form-control" id="jueves_ingreso" name="jueves_ingreso"></select>
                                <label for="jueves_egreso">Egreso:</label>
                                <select class="form-control" id="jueves_egreso" name="jueves_egreso"></select>
                            </td>
                        </tr>
                    </tbody>
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Viernes</th>
                            <th>Sabado</th>
                            <th>Domingo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <label for="viernes_ingreso">Ingreso:</label>
                                <select class="form-control" id="viernes_ingreso" name="viernes_ingreso"></select>
                                <label for="viernes_egreso">Egreso:</label>
                                <select class="form-control" id="viernes_egreso" name="viernes_egreso"></select>
                            </td>
                            <td>
                                <label for="sabado_ingreso">Ingreso:</label>
                                <select class="form-control" id="sabado_ingreso" name="sabado_ingreso"></select>
                                <label for="sabado_egreso">Egreso:</label>
                                <select class="form-control" id="sabado_egreso" name="sabado_egreso"></select>
                            </td>
                            <td>
                                <label for="domingo_ingreso">Ingreso:</label>
                                <select class="form-control" id="domingo_ingreso" name="domingo_ingreso"></select>
                                <label for="domingo_egreso">Egreso:</label>
                                <select class="form-control" id="domingo_egreso" name="domingo_egreso"></select>
                            </td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <div class="form-line focused">
                        <label>Tolerancia (minutos):</label>
                        <input type="number" name="tolerancia" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-line focused">
                        <label>Tiempo Almuerzo (minutos):</label>
                        <input type="number" name="tiempo_almuerzo" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>control/FranjasHorarias" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
var comboLunes = new TimeRangeCombo("lunes_ingreso", "lunes_egreso");
var comboMartes = new TimeRangeCombo("martes_ingreso", "martes_egreso");
var comboMiercoles = new TimeRangeCombo("miercoles_ingreso", "miercoles_egreso");
var comboJueves = new TimeRangeCombo("jueves_ingreso", "jueves_egreso");
var comboViernes = new TimeRangeCombo("viernes_ingreso", "viernes_egreso");
var comboSabado = new TimeRangeCombo("sabado_ingreso", "sabado_egreso");
var comboDomingo = new TimeRangeCombo("domingo_ingreso", "domingo_egreso");
</script>