<section class="content">
    <h1>
        Cargos
        <small>Editar</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>areas/Cargos/Update" method="POST">
                <input type="hidden" name="id_cargo" value="<?php echo $cargo->id; ?>">
                <div class="form-group">
                    <label>Cargo:</label>
                    <div class="form-line focused<?php echo form_error('cargo') == true ? ' error' : '' ?>">
                        <input type="text" name="cargo" class="form-control" required value="<?php echo !empty(set_value("cargo")) ? set_value("cargo") : $cargo->cargo; ?>">
                    </div>
                    <?php echo form_error("cargo", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <label>Descripcion:</label>
                    <div class="form-line focused">
                        <input type="text" name="descripcion" class="form-control" required value="<?php echo !empty(set_value("descripcion")) ? set_value("descripcion") : $cargo->descripcion; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Tareas:</label>
                    <div class="form-line focused">
                        <input type="text" name="tareas" class="form-control" required value="<?php echo !empty(set_value("tareas")) ? set_value("tareas") : $cargo->tareas; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Area:</label>
                    <select name="area" id="area" class="form-control">
                        <?php foreach ($areas as $area) : ?>
                            <?php if ($area->id == $area->id) : ?>
                                <option value="<?php echo $area->id ?>" selected><?php echo $area->nombre_area; ?></option>
                            <?php else : ?>
                                <?php echo $area->nombre_area; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_error("nombre", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>areas/cargos" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</section>