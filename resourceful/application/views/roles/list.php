<section class="content">
    <h1>
        Roles
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <?php if(in_array("Seguridad -> Roles", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <div class="input-group">
                    <a href="<?php echo base_url(); ?>seguridad/roles/Add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                        </i> Agregar Rol</a>
                </div>
            </div>
            <hr>
            <?php endif; ?>

            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>#</th>
                            <th>Rol</th>
                            <th>Descripcion</th>
                            <th>Permisos</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($roles)) : ?>
                            <?php foreach ($roles as $rol) : ?>
                                <tr>
                                    <td><?php echo $rol->id; ?></td>
                                    <td><?php echo $rol->nombre; ?></td>
                                    <td><?php echo $rol->descripcion; ?></td>

                                    <td>
                                        <div class="btn-gruop">
                                            <button type="button" id="ver-permisos" class="btn btn-info waves-effect ver-permiso" data-toggle="modal" data-target="#defaultModal" value="<?php echo $rol->id; ?>">
                                                <i class=material-icons>search</i>
                                            </button>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="btn-group">
                                            <?php if(in_array("Seguridad -> Roles", $this->session->userdata("permisos_modificar"))): ?>
                                            <a href="<?php echo ($rol->id < 3) ? "#" : base_url() . "seguridad/roles/Edit/" . $rol->id; ?>" class="btn btn-warning waves-effect" <?php if($rol->id < 3) echo "disabled"; ?>><i class=material-icons>mode_edit</i></a>
                                            <?php endif; ?>

                                            <?php if(in_array("Seguridad -> Roles", $this->session->userdata("permisos_baja"))): ?>
                                            <a href="<?php echo ($rol->id < 3) ? "#" : base_url() . "seguridad/roles/DeleteRoles/" . $rol->id; ?>" class="btn btn-danger btn-remove waves-effect" <?php if($rol->id < 3) echo "disabled"; ?>><i class=material-icons>delete_forever</i></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue-grey">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Permisos del rol</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Seccion</th>
                            <th>Ver</th>
                            <th>Cargar</th>
                            <th>Modificar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody id="body">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var base_url = "<?php echo base_url(); ?>";
        $(".ver-permiso").on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "seguridad/roles/MostrarPermisos",
                data: {
                    idRol: id
                },
                type: "POST",
                success: function(resp) {
                    document.getElementById("body").innerHTML = resp;
                }
            });
        });
    });
</script>