<section class="content">

  <h1>
    Busqueda
    <small>Manual</small>
  </h1>

  <body>
    <div class="col-sm-12">
      <!-- agregar el atributo hidden y despues sacar a la hora de buscar que se dibuje-->
      <input type="hidden" id="tipoReclutamiento" value="<?php echo $tipoReclutamiento; ?>">
      <div class="card hidden" id="resultCard">
        <div>
          <h2 class="header bg-teal text-center">Resultados de la busqueda</h2>
        </div>
        <div class="row">
          <div class="body">
            <table class="table">
              <thead>
                <tr class="bg-blue-grey">
                  <th>Apellido</th>
                  <th>Nombre</th>
                  <th>Puntuancion</th>
                  <th class="col-md-2">Opciones</th>
                </tr>
              </thead>
              <tbody id="contenido">
              </tbody>
            </table>
            <hr>
            <div style="padding-left:33.33%">
              <div class="btn-group" role="group">
                <a type="button" class="btn btn-success wave-efect" href="buscar"><i class="material-icons">search</i> Volver a : Tipo de busquedas </a>
                <a type="button" class="btn btn-danger wave-efect" href="buscar2"><i class="material-icons">post_add</i> Volver a : Busqueda manual </a>
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
    </div>
</section>
</body>
<script>
  $(document).ready(function() {
    $('#resultCard').removeClass('hidden');
    var cargoID = 19;
    var tipoReclutamiento = $('#tipoReclutamiento').val();
    var contenido = document.querySelector('#contenido');
    contenido.innerHTML = '';
    var sharp = 0;
    parametros = {
      'id': cargoID,
      'tipoReclutamiento': tipoReclutamiento,
      'tipoBusqueda': 'Manual'
    };
    var colorColum = 0,
      c = 0;
    $.ajax({
      url: "BusquedaManual",
      type: "GET",
      data: parametros,
      dataType: "json",
      success: function(resp) {
        for (let i of resp) {
          MENOR = (i.totalAP / 3);
          MENOR2 = (i.totalAP / 3) * 2;
          MENOR >= i.suma ? colorFila = 'bg-orange' : MENOR2 >= i.suma ? colorFila = 'bg-green' : colorFila = 'bg-teal';
          if (tipoReclutamiento == '2') {
            sharp = i.id_curriculum;
            contenido.innerHTML += `
                <tr id='${i.id_curriculum}'>
                  <td>${i.apellido}</td>
                  <td>${i.nombre}</td>
                  <td>${i.suma}</td>
                  <td>
                    <div class="btn-group">
                      <a type="button" class="btn btn-block btn-lg bg-grey dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                          Opciones</span>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>reclutamiento/candidatos" class="btn btn-block btn-lg bg-blue-grey waves-effect"><i class="material-icons">system_update</i>Ir al perfil</a></li>
                        <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesCurriculum/${i.id_curriculum}" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>`;
          } else {
            sharp = i.id_legajo;
            contenido.innerHTML += `
                <tr id='${i.id_legajo}'>
                  <td>${i.apellido}</td>
                  <td>${i.nombre}</td>
                  <td>${i.suma}</td>
                  <td>
                    <div class="btn-group">
                      <a type="button" class="btn btn-block btn-lg bg-grey dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                          Opciones</span>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesLegajo/${i.id_legajo}" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>`;
          }
          $('#' + sharp).addClass(colorFila);
        }
        $.ajax({
          url: 'clearTableTemp',
          type: "POST",
          success: function(resp) {
            resp = resp;
          }
        })
      }
    })
  });
</script>