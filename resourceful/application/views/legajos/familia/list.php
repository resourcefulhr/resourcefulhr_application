<section class="content">
    <h1>
        Familiares
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <div class="input-group">
                <a href="<?php echo base_url(); ?>areas/legajos/AddFamiliar/<?php echo $id_legajo; ?>" class="btn btn-success waves-effect"><i class=material-icons>add_box
                    </i> Agregar Familiar</a>
            </div>
            <hr>
            <div>
                <input type="hidden" value="<?php echo $id_legajo; ?>" id="idLeg">
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Parentesco</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>fecha de nacimiento</th>
                            <th>Domicilio</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($familia)) : ?>
                            <?php foreach ($familia as $family) : ?>
                                <tr>
                                    <td><?php echo $family->parentesco; ?></td>
                                    <td><?php echo $family->nombre; ?></td>
                                    <td><?php echo $family->apellido; ?></td>
                                    <td><?php echo $family->fecha_nacimiento; ?></td>
                                    <td><?php echo $family->domicilio; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?php echo base_url(); ?>areas/legajos/EditFamiliar/<?php echo $family->id; ?>" class="btn btn-warning waves-effect"><i class=material-icons>mode_edit</i></a>
                                            <button value="<?php echo $family->id; ?>" id="reload" class="btn btn-danger waves-effect remove-family"><i class=material-icons>delete_forever</i></button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        var base_url = "<?php echo base_url(); ?>";
        $(".remove-family").on("click", function() {
            leg = $('#idLeg').val();
            legFam = $('#reload').val();
            parametro = {
                'id': leg,
                'familiarID': legFam,
            };
            $.ajax({
                data: parametro,
                url: base_url + "areas/legajos/DeleteFamiliar",
                type: "GET",
                success: function(resp) {
                    // document.location.href  tiene la url actual completa y le concatenamos el 1
                    document.location.href = leg;
                }
            });
        });
    });
</script>