<section class="content">
    <h1>
        Area
        <small>Nuevo</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>areas/area/Store" method="POST">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('area') == true ? ' error' : '' ?>">
                        <label>Nombre del area:</label>
                        <input type="text" name="area" class="form-control" required value="<?php echo set_value("area"); ?>">
                    </div>
                    <?php echo form_error("area", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <div class="form-line focused">
                        <label>Descripcion del area:</label>
                        <input type="text" name="descripcion" class="form-control" value="<?php echo set_value("descripcion"); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Area dependiente:</label>
                    <select name="depencdencia" id="depencdencia" class="form-control" required>
                        <option value="">Seleccione...</option>
                        <?php foreach ($areas as $area) : ?>
                            <option value="<?php echo $area->id ?>" <?php echo set_select("depencdencia", $area->id) ?>>
                                <?php echo $area->nombre_area; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>areas/area" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>

        </div>
    </div>
</section>