<section class="content">
    <h1>
        Areas
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">

            <?php if(in_array("Aplicacion -> Areas", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <div class="input-group">
                    <a href="<?php echo base_url(); ?>areas/area/Add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                        </i> Agregar Area</a>
                </div>
            </div>
            <hr>
            <?php endif; ?>

            <?php if ($this->session->flashdata("ERROR")) : ?>
                <div class="alert alert-danger">
                    <p><?php echo $this->session->flashdata("ERROR") ?></p>
                </div>
                <hr>
            <?php endif; ?>
            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>#</th>
                            <th>Area</th>
                            <th>Descripcion</th>
                            <th>Dependencia</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($areas)) : ?>
                            <?php foreach ($areas as $area) : ?>
                                <tr>
                                    <td><?php echo $area->id; ?></td>
                                    <td><?php echo $area->nombre_area; ?></td>
                                    <td><?php echo $area->descripcion; ?></td>
                                    <td>
                                        <!-- Esto es para mostrar la dependencia de la misma area -->
                                        <?php foreach ($areas as $areaID) : ?>
                                            <?php if ($areaID->id == $area->id_area) :
                                                            echo  "$areaID->nombre_area";
                                                            break ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info waves-effect" data-toggle="modal" data-target="#modal-default" value="<?php echo $area->id; ?>" disabled>
                                                <i class=material-icons>search</i>
                                            </button>
                                            <?php if(in_array("Aplicacion -> Areas", $this->session->userdata("permisos_modificar"))): ?>
                                            <a href="<?php echo base_url(); ?>areas/area/Edit/<?php echo $area->id; ?>" class="btn btn-warning waves-effect"><i class=material-icons>mode_edit</i></a>
                                            <?php endif; ?>

                                            <?php if(in_array("Aplicacion -> Areas", $this->session->userdata("permisos_baja"))): ?>
                                            <a href="<?php echo base_url(); ?>areas/area/DeleteArea/<?php echo $area->id; ?>" class="btn btn-danger waves-effect"><i class=material-icons>delete_forever</i></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>