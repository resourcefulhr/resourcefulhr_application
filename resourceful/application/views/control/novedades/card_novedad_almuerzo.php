<?php foreach($novedades as $novedad): ?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card">
        <div class="header <?php echo getCardColor($novedad->estado); ?>">
            <h2 class="text-center">
            <?php echo $novedad->tipo . " <br> " . $novedad->estado; ?> <small><?php echo $novedad->dia; ?></small>
            </h2>
            <ul class="header-dropdown m-r--5">
                <li class="dropdown">
                    <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <?php if($novedad->estado == "ABIERTA" && in_array("Control -> Novedades", $this->session->userdata("permisos_modificar"))): ?>
                        <li><a class=" waves-effect waves-block" onclick="justificarNovedadAlmuerzo(`<?php echo $novedad->id; ?>`);">Justificar</a></li>
                        <li><a class=" waves-effect waves-block" onclick="injustificarNovedadAlmuerzo(`<?php echo $novedad->id; ?>`);">NO Justificar</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="row">
                <div class="col-xs-6">
                    <label for="num_legajo">Numero de Legajo:</label>
                    <p id="num_legajo"><?php echo $novedad->num_legajo; ?></p>
                </div>
                <div class="col-xs-6">
                    <label for="nombre">Nombre:</label>
                    <p id="nombre"><?php echo $novedad->nombre . " " . $novedad->apellido; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <label for="area">Area:</label>
                    <p id="area"><?php echo $novedad->nombre_area; ?></p>
                </div>
                <div class="col-xs-6">
                    <label for="cargo">Cargo:</label>
                    <p id="cargo"><?php echo $novedad->cargo; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <label for="area">Tiempo Disponible:</label>
                    <p id="area"><?php echo $novedad->tiempo_almuerzo; ?></p>
                </div>
                <div class="col-xs-6">
                    <label for="cargo">Tiempo tomado:</label>
                    <p id="cargo"><?php echo ($novedad->tiempo_almuerzo_tomado != NULL) ? $novedad->tiempo_almuerzo_tomado : "-"; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>
<input type="hidden" id="novedades-almuerzo-count" value=<?php echo sizeOf($novedades); ?>>

<?php
function getCardColor($estado) {
    switch($estado) {
        case "ABIERTA":
            return "bg-orange";
        case "JUSTIFICADA":
            return "bg-teal";
        case "NO JUSTIFICADA":
            return "bg-red";
    }
}