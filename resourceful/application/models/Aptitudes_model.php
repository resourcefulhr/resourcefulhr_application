<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aptitudes_model extends CI_Model
{

    public function GetAptitudes()
    {
        $resultado = $this->db->get("aptitudes");
        return $resultado->result();
    }
    public function GetAptitudesC($id)
    {
        $resultado = $this->db->query("SELECT * FROM aptitudes WHERE id NOT IN (SELECT id_aptitud FROM aptitudes_cargos WHERE id_cargo = $id)");
        return $resultado->result();
    }
    public function GetAptitudesL($id)
    {
        $resultado = $this->db->query("SELECT * FROM aptitudes WHERE id NOT IN (SELECT id_aptitud FROM aptitudes_legajos WHERE id_legajo = $id)");
        return $resultado->result();
    }
    public function GetAptitudesCu($id)
    {
        $resultado = $this->db->query("SELECT * FROM aptitudes WHERE id NOT IN (SELECT id_aptitud FROM aptitudes_curriculums WHERE id_curriculum = $id)");
        return $resultado->result();
    }
    public function Save($info)
    {
        return $this->db->insert("aptitudes", $info);
    }
    public function DeleteAptitud($id)
    {
        $this->db->where('aptitudes.id', $id);
        $this->db->delete('aptitudes');
    }
    //te borra todas las relaiones de cargos con aptitudes que tenga esa aptitud
    public function DeletesAptitudCargo($id)
    {
        $this->db->where('aptitudes_cargos.id_aptitud', $id);
        $this->db->delete('aptitudes_cargos');
    }
    public function Existe($aptitud)
    {
        $this->db->select("*")
            ->from("aptitudes")
            ->where("aptitud", $aptitud);
        $resultados = $this->db->get();
        return $resultados->result();
    }
    public function GetAptitud($id)
    {
        $this->db->where("a.id", $id);
        $resultado = $this->db->get("aptitudes a");
        return $resultado->row();
    }
    public function Update($info, $id)
    {
        $this->db->where("aptitudes.id", $id);
        return $this->db->update("aptitudes", $info);
    }
    public function saveAptitudCargo($info)
    {
        return $this->db->insert("aptitudes_cargos", $info);
    }
    public function saveAptitudLegajo($info)
    {
        return $this->db->insert("aptitudes_legajos", $info);
    }
    public function saveAptitudCurriculums($info)
    {
        return $this->db->insert("aptitudes_curriculums", $info);
    }
    //////////////aptitud cargo
    public function GetAptitudesCargo($id)
    {
        $this->db->select("ac.*, a.aptitud");
        $this->db->from("aptitudes_cargos ac");
        $this->db->join("aptitudes a", " ac.id_aptitud = a.id ");
        $this->db->where("ac.id_cargo", $id);
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function GetAptitudesLegajo($id)
    {
        $this->db->select("al.*, a.aptitud");
        $this->db->from("aptitudes_legajos al");
        $this->db->join("aptitudes a", " al.id_aptitud = a.id ");
        $this->db->where("al.id_legajo", $id);
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function GetAptitudesCurriculum($id)
    {
        $this->db->select("ac.*, a.aptitud");
        $this->db->from("aptitudes_curriculums ac");
        $this->db->join("aptitudes a", " ac.id_aptitud = a.id ");
        $this->db->where("ac.id_curriculum", $id);
        $resultado = $this->db->get();
        return $resultado->result();
    }
    //este te borra una apititud relacionada a ese cargo
    public function DeleteAptitudCargo($idCargo, $idAptitud)
    {
        $this->db->where('aptitudes_cargos.id_cargo', $idCargo);
        $this->db->where('aptitudes_cargos.id_aptitud', $idAptitud);
        $this->db->delete('aptitudes_cargos');
    }
    public function DeleteAptitudLegajo($idLegajo, $idAptitud)
    {
        $this->db->where('aptitudes_legajos.id_legajo', $idLegajo);
        $this->db->where('aptitudes_legajos.id_aptitud', $idAptitud);
        $this->db->delete('aptitudes_legajos');
    }
    public function deleteAptitudCurriculum($idcurriculum, $idAptitud)
    {
        $this->db->where('aptitudes_curriculums.id_curriculum', $idcurriculum);
        $this->db->where('aptitudes_curriculums.id_aptitud', $idAptitud);
        $this->db->delete('aptitudes_curriculums');
    }
    public function GetAptitudCargo($idCargo, $idAptitud)
    {
        $this->db->where('aptitudes_cargos.id_cargo', $idCargo);
        $this->db->where('aptitudes_cargos.id_aptitud', $idAptitud);
        $resultado = $this->db->get('aptitudes_cargos');
        return $resultado->row();
    }
    public function GetAptitudLegajo($idLegajo, $idAptitud)
    {
        $this->db->where('aptitudes_legajos.id_legajo', $idLegajo);
        $this->db->where('aptitudes_legajos.id_aptitud', $idAptitud);
        $resultado = $this->db->get('aptitudes_legajos');
        return $resultado->row();
    }
    public function GetAptitudCurriculum($idcurriculum, $idAptitud)
    {
        $this->db->where('aptitudes_curriculums.id_curriculum', $idcurriculum);
        $this->db->where('aptitudes_curriculums.id_aptitud', $idAptitud);
        $resultado = $this->db->get('aptitudes_curriculums');
        return $resultado->row();
    }
    public function updateAptitudCargo($data, $idCargo, $idAptitud)
    {
        $this->db->where("aptitudes_cargos.id_cargo", $idCargo);
        $this->db->where("aptitudes_cargos.id_aptitud", $idAptitud);
        $this->db->update("aptitudes_cargos", $data);
    }
    public function updateAptitudLegajo($data, $idLegajo, $idAptitud)
    {
        $this->db->where("aptitudes_legajos.id_legajo", $idLegajo);
        $this->db->where("aptitudes_legajos.id_aptitud", $idAptitud);
        $this->db->update("aptitudes_legajos", $data);
    }
    public function updateAptitudCurriculum($data, $idcurriculum, $idAptitud)
    {
        $this->db->where("aptitudes_curriculums.id_curriculum", $idcurriculum);
        $this->db->where("aptitudes_curriculums.id_aptitud", $idAptitud);
        $this->db->update("aptitudes_curriculums", $data);
    }
    public function saveAptitudManual($info)
    {
        return $this->db->insert("temp_busqueda_manual", $info);
    }
}
