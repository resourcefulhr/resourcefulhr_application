<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidatos_model extends CI_Model
{
    public function GetCVs()
    {
        $resultados = $this->db->select("*")
            ->get("candidatos")
            ->result();
        return $resultados;
    }
    public function AddCV($info)
    {
        return $this->db->insert('candidatos', $info);
    }

    public function GetCargo($id)
    {
        $this->db->select("*")
            ->from("cargos")
            ->where("estado", "1")
            ->where("id", $id);
        $resultados = $this->db->get();
        return $resultados->row();
    }
    public function Save($info)
    {
        return $this->db->insert('cargos', $info);
    }
    public function GetRutaLocalCV($id)
    {
        return $ruta = $this->db->select('url_cv')
            ->where('id', $id)
            ->get('candidatos')
            ->row();
    }
    public function DeleteCV($id)
    {
        return $this->db->where('candidatos.id', $id)
            ->delete('candidatos');
    }

    public function GetCV($id)
    {
        return $this->db->select("*")
            ->where('id', $id)
            ->get("candidatos")
            ->row();
    }
    public function UpdateCV($id, $info)
    {
        return $this->db->where("id", $id)
            ->update("candidatos", $info);
    }
}
