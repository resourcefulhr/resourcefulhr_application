<section class="content">
    <h1>
        Cargos
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <?php if(in_array("Aplicacion -> Cargos", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <a href="<?php echo base_url(); ?>areas/cargos/add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                    </i> Agregar Cargo</a>
            </div>
            <hr>
            <?php endif; ?>

            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>#</th>
                            <th>Cargo</th>
                            <th>Descripción</th>
                            <th>Tareas</th>
                            <th>Area</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($cargos)) : ?>
                            <?php foreach ($cargos as $cargo) : ?>
                                <tr>
                                    <td><?php echo $cargo->id; ?></td>
                                    <td><?php echo $cargo->cargo; ?></td>
                                    <td><?php echo $cargo->descripcion; ?></td>
                                    <td><?php echo $cargo->tareas; ?></td>
                                    <td><?php echo $cargo->areaname; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a type="button" class="btn btn-block btn-lg bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                                                    Opciones</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <?php if(in_array("Aplicacion -> Cargos", $this->session->userdata("permisos_modificar"))): ?>
                                                <li><a href="<?php echo base_url() ?>areas/cargos/Edit/<?php echo $cargo->id; ?>" class="btn btn-block btn-lg bg-amber waves-effect"><i class="material-icons">mode_edit</i>Editar</a></li>
                                                <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesCargo/<?php echo $cargo->id; ?>" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                                                <?php endif; ?>

                                                <?php if(in_array("Aplicacion -> Cargos", $this->session->userdata("permisos_baja"))): ?>
                                                <li><a href="<?php echo base_url(); ?>areas/cargos/Delete/<?php echo $cargo->id; ?>" class="btn btn-remove btn-block btn-lg bg-red waves-effect"><i class="material-icons">delete_forever</i>Borrar</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>