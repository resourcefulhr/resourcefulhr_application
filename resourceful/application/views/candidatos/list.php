<style>
    .text-color {
        color: black;
    }

    ;
</style>
<section class="content">
    <h1>
        Candidatos
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <?php if(in_array("Reclutamiento -> Candidatos", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <a href="<?php echo base_url(); ?>reclutamiento/candidatos/add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                    </i> Agregar Candidato</a>
            </div>
            <hr>
            <?php endif; ?>

            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">

                            <th>Apellido</th>
                            <th>Nombre</th>
                            <th>Sexo</th>
                            <th>Puesto/Perfil</th>
                            <th>Email</th>
                            <th>Tel</th>
                            <th>Fecha del CV</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($curriculums)) : ?>
                            <?php foreach ($curriculums as $curriculum) : ?>
                                <tr>
                                    <td><?php echo $curriculum->apellido; ?></td>
                                    <td><?php echo $curriculum->nombre; ?></td>
                                    <td><?php echo $curriculum->sexo; ?></td>
                                    <td><?php echo $curriculum->puesto; ?></td>
                                    <td><?php echo $curriculum->email; ?></td>
                                    <td><?php echo $curriculum->telefono; ?></td>
                                    <td type="hidden"><?php echo $curriculum->fecha_cv; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a type="button" class="btn btn-block btn-lg bg-light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                                                    Opciones</span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <?php if(in_array("Reclutamiento -> Candidatos", $this->session->userdata("permisos_modificar"))): ?>
                                                <li><a class="btn btn-block btn-lg bg-blue-grey waves-effect" data-toggle="modal" data-target="#updateCV" onclick="update('<?php echo $curriculum->fecha_cv; ?>','<?php echo $curriculum->url_cv; ?>','<?php echo $curriculum->id; ?>');"><i class="material-icons">update</i>Actualizar CV</a></li>
                                                <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesCurriculum/<?php echo $curriculum->id; ?>" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                                                <li><a href="<?php echo base_url(); ?>reclutamiento/candidatos/Edit/<?php echo $curriculum->id; ?>" class="btn btn-block btn-lg bg-amber waves-effect"><i class="material-icons">mode_edit</i>Editar</a></li>
                                                <?php endif; ?>

                                                <?php if(in_array("Reclutamiento -> Candidatos", $this->session->userdata("permisos_baja"))): ?>
                                                <li><a href="<?php echo base_url(); ?>reclutamiento/candidatos/deleteCV/<?php echo $curriculum->id; ?>" class="btn btn-block btn-lg bg-red waves-effect"><i class="material-icons">delete_forever</i>Borrar</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!--modal -->
<div class="modal fade" id="updateCV" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header bg-cyan">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title">Actualizar Curriculum Vitae</h5>
            </div>

            <div class="modal-body">
                <label for="">
                    <h4>Fecha de subida: </h4>
                </label>
                <p id="fechaSubida"></p>
                <hr>
                <form action="<?php echo base_url(); ?>reclutamiento/candidatos/UpdateCV" method="post" enctype="multipart/form-data">
                    <label for="">Actualizar o subir un curriculum</label>
                    <input type="file" class="form-control" name="file" required><br>
                    <input type="hidden" name="idCV" id="idCV" value="">
                    <hr>
                    <label for="">Visualizar el Curriculum actual</label><br>
                    <input type="hidden" id="urlCV" value="">
                    <button type="button" id="btnVerCV" class="btn bg-light-grey text-color" onclick="viewCV();"><i class="material-icons">pageview</i></button>
            </div>
            <div class="modal-footer bg-cyan">
                <button type="submit" class="btn bg-blue waves-effect">Enviar</button>
                </form>
                <button type="button" class="btn waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    function update(fecha, url, id) {

        $("#urlCV").val(url);
        $("#idCV").val(id);

        if (url == "") {
            $('#btnVerCV').attr("disabled", true);
            document.getElementById("fechaSubida").innerHTML = "No existe registro";
            document.getElementById("btnVerCV").innerHTML = "No tiene un Curriculum";
        } else {
            $('#btnVerCV').attr("disabled", false);
            document.getElementById("btnVerCV").innerHTML = "Ver curriculum";
            document.getElementById("fechaSubida").innerHTML = fecha;
        }
    }

    function viewCV() {
        url_cv = $("#urlCV").val();
        url = "<?php echo base_url()?>" + url_cv;
        window.open(url, '_blank');
    }
</script>