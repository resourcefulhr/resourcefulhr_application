<section class="content">
    <h1>
        Perfil del curriculum
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <input type="hidden" id="idcurriculum" name="idcurriculum" value="<?php echo $idcurriculum; ?>">
            <input type="hidden" id="aptitudid" name="aptitudid" value="">
            <div class="input-group">
                <div class="input-group">
                    <button type="button" class="btn btn-success waves-effect" data-toggle="modal" data-target="#defaultModal">
                        <i class=material-icons>
                            add_box
                        </i>
                        Agregar Aptitud
                    </button>
                </div>
            </div>
            <hr>
            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Aptitud</th>
                            <th>Nivel</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($aptitudescu)) : ?>
                            <?php foreach ($aptitudescu as $aptitudesc) : ?>
                                <tr>
                                    <td>
                                        <?php echo $aptitudesc->aptitud; ?>
                                    </td>
                                    <td>
                                        <?php echo ($aptitudesc->nivel == 1 ? 'Basico' : ($aptitudesc->nivel == 2 ? 'Normal' : 'Avanzado')) ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" id="edit-aptitud" class="btn btn-warning waves-effect edit-aptitud" data-toggle="modal" data-target="#defaultModal1" value="<?php echo $aptitudesc->id_aptitud; ?>">
                                                <i class=material-icons>mode_edit</i>
                                            </button>
                                            <button type="button" id="delete-aptitud" class="btn btn-danger waves-effect" onclick="deleteAptitud(`<?php echo $aptitudesc->id_aptitud; ?>`);">
                                                <i class=material-icons>delete_forever</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar Aptitud</h4>
            </div>
            <div class="modal-body">
                <div class="row clearfix">
                    <div class="col-sm-8">
                        <label>NIVEL</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="niveles" id="niveles" class="form-control">
                                <option value="1">Basico</option>
                                <option value="2">Normal</option>
                                <option value="3">Avanzado</option>
                            </select>
                        </div>
                    </div>
                </div>
                <table id="dataTable1" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th class="col-md-1">#</th>
                            <th>Aptitud</th>
                            <th class="col-md-2">AGREGAR</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($aptitudes)) : ?>
                            <?php foreach ($aptitudes as $aptitud) : ?>
                                <tr>
                                    <td><?php echo $aptitud->id; ?></td>
                                    <td><?php echo $aptitud->aptitud; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-success waves-effect" onclick="addAptitud(`<?php echo $aptitud->id; ?>`);">
                                            <i class=material-icons>check</i>
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Editar aptitud</h4>
            </div>
            <div class="modal-body">
                <label>Descripcion de la aptitud:</label>
                <div id="body">
                    <div class='col-sm-8'>
                        <label>NIVEL</label>
                        <div class='input-group'>
                            <span class='input-group-addon'>
                                <i class='material-icons'>keyboard_arrow_right</i>
                            </span>
                            <select name='niveledit' id='niveledit' class='form-control'>
                                <option value='1'>Basico</option>
                                <option value='2'>Normal</option>
                                <option value='3'>Avanzado</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect pull-right" onclick="updateAptitudCurriculum();"><i class="material-icons">done_all</i> Guardar</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>



<script>
    var base_url = "<?php echo base_url(); ?>";
    $(document).ready(function() {
        $(".edit-aptitud").on("click", function() {
            var id = $(this).val();
            idcurriculum = $('#idcurriculum').val();
            parametro = {
                'idAptitud': id,
                'idcurriculum': idcurriculum,
            };
            $.ajax({
                data: parametro,
                url: base_url + "reclutamiento/aptitudes/getAptitudCurriculum",
                type: "POST",
                dataType: "json",
                success: function(aptitud) {
                    $("#niveledit").val(aptitud.nivel).change();
                    $("#aptitudid").val(aptitud.id_aptitud);
                }
            });
        });
    });

    function deleteAptitud(idAptitud) {

        idcurriculum = $('#idcurriculum').val();
        parametro = {
            'idAptitud': idAptitud,
            'idcurriculum': idcurriculum,
        };
        $.ajax({
            data: parametro,
            url: base_url + "reclutamiento/aptitudes/deleteAptitudCurriculum",
            type: "GET",
            success: function(resp) {
                document.location.href = base_url + "reclutamiento/aptitudes/getAptitudesCurriculum/" + idcurriculum;
            }
        });
    }

    function addAptitud(idAptitud) {

        idcurriculum = $('#idcurriculum').val();
        niveles = $('#niveles').val();
        parametro = {
            'idAptitud': idAptitud,
            'idcurriculum': idcurriculum,
            'niveles': niveles,
        };
        $.ajax({
            data: parametro,
            url: base_url + "reclutamiento/aptitudes/addAptitudCurriculum",
            type: "GET",
            success: function(resp) {
                document.location.href = base_url + "reclutamiento/aptitudes/getAptitudesCurriculum/" + idcurriculum;
            }
        });
    }

    function updateAptitudCurriculum() {

        idcurriculum = $('#idcurriculum').val();
        idAptitud = $('#aptitudid').val();
        niveles = $('#niveledit').val();
        parametro = {
            'idAptitud': idAptitud,
            'idcurriculum': idcurriculum,
            'niveles': niveles,
        };
        $.ajax({
            data: parametro,
            url: base_url + "reclutamiento/aptitudes/updateAptitudCurriculum",
            type: "GET",
            success: function(resp) {
                document.location.href = base_url + "reclutamiento/aptitudes/getAptitudesCurriculum/" + idcurriculum;
            }
        });
    }
</script>