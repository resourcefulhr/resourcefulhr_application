<section class="content">
    <h1>
        Usuario
        <small>Editar</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>seguridad/usuarios/Update" method="POST">
                <input type="hidden" value="<?php echo $usuario->id; ?>" name="usuarioID">
                <div class="form-group">
                    <label for="rol">Rol:</label>
                    <select name="rol" id="rol" class="form-control" required>
                        <?php foreach ($roles as $rol) : ?>
                            <?php if ($rol->id == $usuario->id_rol) : ?>
                                <option value="<?php echo $rol->id ?>" selected><?php echo $rol->nombre; ?></option>
                            <?php else : ?>
                                <option value="<?php echo $rol->id ?>"><?php echo $rol->nombre; ?></option>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>seguridad/usuarios" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a></div>
            </form>
        </div>
    </div>
</section>