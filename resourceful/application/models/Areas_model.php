<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Areas_model extends CI_Model
{

    public function GetAreas()
    {
        $this->db->select("*");
        $this->db->from("areas a");
        $this->db->where("a.estado = 1");
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function PoseeHijos($id)
    {
        $this->db->select("*");
        $this->db->from("areas a");
        $this->db->where("a.id_area", $id);
        $this->db->where("a.estado = 1");
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function Update($id, $info)
    {
        $this->db->where("areas.id", $id);
        return $this->db->update("areas", $info);
    }
    public function Save($info)
    {
        return $this->db->insert('areas', $info);
    }
    public function GetArea($id)
    {
        $this->db->where("a.id", $id);
        $resultado = $this->db->get("areas a");
        return $resultado->row();
    }
}
