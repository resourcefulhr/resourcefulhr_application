<section class="content">
    <form class="form-line" action="<?php echo base_url(); ?>reclutamiento/busquedas/Store" method="POST">
        <div class="card" style="padding:0px;">
            <div class="">
                <h3 class="header bg-cyan text-center">Bienvenido a nuestro sistema de busqueda manual.<br>
                    A continuacion elija las aptitudes por las que desea filtrar...</h3>
            </div>
            <div class="body">
                <div class="col-sm-12">
                    <h4 class="text-center">Tipo de reclutamiento</h4>
                    <div class="input-group">
                        <select name="tipoReclutamiento" id="tipoReclutamiento">
                            <option value="0">Seleccione</option>
                            <option value="1" <?php echo "1" == set_value("tipoReclutamiento") ? "selected" : "" ?>>Interno</option>
                            <option value="2" <?php echo "2" == set_value("tipoReclutamiento") ? "selected" : "" ?>>Externo</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div id="apSelect" hidden>
                    <h4 class="text-center">Aptitudes Seleccionadas</h4>
                    <table id="tabla_aptitudes" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr class="bg-blue-grey">
                                <th>Aptitud</th>
                                <th>Nivel</th>
                                <th class="col-md-2">Excluyente</th>
                                <th class="col-md-1">Cancelar</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <br>
                    <hr><br>
                </div>
                <div id="apDisp" hidden>
                    <h4 class="text-center">Aptitudes Disponibles</h4>
                    <table id="dataTable" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr class="bg-blue-grey">
                                <th class="col-md-1">#</th>
                                <th>Aptitud</th>
                                <th>Nivel</th>
                                <th class="col-md-2">Excluyente</th>
                                <th class="col-md-2">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($aptitudes)) : ?>
                                <?php foreach ($aptitudes as $aptitud) : ?>
                                    <tr>
                                        <td><?php echo $aptitud->id; ?></td>
                                        <td>
                                            <?php echo $aptitud->aptitud; ?>
                                            <input type="hidden" id="aptitud<?php echo $aptitud->id; ?>" class="form-control" value="<?php echo $aptitud->aptitud; ?>">
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <select name="nivel<?php echo $aptitud->id; ?>" id="nivel<?php echo $aptitud->id; ?>" class="form-control">
                                                    <option value="1">Basico</option>
                                                    <option value="2">Normal</option>
                                                    <option value="3">Avanzado</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <select name="excluyente<?php echo $aptitud->id; ?>" id="excluyente<?php echo $aptitud->id; ?>" class="form-control">
                                                    <option value="0">NO</option>
                                                    <option value="1">SI</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <button id="btn-agregar-aptitud" type="button" class="btn btn-success waves-effect pull-right" onclick="cargarApt(`<?php echo $aptitud->id; ?>`);"><i class=material-icons>check
                                                    </i>Agregar</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <hr>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect btn-block btn-lg btn-save"><i class="material-icons">done_all</i> Guardar</button>
                </div>
            </div>
        </div>
    </form>
</section>
<script>
    $(document).ready(function(){
        $('.btn-save').attr("disabled", true);
    })
    $('#tipoReclutamiento').on('change', function() {
        var TRvalue = $('#tipoReclutamiento').val();
        if (TRvalue != '0') {
            //$('#apSelect').attr('hidden', false);
            // $('#apDisp').attr('hidden', false);
            $('#apSelect').slideDown("slow"); //attr('hidden', true);
            $('#apDisp').slideDown("slow"); //attr('hidden', true);
            $('.btn-save').attr("disabled", false);
        } else {
            $('#apSelect').slideUp("slow"); //attr('hidden', true);
            $('#apDisp').slideUp("slow"); //attr('hidden', true);
            $('.btn-save').attr("disabled", true);
        }
    });
    var apt_agregadas = Array();

    function cargarApt(idApt) {
        let apt = {
            id: idApt,
            aptitud: $("#aptitud" + idApt).val(),
            nivel: $("#nivel" + idApt).val(),
            excluyente: $("#excluyente" + idApt).val()
        }
        var agregada = false;
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idApt) {
                agregada = true;
                break;
            }
        }
        if (!agregada) {
            apt_agregadas.push(apt);
            regenerarTabla();
        } else {
            alert("Aptitud ya no agregada")
        }
    }

    function regenerarTabla() {
        var html = new String();
        for (let i = 0; i < apt_agregadas.length; i++) {
            let cargada = apt_agregadas[i];
            let exclu = (cargada.excluyente == 1 ? 'SI' : 'NO');
            let lvl = (cargada.nivel == 1 ? 'Basico' : (cargada.nivel == 2 ? 'Normal' : 'Avanzado'))
            html += "<tr>";
            html += "<td><input type='hidden' name='aptitud_car[]' value='" + cargada.id + "'>" +
                cargada.aptitud + "</td>";
            html += "<td><input type='hidden' name='nivel_car[]' value='" + cargada.nivel + "'>" +
                lvl + "</td>";
            html += "<td><input type='hidden' name='excluyente_car[]' value='" + cargada.excluyente + "'>" +
                exclu +
                "</td>";
            html +=
                "<td><button type='button' class='btn btn-danger' onclick='deleteAptitud(" + cargada.id + ");' ><i class=material-icons>delete_forever</i></button></td>";
            html += "</tr>";
        }
        let tabla = document.getElementById("tabla_aptitudes");
        tabla.tBodies[0].innerHTML = html;
    }

    function deleteAptitud(idDelete) {
        let flag = (-1);
        for (let i = 0; i < apt_agregadas.length; i++) {
            if (apt_agregadas[i].id == idDelete) {
                flag = i;
                break;
            }
        }
        if (flag !== -1) {
            apt_agregadas.splice(flag, 1);
        }
        regenerarTabla();
    }
</script>