<section class="content">
    <h1>
        Franjas Horarias
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <?php if(in_array("Control -> Franjas Horarias", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <a href="<?php echo base_url(); ?>control/FranjasHorarias/add" class="btn btn-success waves-effect"><i class=material-icons>add_box
                    </i> Agregar Franja Horaria</a>
            </div>
            <hr>
            <?php endif; ?>

            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Lunes</th>
                            <th>Martes</th>
                            <th>Miercoles</th>
                            <th>Jueves</th>
                            <th>Viernes</th>
                            <th>Sabado</th>
                            <th>Domingo</th>
                            <th>Tolerancia</th>
                            <th>Tiempo Almuerzo</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($franjas)) : ?>
                            <?php foreach ($franjas as $franja) : ?>
                                <tr>
                                    <td><?php echo $franja->id; ?></td>
                                    <td><?php echo $franja->nombre; ?></td>
                                    <td><?php echo substr($franja->lunes_ingreso, 0, -3) . " - " . substr($franja->lunes_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->martes_ingreso, 0, -3) . " - " . substr($franja->martes_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->miercoles_ingreso, 0, -3) . " - " . substr($franja->miercoles_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->jueves_ingreso, 0, -3) . " - " . substr($franja->jueves_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->viernes_ingreso, 0, -3) . " - " . substr($franja->viernes_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->sabado_ingreso, 0, -3) . " - " . substr($franja->sabado_egreso, 0, -3); ?></td>
                                    <td><?php echo substr($franja->domingo_ingreso, 0, -3) . " - " . substr($franja->domingo_egreso, 0, -3); ?></td>
                                    <td><?php echo $franja->tolerancia . "min"; ?></td>
                                    <td><?php echo $franja->tiempo_almuerzo . "min"; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-view-cargo waves-effect" data-toggle="modal" data-target="#reporteFranja" onclick="generateReport('<?php echo $franja->id; ?>');"><i class=material-icons>search</i></button>

                                            <?php if(in_array("Aplicacion -> Legajos", $this->session->userdata("permisos_modificar"))): ?>
                                            <button type="button" class="btn btn-success btn-view-cargo waves-effect" data-toggle="modal" data-target="#asignarFranja" onclick="abrirAsignarFranja('<?php echo $franja->id; ?>');"><i class=material-icons>check</i></button>
                                            <?php endif; ?>

                                            <?php if(in_array("Control -> Franjas Horarias", $this->session->userdata("permisos_modificar"))): ?>
                                            <a href="<?php echo base_url() ?>control/FranjasHorarias/Edit/<?php echo $franja->id; ?>" class="btn btn-warning waves-effect"><i class=material-icons>mode_edit</i></a>
                                            <?php endif; ?>

                                            <?php if(in_array("Control -> Franjas Horarias", $this->session->userdata("permisos_baja"))): ?>
                                            <a href="<?php echo ($franja->id == 1) ? "#" : base_url(); ?>control/FranjasHorarias/Delete/<?php echo $franja->id; ?>" class="btn btn-danger waves-effect" <?php if($franja->id == 1) echo "disabled"; ?>><i class=material-icons>delete_forever</i></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- modal reporte de franja horaria -->
<div class="modal fade" id="reporteFranja" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <section>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="bg-light-blue">
                                <img src="../front/images/aa/1.png" alt=""><br>
                                <b>Alquileres Argentia S.A</b><br>
                                <b>Reporte de Franja Horaria</b><br>
                                <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <h3 class="text-center"><b>Informacion de Franja Horaria:</b></h3>
                    <div class="row">
                        <div class="col-xs-4">
                            <label class="input-group ">Nombre: <br><p id="nombreFranjaReporte"></p></label>
                        </div>
                        <div class="col-xs-4">
                            <label class="input-group ">Tolerancia: <br><p id="toleranciaFranjaReporte"></p></label>
                        </div>
                        <div class="col-xs-4">
                            <label class="input-group ">Tiempo de Almuerzo: <br><p id="almuerzoFranjaReporte"></p></label>
                        </div>
                    </div>
                </section>

                <section>
                    <table class="table table-bordered table-striped table-hover" id="horarioReporte">
                        <thead class="bg-blue-grey">
                            <tr>
                                <th>Lunes</th>
                                <th>Martes</th>
                                <th>Miercoles</th>
                                <th>Jueves</th>
                                <th>Viernes</th>
                                <th>Sabado</th>
                                <th>Domingo</th>
                            </tr>
                        <thead>
                        <tbody><!-- dynamically filled --></tbody>
                    </table>
                    <hr>
                </section>

                <section>
                    <h3 class="text-center"><b>Legajos Asignados:</b></h3>
                    <table class="table table-bordered table-striped table-hover" id="legajosReporte">
                        <thead class="bg-blue-grey">
                            <tr>
                                <th>Legajo Nº</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Documento</th>
                                <th>Area</th>
                                <th>Cargo</th>
                            </tr>
                        <thead>
                        <tbody><!-- dynamically filled --></tbody>
                    </table>
                </section>
            </div>
            <hr>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-print">Descargar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- modal para asignar una franja horaria -->
<div class="modal fade" id="asignarFranja" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <section>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="bg-light-blue">
                                <img src="../front/images/aa/1.png" alt=""><br>
                                <h3 class="text-center"><b>Asignar Franja Horaria:</b></h3><br>
                            </div>
                        </div>
                    </div>
                    <hr>
                </section>
                <section>
                    <h3 class="text-center"><b>Legajos Asignados:</b></h3>
                    <form class="form-line" action="<?php echo base_url(); ?>control/FranjasHorarias/asignarFranja" method="POST" id="asignarForm">
                    <input type=hidden id="franjaAsignacion" name="franjaAsignacion">
                    <table id="dataTable1" class="table table-bordered table-striped table-hover" id="legajosAsignacion">
                        <thead class="bg-blue-grey">
                            <tr>
                                <th><input type="checkbox" id="toggleTodos" class="filled-in chk-col-light-blue"><label for="toggleTodos"></label></th>
                                <th>Legajo Nº</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Documento</th>
                                <th>Area</th>
                                <th>Cargo</th>
                            </tr>
                        <thead>
                        <tbody>
                        <?php if (!empty($legajos)) : ?>
                            <?php foreach ($legajos as $legajo) : ?>
                                <tr>
                                    <td><input type="checkbox" id="legajos_seleccionados[<?php echo $legajo->num_legajo; ?>]" name="legajos_seleccionados[]" class="filled-in chk-col-light-blue" value="<?php echo $legajo->num_legajo; ?>"><label for="legajos_seleccionados[<?php echo $legajo->num_legajo; ?>]"></label></td>
                                    <td><?php echo $legajo->num_legajo; ?></td>
                                    <td><?php echo $legajo->nombre; ?></td>
                                    <td><?php echo $legajo->apellido; ?></td>
                                    <td><?php echo $legajo->tipo_documento . " " . $legajo->numero_documento; ?></td>
                                    <td><?php echo $legajo->nombre_area; ?></td>
                                    <td><?php echo $legajo->cargo; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    </form>
                </section>
            </div>
            <hr>
            <div class="modal-footer">
                <button type="submit" form="asignarForm" class="btn btn-success btn-assign">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on("click", ".btn-print", function() {
        $("#reporteFranja .modal-body").print({
            /* no-op */
        });
    });

    $("#toggleTodos").click(function() {
        let checked = $(this).is(":checked");
        let checkboxes = $("input[name='legajos_seleccionados\\[\\]']");
        jQuery.each(checkboxes, function(index, value) {
            value.checked = checked;
        });
    });

    function abrirAsignarFranja(idFranja) {
        $("#franjaAsignacion").val(idFranja);
        let checkboxes = $("input[name='legajos_seleccionados\\[\\]']");
        jQuery.each(checkboxes, function(index, value) {
            value.checked = false;
        });
    }

    function generateReport(idFranja) {
        var base_url = "<?php echo base_url(); ?>";
            $.ajax({
                url: base_url + "control/FranjasHorarias/getInfoReporte/" + idFranja,
                dataType: "json",
                type: "POST",
                success: function(resp) {
                    $("#nombreFranjaReporte").text(resp.franja.nombre);
                    $("#toleranciaFranjaReporte").text(`${resp.franja.tolerancia} min`);
                    $("#almuerzoFranjaReporte").text(`${resp.franja.tiempo_almuerzo} min`);
                    regenerarTablaHorariosReporte(resp.franja);
                    regenerarTablaLegajos(resp.legajos, "legajosReporte", false);
                }
        });
    }

    function regenerarTablaHorariosReporte(datos) {
        var html = new String();
        let lunes = datos.lunes_ingreso ? `${datos.lunes_ingreso.slice(0, -3)} - ${datos.lunes_egreso.slice(0, -3)}` : "-";
        let martes = datos.martes_ingreso ? `${datos.martes_ingreso.slice(0, -3)} - ${datos.martes_egreso.slice(0, -3)}` : "-";
        let miercoles = datos.miercoles_ingreso ? `${datos.miercoles_ingreso.slice(0, -3)} - ${datos.miercoles_egreso.slice(0, -3)}` : "-";
        let jueves = datos.jueves_ingreso ? `${datos.jueves_ingreso.slice(0, -3)} - ${datos.jueves_egreso.slice(0, -3)}` : "-";
        let viernes = datos.viernes_ingreso ? `${datos.viernes_ingreso.slice(0, -3)} - ${datos.viernes_egreso.slice(0, -3)}` : "-";
        let sabado = datos.sabado_ingreso ? `${datos.sabado_ingreso.slice(0, -3)} - ${datos.sabado_egreso.slice(0, -3)}` : "-";
        let domingo = datos.domingo_ingreso ? `${datos.domingo_ingreso.slice(0, -3)} - ${datos.domingo_egreso.slice(0, -3)}` : "-";

        html += `<tr>`;
        html += `<td>${lunes} </td>`;
        html += `<td>${martes}</td>`;
        html += `<td>${miercoles}</td>`;
        html += `<td>${jueves}</td>`;
        html += `<td>${viernes}</td>`;
        html += `<td>${sabado}</td>`;
        html += `<td>${domingo}</td>`;
        html += `</tr>`;

        let tabla = document.getElementById("horarioReporte");
        tabla.tBodies[0].innerHTML = html;
    }

    function regenerarTablaLegajos(datos, idTabla, checkbox) {
        var html = new String();
        for (let i = 0; i < datos.length; i++) {
            let legajo = datos[i];
            html += `<tr>`;
            html += `<td>${legajo.num_legajo}</td>`;
            html += `<td>${legajo.nombre}</td>`;
            html += `<td>${legajo.apellido}</td>`;
            html += `<td>${legajo.tipo_documento} ${legajo.numero_documento}</td>`;
            html += `<td>${legajo.nombre_area}</td>`;
            html += `<td>${legajo.cargo}</td>`;
            html += `</tr>`;
        }
        let tabla = document.getElementById(idTabla);
        tabla.tBodies[0].innerHTML = html;
    }
</script>