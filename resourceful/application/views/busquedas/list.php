<section class="content">
  <h1>
    Busquedas
    <small>Filtro</small>
  </h1>

  <body>
    <div class="col col-lg-7">
      <div class="card" style="height:390px">
        <div>
          <h2 class="header bg-blue text-center">Busqueda por cargo</h2>
        </div>
        <div class="body">
          <div class="row clearfix">
            <div class="col-sm-6">
              <label>Cargo A cubrir</label>
              <div><select tabindex="-98" id="cargo" class="form-control show-tick novedades-filter" data-live-search="true" data-selected-text-format="count" id="legajos_novedad" name="legajos_novedad">
                  <option value="0">Seleccione...</option>
                  <?php foreach ($cargos as $cargo) : ?>
                    <option value="<?php echo $cargo->id ?>"><?php echo $cargo->cargo; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <label>Tipo de reclutamiento</label>
              <div class="input-group">
                <select name="tipoReclutamiento" id="tipoReclutamiento" class="form-control">
                  <option value="1" <?php echo "1" == set_value("tipoReclutamiento") ? "selected" : "" ?>>Interno</option>
                  <option value="2" <?php echo "2" == set_value("tipoReclutamiento") ? "selected" : "" ?>>Externo</option>
                </select>
              </div>
            </div>
            <div class="col-sm-12 text-center">
              <button class="btn bg-light-blue btn-sm waves-effect consultCargo" data-toggle="modal" data-target="#modalCargo" disabled><i class="material-icons">search </i> Consultar perfil de cargo</button>
              <hr>
            </div>
            <div class="col-sm-12">
              <button class="btn bg-blue btn-block btn-sm waves-effect FindByCargo" disabled><i class="material-icons">search </i>Buscar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- CARD 2 -->
    <div class="col col-lg-5">
      <div class="card" style="height:390px">
        <div>
          <h2 class="header bg-cyan text-center">Busqueda manual</h2>
        </div>
        <div class="body">
          <div class="row clearfix">
            <div class="col-sm-12">
              <h4 class="text-center">¿ No encontraste resultados en el filtro por cargo o quiere realizar una busqueda personalizada?
                <hr><br>Pruebe nuestra busqueda manual.</h4>
              <hr>
            </div>
            <div class="text-center">
              <a class="btn bg-cyan btn-sm waves-effect" href="buscar2"><i class="material-icons">search </i>Buscar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="text-center">
      <a href="estadisticasBusqueda"><i class="material-icons">trending_up</i> ¿ Que puedo ver acerca de los candidatos y el personal ?</a>
    </div>
    <div class="col-sm-12">
      <div class="card hidden" id="resultCard">
        <div>
          <h2 class="header bg-teal text-center">Resultados de busqueda</h2>
        </div>
        <div class="body">
          <table class="table">
            <thead>
              <tr class="bg-blue-grey">
                <th>Apellido</th>
                <th>Nombre</th>
                <th>Puntuancion</th>
                <th class="col-md-2">Opciones</th>
              </tr>
            </thead>
            <tbody id="contenido">
            </tbody>
          </table>
        </div>
      </div>
</section>
</body>
<!-- modal -->
<div class="modal fade" id="modalCargo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
      </div>
      <hr>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script>
  $('.consultCargo').on('click', function() {
    var cargoValid = $('#cargo').val();
    parametros = {
      'id': cargoValid
    };
    $.ajax({
      url: "consultarCargo",
      type: "GET",
      data: parametros,
      dataType: "html",
      success: function(resp) {
        $("#modalCargo .modal-body").html(resp);
      }
    });

  });
  $('#cargo').on('change', function() {
    var cargoValid = $('#cargo').val();
    if (cargoValid != '0') {
      $('.FindByCargo').attr('disabled', false);
      $('.consultCargo').attr('disabled', false);
    } else {
      $('.FindByCargo').attr('disabled', true);
      $('.consultCargo').attr('disabled', true);
    }
  });
  $('.FindByCargo').on('click', function() {
    $('#resultCard').removeClass('hidden');
    var cargoID = $('#cargo').val(),
      tipoReclutamiento = $('#tipoReclutamiento').val();
    var contenido = document.querySelector('#contenido');
    contenido.innerHTML = '';
    var sharp = 0;
    parametros = {
      'id': cargoID,
      'tipoReclutamiento': tipoReclutamiento,
      'tipoBusqueda': 'porCargo'
    };
    var colorColum = 0,
      c = 0;
    $.ajax({
      url: "busquedaCargo",
      type: "GET",
      data: parametros,
      dataType: "json",
      success: function(resp) {

        for (let i of resp) {
          MENOR = (i.totalAP / 3);
          MENOR2 = (i.totalAP / 3) * 2;
          MENOR >= i.suma ? colorFila = 'bg-orange' : MENOR2 >= i.suma ? colorFila = 'bg-green' : colorFila = 'bg-teal';
          if (tipoReclutamiento == '2') {
            sharp = i.id_curriculum;
            contenido.innerHTML += `
                <tr id='${i.id_curriculum}'>
                  <td>${i.apellido}</td>
                  <td>${i.nombre}</td>
                  <td>${i.suma}</td>
                  <td>
                    <div class="btn-group">
                      <a type="button" class="btn btn-block btn-lg bg-grey dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                          Opciones</span>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>reclutamiento/candidatos" class="btn btn-block btn-lg bg-blue-grey waves-effect"><i class="material-icons">system_update</i>Ir al perfil</a></li>
                        <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesCurriculum/${i.id_curriculum}" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>`;
          } else {
            sharp = i.id_legajo;
            contenido.innerHTML += `
                <tr id='${i.id_legajo}'>
                  <td>${i.apellido}</td>
                  <td>${i.nombre}</td>
                  <td>${i.suma}</td>
                  <td>
                    <div class="btn-group">
                      <a type="button" class="btn btn-block btn-lg bg-grey dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="material-icons">format_list_bulleted</i><span>
                          Opciones</span>
                      </a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes/getAptitudesLegajo/${i.id_legajo}" class="btn btn-block btn-lg bg-teal waves-effect"><i class="material-icons">star</i>Aptitudes</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>`;
          }
          $('#' + sharp).addClass(colorFila);
        }
      }
    });
  });
</script>