<section class='content'>
    <div class="row">
        <div class="col-sm-4" id="1">
            <div class="card">
                <p class="header bg-blue text-center">Aptitudes Candidatos VS Aptitudes Legajos</p>
                <div id="estadisticasReclutamiento"></div>
            </div>
        </div>
        <div class="col-sm-4" id="2">
            <div class="card">
                <p class="header bg-blue text-center">Ambitos en los que buscaste</p>
                <div id="tipoDeBusquedas"></div>
            </div>
        </div>
        <div class="col-sm-4" id="3">
            <div class="card">
                <p class="header bg-blue text-center">Generos de nuestros candidatos</p>
                <div id="empleadosSexo"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" id="4">
            <div class="card">
                <p class="header bg-blue text-center">Top 10 de aptitudes mas buscadas</p>
                <div id="topTenAp"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9" id="5">
            <div class="card">
                <p class="header bg-blue text-center">Cantidad de busquedas</p>
                <div id="cantidadDeBusquedas"></div>
            </div>
        </div>
        <div class="col-sm-3" id="6">
            <div class="card">
                <p class="header bg-blue text-center">Busquedas por cargo VS Manual</p>
                <div id="divisionDeBusquedas"></div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $.ajax({
            url: "calcularEstadisticasReclutamiento",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                Morris.Bar({
                    element: 'estadisticasReclutamiento',
                    data: [{
                            x: 'Para ' + resp.promedioLegajos.legajos + ' Legajos',
                            y: resp.promedioLegajos.promAptiLeg,
                        },
                        {
                            x: 'Para ' + resp.promedioCandi.candidatos + ' Candidatos',
                            y: resp.promedioCandi.promAptiCand,
                        },

                    ],
                    xkey: 'x',
                    ykeys: ['y'],
                    hideHover: 'auto',
                    labels: ['El promedio es'],
                    resize: true,
                    barColors: ['#2BCCF7'],
                    xLabelAngle: 0
                });
            }
        })
        $.ajax({
            url: "GetTop10ApMasBuscadas",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                Morris.Bar({
                    element: 'topTenAp',
                    data: resp,
                    xkey: 'nombreAP',
                    ykeys: ['top'],
                    resize: true,
                    hideHover: 'auto',
                    labels: ['Veces buscado'],
                    barColors: ['#2BCCF7 '],
                    xLabelAngle: 15
                });
            }
        })
        $.ajax({
            url: "cantidadDeBusquedasRealizadas",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                Morris.Bar({
                    element: 'tipoDeBusquedas',
                    data: resp,
                    xkey: 'tipo_reclutamiento',
                    ykeys: ['total'],
                    labels: ['Veces buscado'],
                    resize: true,
                    hideHover: 'auto',
                    barColors: ['#2BCCF7'],
                    xLabelAngle: 0
                });
            }
        })
        $.ajax({
            url: "GetCandSexo",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                let total = 0;
                for (let i of resp) {
                    total += Number(i.cant)
                }
                Morris.Donut({
                    element: 'empleadosSexo',
                    data: [{
                            label: resp[0].sexo,
                            value: resp[0].cant,
                            formatted: 'Aproximadamente el ' + ((resp[0].cant * 100) / total).toFixed(1) + '%',

                        },
                        {
                            label: resp[1].sexo,
                            value: resp[1].cant,
                            formatted: 'Aproximadamente el ' + ((resp[1].cant * 100) / total).toFixed(1) + '%',
                        },
                        {
                            label: resp[2].sexo,
                            value: resp[2].cant,
                            formatted: 'Aproximadamente el ' + ((resp[2].cant * 100) / total).toFixed(1) + '%',
                        },
                    ],
                    hideHover: 'auto',
                    resize: true,
                    colors: [
                        '#f0e407',
                        '#0aebf7',
                        '#0ac93d'
                    ],
                    formatter: function(x, data) {
                        return data.formatted;
                    }
                });
            }
        })
        $.ajax({
            url: "busquedasPorFecha",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                Morris.Area({
                    element: 'cantidadDeBusquedas',
                    data: resp,
                    xkey: 'fecha',
                    ykeys: ['total'],
                    labels: ['Consultas realizadas'],
                    xLabelAngle: 45,
                    fillOpacity: 0.6,
                    hideHover: 'auto',
                    resize: true,
                    pointFillColors: ['#FFEB3B'],
                    pointStrokeColors: ['#000000'],
                    lineColors: ['#03A9F4']
                })
            }
        })
        $.ajax({
            url: "divisionDeBusquedas",
            type: "GET",
            dataType: "json",
            success: function(resp) {
                Morris.Bar({
                    element: 'divisionDeBusquedas',
                    data: resp,
                    xkey: 'tipo_busqueda',
                    ykeys: ['total'],
                    xLabelAngle: 15,
                    labels: ['Veces buscado'],
                    hideHover: 'auto',
                    resize: true,
                    barColors: ['#2BCCF7'],
                });
            }
        })
    });
</script>