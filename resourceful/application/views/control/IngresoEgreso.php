<link href="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.min.js"></script>

<section class="content">
    <center>

        <head>
            <h1> REGISTRO DE HORARIO</h1>
        </head>

        <body>
            <div class="card" style="width: 33rem;">
                <div class="card-body" class="width=100%">
                    <div class="form-group">
                        <label>
                            <h2 id="hora"></h2>
                        </label>
                        <form id="form" class="form-line" action="<?php echo base_url(); ?>control/IngresoEgreso/Store" method="POST">
                            <div class="form-line focused">
                                <label>
                                    <h4>Ingrese su N° de Legajo:</h4>
                                </label>
                                <input type="number" id="num_legajo" name="num_legajo" class="form-control" required>
                            </div>

                            <label>Registre su:</label>
                            <div class="form-group">
                                <button type="submit" id="ingreso" name="ingreso" class="btn btn-success waves-effect"><i class="material-icons">play_arrow</i> INGRESO</button>
                            </div>

                            <hr>

                            <label>Registre su:</label>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning waves-effect" name="almuerzo"><i class="material-icons">fastfood</i> ALMUERZO</button>
                            </div>

                            <hr>

                            <label>Registre su:</label>
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger waves-effect" name="egreso"><i class="material-icons">stop</i> SALIDA</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </body>

</section>


<script>
    <?php $today = getdate(); ?>
    var dt = new Date('<?php echo $today['month'] . " " . $today['mday'] . ", " .  $today['year'] . " " . $today['hours'] . ":" . $today['minutes'] . ":" . $today['seconds']; ?>');
    $('#hora').text(armarFechaString(dt));

    setInterval(function() {
        dt.setSeconds(dt.getSeconds() + 1);
        $('#hora').text(armarFechaString(dt));
    }, 1000);

    function armarFechaString(date) {
        var DD = ("0" + date.getDate()).slice(-2);
        // getMonth returns month from 0
        var MM = ("0" + (date.getMonth() + 1)).slice(-2);
        var YYYY = date.getFullYear();
        var hh = ("0" + date.getHours()).slice(-2);
        var mm = ("0" + date.getMinutes()).slice(-2);
        var ss = ("0" + date.getSeconds()).slice(-2);
        return DD + "-" + MM + "-" + YYYY + " " + hh + ":" + mm + ":" + ss;
    }

    $("#form").submit(function(e) {
        var base_url = "<?php echo base_url(); ?>";
        var processSubmit = true;
        $.ajax({
            url: base_url + "control/IngresoEgreso/validate/" + $("#num_legajo").val(),
            type: "POST",
            async: false
        }).done(function(data) {
            if (data) {
                processSubmit = false;
                swal("Oops", data, "error");
            }
        }).error(function(data) {
            swal("Oops", data, "error");
        });

        return processSubmit;
    });
</script>