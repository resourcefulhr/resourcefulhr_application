$(function() {
    $('#cambiar-contraseña').validate({
        rules: {
            'password': {
                required: true,
                minlength: 6
            },
            'confirm': {
                required: true,
                equalTo: '[name="password"]'
            },
        },
        highlight: function(input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function(input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function(error, element) {
            $(element).parents('.input-group').append(error);
        },
        messages: {
            'password': {
                required: "Debe ingresar una contraseña",
                minlength: "La contraseña debe tener al menos 6 caracteres"
            },
            'confirm': {
                required: "Debe ingresar una contraseña",
                equalTo: 'Los campos de contraseña deben coincidir',
            }
        }
    });
});