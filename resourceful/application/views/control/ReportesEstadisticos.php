<link href="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>front/plugins/sweetalert/sweetalert.min.js"></script>

<link href="<?php echo base_url(); ?>front/plugins/morrisjs/morris.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>front/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>front/plugins/morrisjs/morris.js"></script>
<section class="content">
    <div class="container-fluid">
        <div class="block-header row">
            <h1>Estadisticas de Control</h1>
            <div class="col-sm-4">
                <label>Legajos:</label>
                <select class="form-control show-tick filter" multiple="" data-live-search="true" data-selected-text-format="count" id="legajos_input" name="legajos_input">
                    <?php foreach($legajos as $legajo): ?>
                        <option value="<?php echo $legajo->num_legajo?>">
                            <?php echo $legajo->nombre . " " . $legajo->apellido . ", " . $legajo->num_legajo; ?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-4" id="datepicker_reportes">
                <label></label>
                <div class="input-group input-daterange" data-provide="datepicker">
                    <div class="input-group-addon">Desde</div>
                    <input id="min-date" name="start-date" type="text" class="form-control filter" data-date-container='#datepicker_reportes'>
                    <div class="input-group-addon">Hasta</div>
                    <input id="max-date" name="end-date" type="text" class="form-control filter" data-date-container='#datepicker_reportes'>
                </div>
            </div>
            <div class="col-sm-4">
                <label></label>
                <input type="button" class="form-control" value="Imprimir Reporte" onclick="imprimirFiltro()">
            </div>
        </div>
         <!-- Reportes -->
        <div id="imprimir">
        <div class="row" id="header" style="display:none;">
                <div class="col-xs-12 text-center">
                    <div class="bg-light-blue">
                        <img src="../front/images/aa/1.png" alt="">
                        <br>
                        <b>Alquileres Argentia S.A</b><br>
                        <b>Informe de Novedad</b><br>
                        <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
                        <b>Legajos: <span id="legajos_reporte"></span></b><br>
                        <b>Desde: <span id="desde_reporte"></span> Hasta: <span id="hasta_reporte"></span></b><br>
                        <br>
                    </div>
                </div>
        </div>
        <div class="row clearfix">
            <h2 class="text-center">DE LOS EMPLEADOS</h2>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>FRECUENCIA DE NOVEDADES GENERADAS EN PERIODO</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra la frecuencia de generacion de novedades, agrupadas por su día de generacion.<br> Nota: Solo se muestran los dias en los que se generaron novedades.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="novedades_generadas" class="graph text-center"></div>
                        <div id="placeholder_novedades_generadas" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INDICE DE INASISTENCIAS EN PERIODO</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el indice de inasistencias en el periodo.<br>Se excluyen de los dias laborales: feriados, vacaciones, licencias, francos.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="donut_chart_inasistencias" class="graph text-center"></div>
                        <div id="placeholder_donut_chart_inasistencias" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INDICE DE CUMPLIMIENTO DE HORARIO</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el indice de inclumplimiento horario en el periodo.<br>Se excluyen de los dias laborales: feriados, vacaciones, licencias, francos, inasistencias.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="donut_chart_cumplimiento_horario" class="graph text-center"></div>
                        <div id="placeholder_donut_chart_cumplimiento_horario" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix" id="tablero_reportes">
            <h2 class="text-center">DE LA GESTION</h2>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>ESTADO DE NOVEDADES EN PERIODO</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el estado de todas las novedades generadas en el periodo.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="donut_chart_estados" class="graph text-center"></div>
                        <div id="placeholder_donut_chart_estados" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INDICE DE JUSTIFICACION - INASISTENCIAS</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el indice de justificacion para las inasistencias en el periodo.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="indice_justificacion_inasistencias" class="graph text-center"></div>
                        <div id="placeholder_indice_justificacion_inasistencias" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INDICE DE JUSTIFICACION - HORARIO INCONSISTENTE</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el indice de justificacion para las inconsistencias horarias en el periodo.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="indice_justificacion_horarioinconsistente" class="graph text-center"></div>
                        <div id="placeholder_indice_justificacion_horarioinconsistente" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INDICE DE JUSTIFICACION - REGISTRO INCONSISTENTE</h2>
                        <div class="header-dropdown m-r--5">
                            <div class="dropdown">
                                <a class="dropdown-toggle" role="button" aria-expanded="true" aria-haspopup="true" href="javascript:void(0);" data-toggle="dropdown">
                                    <i class="material-icons">help</i>
                                </a>
                                <div class="dropdown-menu pull-right">
                                    <li><a class="waves-effect waves-block">Muestra el indice de justificacion para las inconsistencias en el registro horario del periodo.</a></li>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <div id="indice_justificacion_registroinconsistente" class="graph text-center"></div>
                        <div id="placeholder_indice_justificacion_registroinconsistente" class="row">
                            <h4 class='col-xs-12 text-center'>No Existen datos para el filtro seleccionado.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</section>

<script>
$(function() {
    $('.filter').change(function() {
        let base_url = '<?php echo base_url(); ?>'
        let desde_completo = $("#datepicker_reportes input[name=start-date]").datepicker("getDate");
        let desde;
        if (desde_completo) {
            desde = desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2);
        } else {
            return;
        }

        let hasta_completo = $("#datepicker_reportes input[name=end-date]").datepicker("getDate");
        let hasta;
        if (hasta_completo) {
            hasta = hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2);
        } else {
            return;
        }

        let legajos = JSON.stringify($("#legajos_input").val());
        // Distribucion de Novedades
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/DistribucionNovedades",
            type: "POST",
            data: {
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#novedades_generadas").html("");
            $("#placeholder_novedades_generadas").hide();
            if (data.length) {
                Morris.Bar({
                    element: "novedades_generadas",
                    hideHover: "auto",
                    data: data,
                    xkey: 'dia',
                    ykeys: ['inasistencias', 'registros_inconsistentes', 'horarios_inconsistentes'],
                    labels: ['Inasistencia', 'Registro Inconsistente', 'Horario Inconsistente'],
                    barColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)'],
                });
            } else {
                $("#placeholder_novedades_generadas").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Indice de Asistencias
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/IndiceInasistencia",
            type: "POST",
            data: {
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#donut_chart_inasistencias").html("");
            $("#placeholder_donut_chart_inasistencias").hide();
            if (data.dias_laborales) {
                Morris.Donut({
                    element: "donut_chart_inasistencias",
                    data: [
                        {
                            label: 'Asistencias',
                            value: (((data.dias_laborales - data.inasistencias) * 100) / data.dias_laborales).toFixed(2)
                        },
                        {
                            label: 'Inasistencias',
                            value: ((data.inasistencias * 100) / data.dias_laborales).toFixed(2)
                        }
                    ],
                    colors: ['rgb(0, 150, 136)', 'rgb(233, 30, 99)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_donut_chart_inasistencias").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Indice de Cumplimiento Horario
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/IndiceInasistencia",
            type: "POST",
            data: {
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#donut_chart_cumplimiento_horario").html("");
            $("#placeholder_donut_chart_cumplimiento_horario").hide();
            let total = data.dias_laborales - data.inasistencias;
            if (total) {
                Morris.Donut({
                    element: "donut_chart_cumplimiento_horario",
                    data: [
                        {
                            label: 'En Horario',
                            value: (((total - data.horario_inconsistente) * 100) / total).toFixed(2)
                        },
                        {
                            label: 'Incumplimiento al Horario',
                            value: ((data.horario_inconsistente * 100) / total).toFixed(2)
                        }
                    ],
                    colors: ['rgb(0, 150, 136)', 'rgb(233, 30, 99)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_donut_chart_cumplimiento_horario").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Estados
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/EstadoDeNovedades",
            type: "POST",
            data: {
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#donut_chart_estados").html("");
            $("#placeholder_donut_chart_estados").hide();
            data.abierta = new Number(data.abierta);
            data.justificada = new Number(data.justificada);
            data.no_justificada = new Number(data.no_justificada);
            let total = data.abierta + data.justificada + data.no_justificada;
            if (total) {
                Morris.Donut({
                    element: "donut_chart_estados",
                    data: [
                        {
                            label: 'ABIERTA',
                            value: (data.abierta * 100 / total).toFixed(2)
                        },
                        {
                            label: 'JUSTIFICADA',
                            value: (data.justificada * 100 / total).toFixed(2)
                        },
                        {
                            label: 'NO JUSTIFICADA',
                            value: (data.no_justificada * 100 / total).toFixed(2)
                        }
                    ],
                    colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(0, 150, 136)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_donut_chart_estados").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Indice Justificacion - Inasistencias
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/IndiceJustificacion",
            type: "POST",
            data: {
                tipo: "INASISTENCIA",
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#indice_justificacion_inasistencias").html("");
            $("#placeholder_indice_justificacion_inasistencias").hide();
            data.justificadas = new Number(data.justificadas);
            data.no_justificadas = new Number(data.no_justificadas);
            let total = data.justificadas + data.no_justificadas;
            if (total) {
                Morris.Donut({
                    element: "indice_justificacion_inasistencias",
                    data: [
                        {
                            label: 'JUSTIFICADAS',
                            value: (data.justificadas * 100 / total).toFixed(2)
                        },
                        {
                            label: 'NO JUSTIFICADAS',
                            value: (data.no_justificadas * 100 / total).toFixed(2)
                        }
                    ],
                    colors: ['rgb(0, 150, 136)', 'rgb(233, 30, 99)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_indice_justificacion_inasistencias").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Indice Justificacion - Horario Inconsistente
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/IndiceJustificacion",
            type: "POST",
            data: {
                tipo: "HORARIO INCONSISTENTE",
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#indice_justificacion_horarioinconsistente").html("");
            $("#placeholder_indice_justificacion_horarioinconsistente").hide();
            data.justificadas = new Number(data.justificadas);
            data.no_justificadas = new Number(data.no_justificadas);
            let total = data.justificadas + data.no_justificadas;
            if (total) {
                Morris.Donut({
                    element: "indice_justificacion_horarioinconsistente",
                    data: [
                        {
                            label: 'JUSTIFICADAS',
                            value: (data.justificadas * 100 / total).toFixed(2)
                        },
                        {
                            label: 'NO JUSTIFICADAS',
                            value: (data.no_justificadas * 100 / total).toFixed(2)
                        }
                    ],
                    colors: ['rgb(0, 150, 136)', 'rgb(233, 30, 99)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_indice_justificacion_horarioinconsistente").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });

        // Indice Justificacion - Registro Inconsistente
        $.ajax({
            url: base_url + "control/ReportesEstadisticos/IndiceJustificacion",
            type: "POST",
            data: {
                tipo: "REGISTRO INCONSISTENTE",
                legajos: legajos,
                desde: desde,
                hasta: hasta
            }
        }).done(function(data) {
            data = JSON.parse(data);
            $("#indice_justificacion_registroconsistente").html("");
            $("#placeholder_indice_justificacion_registroinconsistente").hide();
            data.justificadas = new Number(data.justificadas);
            data.no_justificadas = new Number(data.no_justificadas);
            let total = data.justificadas + data.no_justificadas;
            if (total) {
                Morris.Donut({
                    element: "indice_justificacion_registroinconsistente",
                    data: [
                        {
                            label: 'JUSTIFICADAS',
                            value: (data.justificadas * 100 / total).toFixed(2)
                        },
                        {
                            label: 'NO JUSTIFICADAS',
                            value: (data.no_justificadas * 100 / total).toFixed(2)
                        }
                    ],
                    colors: ['rgb(0, 150, 136)', 'rgb(233, 30, 99)'],
                    formatter: function (y) {
                        return y + '%'
                    }
                });
            } else {
                $("#placeholder_indice_justificacion_registroinconsistente").show();
            }
        }).error(function(data) {
            console.log(data);
            swal("Oops", "Error: " +  data.status + " - " + data.statusText, "error");
        });
    });
});

function imprimirFiltro() {
    let desde_completo = $("#datepicker_reportes input[name=start-date]").datepicker("getDate");
    let desde;
    if (desde_completo) {
        desde = desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2);
        $("#desde_reporte").html(desde);
    } else {
        $("#desde_reporte").html("-");
    }

    let hasta_completo = $("#datepicker_reportes input[name=end-date]").datepicker("getDate");
    let hasta;
    if (hasta_completo) {
        hasta = hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2);
        $("#hasta_reporte").html(hasta);
    } else {
        $("#hasta_reporte").html("-");
    }

    let legajos = JSON.stringify($("#legajos_input").val());
    if (legajos != "null") {
        $("#legajos_reporte").html(legajos);
    } else {
        $("#legajos_reporte").html("Todos");
    }
    $("#header").show();
    $("#imprimir").print({});
    $("#header").hide();
}
</script>