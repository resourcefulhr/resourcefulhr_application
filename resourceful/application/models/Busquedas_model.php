<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Busquedas_model extends CI_Model
{
    //traer aptitudes excluyentes
    public function GetAptitudesEXcl($id)
    {
        return $this->db->select('id_aptitud, nivel')
            ->where('id_cargo', $id)
            ->where('excluyente = 1')
            ->get('aptitudes_cargos')
            ->result();
    }
    //traer aptitudes opcionales
    public function GetAptitudesOp($id)
    {
        return $this->db->select('id_aptitud, nivel, excluyente')
            ->where('id_cargo', $id)
            ->where('excluyente = 0')
            ->get('aptitudes_cargos')
            ->result();
    }
    //traer aptitudes opcionales
    public function GetAptitudesTodo($id)
    {
        return $this->db->select('id_aptitud, nivel, excluyente')
            ->where('id_cargo', $id)
            ->get('aptitudes_cargos')
            ->result();
    }
    public function GetbusquedaCurriculums($aptitudes, $suma, $cant, $cant2)
    {
        $currs =  $this->db->select("id_curriculum")
            ->from("aptitudes_curriculums")
            ->where($aptitudes)
            ->group_by('id_curriculum')
            ->having("count(`id_curriculum`)=$cant")
            ->get()
            ->result();
        foreach ($currs as $asd) {
            $dsa[] = $asd->id_curriculum;
        }
        $estados_string = "'" . implode("','", $dsa) . "'";
        return   $this->db->select("id_curriculum, SUM(nivel) as suma,  $cant2 as totalAP, c.nombre, c.apellido")
            ->from("aptitudes_curriculums")
            ->join("candidatos c", "c.id = aptitudes_curriculums.id_curriculum")
            ->where($suma)
            ->or_where_in($estados_string)
            ->group_by("id_curriculum")
            ->order_by("suma DESC")
            ->get()
            ->result();
    }
    public function GetbusquedaLegajos($aptitudes, $suma, $cant, $cant2)
    {
        $currs =  $this->db->select("id_legajo")
            ->from("aptitudes_legajos")
            ->where($aptitudes)
            ->group_by('id_legajo')
            ->having("count(`id_legajo`)=$cant")
            ->get()
            ->result();
        foreach ($currs as $asd) {
            $dsa[] = $asd->id_legajo;
        }
        $estados_string = "'" . implode("','", $dsa) . "'";
        return   $this->db->select("id_legajo, SUM(nivel) as suma,  $cant2 as totalAP, l.nombre, l.apellido")
            ->from("aptitudes_legajos")
            ->join("legajos l", "l.id = aptitudes_legajos.id_legajo")
            ->where($suma)
            ->or_where_in($estados_string)
            ->group_by("id_legajo")
            ->order_by("suma DESC")
            ->get()
            ->result();
    }
    public function GetbusquedaCurriculumsOpci($aptitudes, $cant2)
    {

        return   $this->db->select("id_curriculum, SUM(nivel) as suma, $cant2 AS  totalAP, c.nombre, c.apellido ")
            ->from("aptitudes_curriculums")
            ->join("candidatos c", "c.id = aptitudes_curriculums.id_curriculum")
            ->where($aptitudes)
            ->group_by("id_curriculum")
            ->order_by("suma DESC")
            ->get()
            ->result();
    }
    public function GetbusquedaLegajosOpci($aptitudes, $cant2)
    {

        return   $this->db->select("id_legajo, SUM(nivel) as suma, $cant2 AS  totalAP, l.nombre, l.apellido")
            ->from("aptitudes_legajos")
            ->join("legajos l", "l.id = aptitudes_legajos.id_legajo")
            ->where($aptitudes)
            ->group_by("id_legajo")
            ->order_by("suma DESC")
            ->get()
            ->result();
    }

    /////////////////////////////////////////////BUSQUEDA MANUAL///////////////////////////////////////////
    public function GetAptitudesEXclBusquedaManual($id)
    {
        return $this->db->select('id_aptitud, nivel')
            ->where('id_busqueda_manual', $id)
            ->where('excluyente = 1')
            ->get('temp_busqueda_manual')
            ->result();
    }
    //traer aptitudes opcionales
    public function GetAptitudesOpBusquedaManual($id)
    {
        return $this->db->select('id_aptitud, nivel, excluyente')
            ->where('id_busqueda_manual', $id)
            ->get('temp_busqueda_manual')
            ->result();
    }
    public function clearTableTemp()
    {
        $this->db->where("id_busqueda_manual > 0")
            ->delete('temp_busqueda_manual');
    }
    /******************************************Estadisticas ****************************************************/
    public function GetPromedioAptitudesCandidatos()
    {
        $resultados = $this->db->query(" SELECT COUNT(id_aptitud) / COUNT(DISTINCT id_curriculum) AS promAptiCand, COUNT(DISTINCT id_curriculum) AS candidatos  FROM aptitudes_curriculums");
        return $resultados->row();
    }
    public function GetPromedioAptitudesLegajos()
    {
        $resultados = $this->db->query(" SELECT COUNT(id_aptitud) / COUNT(DISTINCT id_legajo) AS promAptiLeg, COUNT(DISTINCT id_legajo) AS legajos  FROM aptitudes_legajos");
        return $resultados->row();
    }
    public function GetCandidatosPorSexo()
    {
        return $this->db->select('sexo, COUNT(sexo) as cant')
            ->group_by('sexo')
            ->get('candidatos')
            ->result();
    }
    public function saveEstadisticasBusqueda($estadisticas)
    {
        $this->db->insert('generacion_estadisticas_busqueda', $estadisticas);
    }
    public function LastID()
    {
        return $this->db->insert_id();
    }
    public function InsertDetallesBusqueda($detalles)
    {
        $this->db->insert('detalle_busqueda', $detalles);
    }
    public function GetTop10ApMasBuscadas()
    {
        return $this->db->select("COUNT(db.aptitud) as top, ap.aptitud as nombreAP")
            ->from("detalle_busqueda db")
            ->join("aptitudes ap", "ap.id = db.aptitud")
            ->group_by("nombreAP")
            ->order_by("top DESC")
            ->limit("10")
            ->get()
            ->result();
    }
    public function cantidadDeBusquedasRealizadas()
    {
        return $this->db->select("COUNT(id) as total, tipo_reclutamiento")
            ->from("generacion_estadisticas_busqueda")
            ->group_by("tipo_reclutamiento")
            ->order_by("total DESC")
            ->get()
            ->result();
    }
    public function busquedasPorFecha()
    {
        return $this->db->select("COUNT(id) as total, fecha")
            ->from("generacion_estadisticas_busqueda")
            ->group_by("fecha")
            ->order_by("fecha ASC")
            ->get()
            ->result();
    }
    public function divisionDeBusquedas()
    {
        return $this->db->select("COUNT(id) as total, tipo_busqueda")
            ->from("generacion_estadisticas_busqueda")
            ->group_by("tipo_busqueda")
            ->order_by("tipo_busqueda DESC")
            ->get()
            ->result();
    }
}
