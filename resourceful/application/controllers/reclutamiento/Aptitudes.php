<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aptitudes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Aptitudes_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudes()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/list', $data);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function add()
    {
        if ($this->session->userdata('login')) {
            $aptitud = $this->input->POST("aptitud");
            $verificacion = $this->Aptitudes_model->Existe($aptitud);
            if ($verificacion) {
                $this->session->set_flashdata("ERROR", "No se puede cargar esta aptitud porque ya existe");
                $this->index();
            } else {
                $info = array(
                    'aptitud' => $aptitud
                );
                $this->Aptitudes_model->Save($info);
                $this->index();
            }
        } else {
            $this->load->view('login');
        }
    }
    public function Delete($id)
    {
        if ($this->session->userdata('login')) {
            $this->Aptitudes_model->DeleteAptitud($id);
            //$this->Aptitudes_model->DeletesAptitudCurriculums($id);
            //$this->Aptitudes_model->DeletesAptitudLegajo($id);
            $this->Aptitudes_model->DeletesAptitudCargo($id);
            $this->index();
        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudEdit()
    {
        $idaptitud = $this->input->post("idaptitud");

        $aptitud = $this->Aptitudes_model->GetAptitud($idaptitud);
        $html = "<input type='text' name='aptitudEdit' class='form-control' value='" . $aptitud->aptitud . "' required>
        <input type='hidden' name='idAptitud' class='form-control' value='" . $idaptitud . "'>";
        echo $html;
    }
    public function Update()
    {
        if ($this->session->userdata('login')) {
            $id = $this->input->POST("idAptitud");
            $aptitud = $this->input->POST("aptitudEdit");
            $verificacion = $this->Aptitudes_model->Existe($aptitud);
            if ($verificacion) {
                $this->session->set_flashdata("ERROR", "No se puede actualizar esa aptitud porque ya existe " . $aptitud);
                $this->index();
            } else {
                $info = array(
                    'aptitud' => $aptitud
                );
                $this->Aptitudes_model->Update($info, $id);
                $this->index();
            }
        } else {
            $this->load->view('login');
        }
    }

    ////////////////////////////////////////////////////////////////
    ////////////////     ACA APTITUDES CARGOS      /////////////////
    ////////////////////////////////////////////////////////////////
    public function getAptitudesCargo($id)
    {
        if ($this->session->userdata('login')) {
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudesC($id),
                'aptitudescs' => $this->Aptitudes_model->GetAptitudesCargo($id),
                'idCargo' => $id,
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/listControl', $data);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function deleteAptitudCargo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idCargo = $_GET['idCargo'];
            $this->Aptitudes_model->DeleteAptitudCargo($idCargo, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    public function addAptitudCargo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idCargo = $_GET['idCargo'];
            $nivel = $_GET['niveles'];
            $excluyente = $_GET['excluyente'];
            $data  = array(
                'id_aptitud' => $idAptitud,
                'id_cargo' => $idCargo,
                'nivel' => $nivel,
                'excluyente' => $excluyente,
            );
            $this->Aptitudes_model->saveAptitudCargo($data);
        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudCargo()
    {
        $idaptitud = $_POST['idAptitud'];
        $idcargo = $_POST['idCargo'];

        $aptitud = $this->Aptitudes_model->GetAptitudCargo($idcargo, $idaptitud);
        echo json_encode($aptitud);
    }
    public function updateAptitudCargo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idCargo = $_GET['idCargo'];
            $nivel = $_GET['niveles'];
            $excluyente = $_GET['excluyente'];
            $data  = array(
                'nivel' => $nivel,
                'excluyente' => $excluyente,
            );
            $this->Aptitudes_model->updateAptitudCargo($data, $idCargo, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    ////////////////////////////////////////////////////////////////
    ////////////////     ACA APTITUDES LEGAJO      /////////////////
    ////////////////////////////////////////////////////////////////
    public function addAptitudesLegajo($id)
    {
        if ($this->session->userdata('login')) {
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudes(),
                'idlegajo' => $id,
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/addLegajos', $data);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function storeAptitudLegajo()
    {
        if ($this->session->userdata('login')) {
            $idlegajo = $this->input->POST("idlegajo");
            $idAptitud = $this->input->POST("aptitud_car");
            $nivelAptitud = $this->input->POST("nivel_car");
            for ($i = 0; $i < count($idAptitud); $i++) {
                $data  = array(
                    'id_aptitud' => $idAptitud[$i],
                    'id_legajo' => $idlegajo,
                    'nivel' => $nivelAptitud[$i],
                );
                $this->Aptitudes_model->saveAptitudLegajo($data);
            }
            redirect(base_url() . "areas/legajos");

        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudesLegajo($id)
    {
        if ($this->session->userdata('login')) {
            if($this->Aptitudes_model->GetAptitudesLegajo($id)){
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudesL($id),
                'aptitudesleg' => $this->Aptitudes_model->GetAptitudesLegajo($id),
                'idLegajo' => $id,
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/listLegajo', $data);
            $this->load->view('layouts/footer');
        }else{
            redirect(base_url() . "reclutamiento/aptitudes/addAptitudesLegajo/".$id);
        }
        } else {
            $this->load->view('login');
        }
    }
    public function updateAptitudLegajo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idLegajo = $_GET['idLegajo'];
            $nivel = $_GET['niveles'];
            $data  = array(
                'nivel' => $nivel,
            );
            $this->Aptitudes_model->updateAptitudLegajo($data, $idLegajo, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudLegajo()
    {
        $idaptitud = $_POST['idAptitud'];
        $idLegajo = $_POST['idLegajo'];

        $aptitud = $this->Aptitudes_model->GetAptitudLegajo($idLegajo, $idaptitud);
        echo json_encode($aptitud);
    }
    public function deleteAptitudLegajo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idLegajo = $_GET['idLegajo'];
            $this->Aptitudes_model->DeleteAptitudLegajo($idLegajo, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    public function addAptitudLegajo()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idLegajo = $_GET['idLegajo'];
            $nivel = $_GET['niveles'];
           
            $data  = array(
                'id_aptitud' => $idAptitud,
                'id_legajo' => $idLegajo,
                'nivel' => $nivel,
            );
            $this->Aptitudes_model->saveAptitudLegajo($data);
        } else {
            $this->load->view('login');
        }
    }
    ////////////////////////////////////////////////////////////////
    ////////////////     ACA APTITUDES CURRICULUMS /////////////////
    ////////////////////////////////////////////////////////////////
    public function addAptitudesCurriculum($id)
    {
        if ($this->session->userdata('login')) {
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudes(),
                'idcurriculum' => $id,
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/addCurriculum', $data);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function storeAptitudCurriculum()
    {
        if ($this->session->userdata('login')) {
            $idcurriculum = $this->input->POST("idcurriculum");
            $idAptitud = $this->input->POST("aptitud_car");
            $nivelAptitud = $this->input->POST("nivel_car");
            for ($i = 0; $i < count($idAptitud); $i++) {
                $data  = array(
                    'id_aptitud' => $idAptitud[$i],
                    'id_curriculum' => $idcurriculum,
                    'nivel' => $nivelAptitud[$i],
                );
                $this->Aptitudes_model->saveAptitudCurriculums($data);
            }
            redirect(base_url() . "reclutamiento/candidatos");

        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudesCurriculum($id)
    {
        if ($this->session->userdata('login')) {
            if($this->Aptitudes_model->GetAptitudesCurriculum($id)){
            $data = array(
                'aptitudes' => $this->Aptitudes_model->GetAptitudesCu($id),
                'aptitudescu' => $this->Aptitudes_model->GetAptitudesCurriculum($id),
                'idcurriculum' => $id,
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('aptitudes/listCurriculum', $data);
            $this->load->view('layouts/footer');
        }else{
            redirect(base_url() . "reclutamiento/aptitudes/addAptitudesCurriculum/".$id);
        }
        } else {
            $this->load->view('login');
        }
    }
    public function updateAptitudCurriculum()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idcurriculum = $_GET['idcurriculum'];
            $nivel = $_GET['niveles'];
            $data  = array(
                'nivel' => $nivel,
            );
            $this->Aptitudes_model->updateAptitudCurriculum($data, $idcurriculum, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    public function getAptitudCurriculum()
    {
        $idaptitud = $_POST['idAptitud'];
        $idcurriculum = $_POST['idcurriculum'];

        $aptitud = $this->Aptitudes_model->GetAptitudCurriculum($idcurriculum, $idaptitud);
        echo json_encode($aptitud);
    }
    public function deleteAptitudCurriculum()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idcurriculum = $_GET['idcurriculum'];
            $this->Aptitudes_model->deleteAptitudCurriculum($idcurriculum, $idAptitud);
        } else {
            $this->load->view('login');
        }
    }
    public function addAptitudCurriculum()
    {
        if ($this->session->userdata('login')) {
            $idAptitud = $_GET['idAptitud'];
            $idcurriculum = $_GET['idcurriculum'];
            $nivel = $_GET['niveles'];
           
            $data  = array(
                'id_aptitud' => $idAptitud,
                'id_curriculum' => $idcurriculum,
                'nivel' => $nivel,
            );
            $this->Aptitudes_model->saveAptitudCurriculums($data);
        } else {
            $this->load->view('login');
        }
    }
}
