<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FranjasHorarias extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->model("FranjasHorarias_model");
        $info = array(
            "franjas" => $this->FranjasHorarias_model->GetFranjas(),
            "legajos" => $this->FranjasHorarias_model->GetLegajos()
        );

        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/franjas/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function add()
    {
        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/franjas/add');
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function Store()
    {
        if ($this->session->userdata('login')) {
            $this->form_validation->set_rules("nombre", "nombre", "is_unique[franjas_horarias.nombre]");
            if ($this->form_validation->run()) {
                $this->load->model("FranjasHorarias_model");
                $info = array(
                    "nombre" => $this->input->POST("nombre"),
                    "lunes_ingreso" => $this->input->POST("lunes_ingreso") == '' ? NULL : $this->input->POST("lunes_ingreso"),
                    "lunes_egreso" => $this->input->POST("lunes_egreso") == '' ? NULL : $this->input->POST("lunes_egreso"),
                    "martes_ingreso" => $this->input->POST("martes_ingreso") == '' ? NULL : $this->input->POST("martes_ingreso"),
                    "martes_egreso" => $this->input->POST("martes_egreso") == '' ? NULL : $this->input->POST("martes_egreso"),
                    "miercoles_ingreso" => $this->input->POST("miercoles_ingreso") == '' ? NULL : $this->input->POST("miercoles_ingreso"),
                    "miercoles_egreso" => $this->input->POST("miercoles_egreso") == '' ? NULL : $this->input->POST("miercoles_egreso"),
                    "jueves_ingreso" => $this->input->POST("jueves_ingreso") == '' ? NULL : $this->input->POST("jueves_ingreso"),
                    "jueves_egreso" => $this->input->POST("jueves_egreso") == '' ? NULL : $this->input->POST("jueves_egreso"),
                    "viernes_ingreso" => $this->input->POST("viernes_ingreso") == '' ? NULL : $this->input->POST("viernes_ingreso"),
                    "viernes_egreso" => $this->input->POST("viernes_egreso") == '' ? NULL : $this->input->POST("viernes_egreso"),
                    "sabado_ingreso" => $this->input->POST("sabado_ingreso") == '' ? NULL : $this->input->POST("sabado_ingreso"),
                    "sabado_egreso" => $this->input->POST("sabado_egreso") == '' ? NULL : $this->input->POST("sabado_egreso"),
                    "domingo_ingreso" => $this->input->POST("domingo_ingreso") == '' ? NULL : $this->input->POST("domingo_ingreso"),
                    "domingo_egreso" => $this->input->POST("domingo_egreso") == '' ? NULL : $this->input->POST("domingo_egreso"),
                    "tolerancia" => $this->input->POST("tolerancia"),
                    "tiempo_almuerzo" => $this->input->POST("tiempo_almuerzo")
                );
                if ($this->FranjasHorarias_model->Insert($info)) {
                    $this->index();
                } else {
                    $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                    $this->Add();
                }
            } else {
                $this->Add();
            }
        } else {
            $this->load->view('login');
        }
    }

    public function Edit($idFranja)
    {
        if ($this->session->userdata('login')) {
            $this->load->model("FranjasHorarias_model");
            $info = array(
                "franja" => $this->FranjasHorarias_model->GetFranja($idFranja)
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/franjas/edit', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function StoreEdit($id)
    {
        if ($this->session->userdata('login')) {
            $this->load->model("FranjasHorarias_model");
            $franja = $this->FranjasHorarias_model->GetFranja($id);

            $this->form_validation->set_rules("nombre", "nombre", 'is_unique[franjas_horarias.nombre]');

            if ($franja->nombre == $this->input->POST("nombre") || $this->form_validation->run()) {
                $info = array(
                    "nombre" => $this->input->POST("nombre"),
                    "lunes_ingreso" => $this->input->POST("lunes_ingreso") == '' ? NULL : $this->input->POST("lunes_ingreso"),
                    "lunes_egreso" => $this->input->POST("lunes_egreso") == '' ? NULL : $this->input->POST("lunes_egreso"),
                    "martes_ingreso" => $this->input->POST("martes_ingreso") == '' ? NULL : $this->input->POST("martes_ingreso"),
                    "martes_egreso" => $this->input->POST("martes_egreso") == '' ? NULL : $this->input->POST("martes_egreso"),
                    "miercoles_ingreso" => $this->input->POST("miercoles_ingreso") == '' ? NULL : $this->input->POST("miercoles_ingreso"),
                    "miercoles_egreso" => $this->input->POST("miercoles_egreso") == '' ? NULL : $this->input->POST("miercoles_egreso"),
                    "jueves_ingreso" => $this->input->POST("jueves_ingreso") == '' ? NULL : $this->input->POST("jueves_ingreso"),
                    "jueves_egreso" => $this->input->POST("jueves_egreso") == '' ? NULL : $this->input->POST("jueves_egreso"),
                    "viernes_ingreso" => $this->input->POST("viernes_ingreso") == '' ? NULL : $this->input->POST("viernes_ingreso"),
                    "viernes_egreso" => $this->input->POST("viernes_egreso") == '' ? NULL : $this->input->POST("viernes_egreso"),
                    "sabado_ingreso" => $this->input->POST("sabado_ingreso") == '' ? NULL : $this->input->POST("sabado_ingreso"),
                    "sabado_egreso" => $this->input->POST("sabado_egreso") == '' ? NULL : $this->input->POST("sabado_egreso"),
                    "domingo_ingreso" => $this->input->POST("domingo_ingreso") == '' ? NULL : $this->input->POST("domingo_ingreso"),
                    "domingo_egreso" => $this->input->POST("domingo_egreso") == '' ? NULL : $this->input->POST("domingo_egreso"),
                    "tolerancia" => $this->input->POST("tolerancia"),
                    "tiempo_almuerzo" => $this->input->POST("tiempo_almuerzo")
                );
                if ($this->FranjasHorarias_model->Update($id, $info)) {
                    $this->index();
                } else {
                    $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                    $this->Edit($id);
                }
            } else {
                $this->Edit($id);
            }
        } else {
            $this->load->view('login');
        }
    }

    public function Delete($id)
    {
        if ($this->session->userdata('login')) {
            $this->load->model("FranjasHorarias_model");
            $data = array(
                "estado" => "0",
            );
            $this->FranjasHorarias_model->Update($id, $data);
            redirect("control/FranjasHorarias");
        } else {
            $this->load->view('login');
        }
    }

    public function getInfoReporte($idFranja)
    {
        $this->load->model("FranjasHorarias_model");

        $info = array(
            "franja" => $this->FranjasHorarias_model->getFranja($idFranja),
            "legajos" => $this->FranjasHorarias_model->getLegajosPorFranja($idFranja)
        );
        echo json_encode($info);
    }

    public function asignarFranja()
    {
        if ($this->session->userdata('login')) {
            $idFranja = $this->input->POST("franjaAsignacion");
            $legajos = $this->input->POST("legajos_seleccionados");

            if (isset($legajos)) {
                $this->load->model("FranjasHorarias_model");
                $this->FranjasHorarias_model->asignarFranjas($idFranja, $legajos);
            }
            redirect("control/FranjasHorarias");
        }else {
            $this->load->view('login');
        }
    }
}