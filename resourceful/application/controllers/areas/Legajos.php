<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Legajos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Legajos_model");
        $this->load->model("Cargos_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                "legajos" => $this->Legajos_model->GetLegajos()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('legajos/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function GetCiudades($id_provincia)
    {
        $ciudades = $this->Legajos_model->GetCiudades($id_provincia);

        echo json_encode($ciudades);
    }
    public function Add()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                "nacionalidades" => $this->Legajos_model->GetNacionalidades(),
                "provincias" => $this->Legajos_model->GetProvincias(),
                "cargos" => $this->Cargos_model->GetCargos(),

            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('legajos/add', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Delete($id)
    {
        $info  = array(
            'estado' => "0"
        );
        $this->Legajos_model->Update($id, $info);
        echo "areas/legajos";
    }
    public function Store()
    {
        $legajo = $this->input->POST("legajo");
        $numero_documento = $this->input->POST("numero_documento");
        $cuil = $this->input->POST("cuil");
        $cbu = $this->input->POST("cbu");
        $this->form_validation->set_rules("legajo", "Legajo", "is_unique[legajos.num_legajo]");
        $this->form_validation->set_rules("numero_documento", "DNI", "is_unique[legajos.numero_documento]");
        $this->form_validation->set_rules("cuil", "CUIL", "is_unique[legajos.cuil]");
        $this->form_validation->set_rules("cbu", "CBU", "is_unique[legajos.cbu]");
        //Para los grupos fammiliares
        $parentesco = $this->input->POST("parentesco_flia_t");
        $apellido_flia = $this->input->POST("apellido_flia_t");
        $nombre_flia = $this->input->POST("nombre_flia_t");
        $fecha_nacimiento_flia = $this->input->POST("fecha_nacimiento_flia_t");
        $domicilio_flia = $this->input->POST("domicilio_flia_t");
        $dni_flia = $this->input->POST("dni_flia_t");
        if ($this->form_validation->run()) {
            $info = array(
                'num_legajo' => $legajo,
                'numero_documento' => $numero_documento,
                'tipo_documento' => $this->input->POST("tipo_documento"),
                'sexo' => $this->input->POST("sexo"),
                'nombre' => $this->input->POST("nombre"),
                'apellido' => $this->input->POST("apellido"),
                'fecha_nacimiento' => $this->input->POST("fecha_nacimiento"),
                'cuil' => $cuil,
                'telefono' => $this->input->POST("telefono"),
                'mail' => $this->input->POST("mail"),
                'estado_civil' => $this->input->POST("estado_civil"),
                'id_nacionalidad' => $this->input->POST("nacionalidad"),
                'id_provincia' => $this->input->POST("provincia"),
                'id_ciudad' => $this->input->POST("ciudad"),
                'domicilio' => $this->input->POST("domicilio"),
                'fecha_ingreso' => $this->input->POST("fecha_ingreso"),
                'id_cargo' => $this->input->POST("cargo"),
                'carga_horaria' => $this->input->POST("carga_horaria"),
                'sindicato' => $this->input->POST("sindicato"),
                'obra_social' => $this->input->POST("obra_social"),
                'carnet_conducir' => $this->input->post("carnet_conducir") == null ? 0 : 1,
                'banco' => $this->input->POST("banco"),
                'cbu' => $cbu,
                'copia_dni' => $this->input->post("copia_dni") == null ? 0 : 1,
                'partida_nacimiento' => $this->input->post("partida_naci") == null ? 0 : 1,
                'acta_matrimonio' => $this->input->post("acta_matri") == null ? 0 : 1,
                'sumario_convivencia' => $this->input->post("sumario") == null ? 0 : 1,
                'constancia_cuil' => $this->input->post("contancia_cuil") == null ? 0 : 1,
                'f572_rig' => $this->input->post("f572_rig") == null ? 0 : 1,
                'f649_rig' => $this->input->post("f649_rig") == null ? 0 : 1,
                'examen_pre_ocupacional' => $this->input->post("examen_pre_ocupacional") == null ? 0 : 1,
                'contrato' => $this->input->post("contrato") == null ? 0 : 1,
                'afi_sindicato' => $this->input->post("afi_sindicato") == null ? 0 : 1,
                'libreta_trabajo' => $this->input->post("librta_trabajo") == null ? 0 : 1,
                'seguro_vida' => $this->input->post("seguro_vida") == null ? 0 : 1,
                'ps261' => $this->input->post("ps261") == null ? 0 : 1,
                'carnet_sanitario' => $this->input->post("carnet_sanitario") == null ? 0 : 1,
                'fecha_sanitario' => $this->input->POST("fecha_sanitario"),
                'estado' => '1',
            );
            if ($this->Legajos_model->Save($info)) {
                $this->save_flia($parentesco, $apellido_flia, $nombre_flia, $fecha_nacimiento_flia, $domicilio_flia, $dni_flia, $legajo);
                redirect(base_url() . "areas/legajos");
            } else {
                $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                redirect(base_url() . "areas/legajos/add");
            }
        } else {
            $this->Add();
        }
    }
    public function save_flia($parentesco, $apellido_flia, $nombre_flia, $fecha_nacimiento_flia, $domicilio_flia, $dni_flia, $legajo)
    {
        for ($i = 0; $i < count($apellido_flia); $i++) {
            $data  = array(
                'parentesco' => $parentesco[$i],
                'apellido' => $apellido_flia[$i],
                'nombre' => $nombre_flia[$i],
                'fecha_nacimiento' => $fecha_nacimiento_flia[$i],
                'domicilio' => $domicilio_flia[$i],
                'dni' => $dni_flia[$i],
                'id_legajo' => $legajo,

            );
            $this->Legajos_model->save_flia($data);
        }
    }
    public function Edit($id)
    {
        if ($this->session->userdata('login')) {
            $legajo = $this->Legajos_model->GetLegajo($id);
            $info = array(
                "legajo" => $legajo,
                "nacionalidades" => $this->Legajos_model->GetNacionalidades(),
                "provincias" => $this->Legajos_model->GetProvincias(),
                "ciudades" => $this->Legajos_model->GetCiudades($legajo->id_provincia),
                "cargos" => $this->Cargos_model->GetCargos(),
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('legajos/edit', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Update()
    {
        $legajo_oculto = $this->input->POST("legajo_oculto");
        $legajo = $this->input->POST("legajo");
        $numero_documento = $this->input->POST("numero_documento");
        $cuil = $this->input->POST("cuil");
        $cbu = $this->input->POST("cbu");
        $legacctual = $this->Legajos_model->GetLegajo($legajo_oculto);
        $no_rules = TRUE;

        if ($legajo != $legacctual->num_legajo) {
            $no_rules = FALSE;
            $this->form_validation->set_rules("legajo", "Legajo", "is_unique[legajos.num_legajo]");
        }
        if ($numero_documento != $legacctual->numero_documento) {
            $no_rules = FALSE;
            $this->form_validation->set_rules("numero_documento", "Documento", "is_unique[legajos.numero_documento]");
        }
        if ($cuil != $legacctual->cuil) {
            $no_rules = FALSE;
            $this->form_validation->set_rules("cuil", "CUIL", "is_unique[legajos.cuil]");
        }
        if ($cbu != $legacctual->cbu) {
            $no_rules = FALSE;
            $this->form_validation->set_rules("cbu", "CBU", "is_unique[legajos.cbu]");
        }


        if ($no_rules || $this->form_validation->run()) {
            $info = array(
                'num_legajo' => $legajo,
                'numero_documento' => $numero_documento,
                'tipo_documento' => $this->input->POST("tipo_documento"),
                'sexo' => $this->input->POST("sexo"),
                'nombre' => $this->input->POST("nombre"),
                'apellido' => $this->input->POST("apellido"),
                'fecha_nacimiento' => $this->input->POST("fecha_nacimiento"),
                'cuil' => $cuil,
                'telefono' => $this->input->POST("telefono"),
                'mail' => $this->input->POST("mail"),
                'estado_civil' => $this->input->POST("estado_civil"),
                'id_nacionalidad' => $this->input->POST("nacionalidad"),
                'id_provincia' => $this->input->POST("provincia"),
                'id_ciudad' => $this->input->POST("ciudad"),
                'domicilio' => $this->input->POST("domicilio"),
                'fecha_ingreso' => $this->input->POST("fecha_ingreso"),
                'id_cargo' => $this->input->POST("cargo"),
                'carga_horaria' => $this->input->POST("carga_horaria"),
                'sindicato' => $this->input->POST("sindicato"),
                'obra_social' => $this->input->POST("obra_social"),
                'carnet_conducir' => $this->input->post("carnet_conducir") == null ? 0 : 1,
                'banco' => $this->input->POST("banco"),
                'cbu' => $cbu,
                'copia_dni' => $this->input->post("copia_dni") == null ? 0 : 1,
                'partida_nacimiento' => $this->input->post("partida_naci") == null ? 0 : 1,
                'acta_matrimonio' => $this->input->post("acta_matri") == null ? 0 : 1,
                'sumario_convivencia' => $this->input->post("sumario") == null ? 0 : 1,
                'constancia_cuil' => $this->input->post("contancia_cuil") == null ? 0 : 1,
                'f572_rig' => $this->input->post("f572_rig") == null ? 0 : 1,
                'f649_rig' => $this->input->post("f649_rig") == null ? 0 : 1,
                'examen_pre_ocupacional' => $this->input->post("examen_pre_ocupacional") == null ? 0 : 1,
                'contrato' => $this->input->post("contrato") == null ? 0 : 1,
                'afi_sindicato' => $this->input->post("afi_sindicato") == null ? 0 : 1,
                'libreta_trabajo' => $this->input->post("librta_trabajo") == null ? 0 : 1,
                'seguro_vida' => $this->input->post("seguro_vida") == null ? 0 : 1,
                'ps261' => $this->input->post("ps261") == null ? 0 : 1,
                'carnet_sanitario' => $this->input->post("carnet_sanitario") == null ? 0 : 1,
                'fecha_sanitario' => $this->input->POST("fecha_sanitario"),
            );
            if ($this->Legajos_model->Update($legajo_oculto, $info)) {
                redirect(base_url() . "areas/legajos");
            } else {
                $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                redirect(base_url() . "areas/legajos/edit/", $legajo);
            }
        } else {
            $this->Edit($legajo);
        }
    }

    public function GetReport($num_leg)
    {
        $this->load->model("FranjasHorarias_model");
        $legajo = $this->Legajos_model->GetLegajo($num_leg);
        $info = array(
            "legajo" => $legajo,
            "familia" => $this->Legajos_model->GetFamilia($legajo->num_legajo),
            "nacionalidad" => $this->Legajos_model->GetNacionalid($legajo->id_nacionalidad),
            "provincia" => $this->Legajos_model->GetProvincia($legajo->id_provincia),
            "ciudad" => $this->Legajos_model->GetCiudad($legajo->id_ciudad),
            "cargo" => $this->Legajos_model->GetCargo($legajo->id_cargo),
            "horario" => $this->FranjasHorarias_model->GetFranja($legajo->id_franja_horaria)
        );

        $this->load->view("legajos/reporte", $info);
    }
    /***************************************************Grupo familiar*****************************************************/

    public function getFamilia($id)
    {
        $info = array(
            'familia' => $this->Legajos_model->GetFamilia($id),
            'id_legajo' => $id,
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('legajos/familia/list', $info);
        $this->load->view('layouts/footer');
    }

    public function AddFamiliar($id)
    {
        $info = array(
            'legajoID' => $id
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('legajos/familia/add', $info);
        $this->load->view('layouts/footer');
    }
    public function StoreFamiliar()
    {
        $idtoList = $this->input->post('legajoID');
        $info = array(
            'id_legajo' => $this->input->post('legajoID'),
            'nombre' => $this->input->post('nombre_flia'),
            'apellido' => $this->input->post('apellido_flia'),
            'parentesco' => $this->input->post('parentesco_flia'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento_flia'),
            'domicilio' => $this->input->post('domicilio_flia'),
            'dni' => $this->input->post('dni_flia')
        );
        if ($this->Legajos_model->SaveFamiliar($info)) {
            redirect(base_url() . "areas/legajos/getFamilia/$idtoList");
        }
    }
    public function EditFamiliar($id)
    {
        $info = array(
            'familiar' => $this->Legajos_model->EditFamiliar($id)
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('legajos/familia/edit', $info);
        $this->load->view('layouts/footer');
    }
    public function UpdateFamiliar()
    {
        $idFamiliar = $this->input->post('idFamiliar');
        $idtoList  = $this->input->post('legajoID');
        $info = array(
            'id_legajo' => $this->input->post('legajoID'),
            'nombre' => $this->input->post('nombre_flia'),
            'apellido' => $this->input->post('apellido_flia'),
            'parentesco' => $this->input->post('parentesco_flia'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento_flia'),
            'domicilio' => $this->input->post('domicilio_flia'),
            'dni' => $this->input->post('dni_flia')
        );
        if ($this->Legajos_model->UpdateFamiliar($idFamiliar, $info)) {
            redirect(base_url() . "areas/legajos/getFamilia/$idtoList");
        }
    }
    public function DeleteFamiliar()
    {
        $id = $_GET['id'];
        $idFam = $_GET['familiarID'];
        if ($this->Legajos_model->DeleteFamiliar($idFam)) {
            redirect(base_url() . "areas/legajos/getFamilia/$id");
        }
    }
}
