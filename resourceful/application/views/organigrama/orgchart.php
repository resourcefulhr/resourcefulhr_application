<!-- jQuery-UI. Solo se usa en esta vista, no tiene sentido que este en el header -->
<script type="text/javascript" src="<?php echo base_url(); ?>front/plugins/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>front/plugins/jquery-ui/jquery-ui.min.css" />

<!-- PDFkit & Libs -->
<script src="<?php echo base_url(); ?>front/plugins/pdfkit/pdfkit.js"></script>
<script src="<?php echo base_url(); ?>front/plugins/pdfkit/blob-stream.js"></script>
<script src="<?php echo base_url(); ?>front/plugins/FileSaver.js/FileSaver.min.js"></script>

<!-- BasicPrimitives. Solo se usa en esta vista, no tiene sentido que este en el header -->
<script type="text/javascript" src="<?php echo base_url(); ?>front/plugins/basicprimitives/primitives.min.js?5100"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>front/plugins/basicprimitives/primitives.jquery.min.js?5100"></script>
<link href="<?php echo base_url(); ?>front/plugins/basicprimitives/primitives.latest.css?5100" media="screen" rel="stylesheet" type="text/css" />

<script type='text/javascript'>
    document.addEventListener('DOMContentLoaded', function() {
        let control;
        let options = new primitives.orgdiagram.Config();

        let items = generateAreasItems();

        options.items = items;
        options.cursorItem = null;
        options.hasSelectorCheckbox = primitives.common.Enabled.False;

        // Boton de Info
        options.buttons = new Array(new primitives.orgdiagram.ButtonConfig("detalles", "ui-icon-info", "Detalles"));
        options.hasButtons = primitives.common.Enabled.True;
        options.onButtonClick = function(e, data) {
            var base_url = "<?php echo base_url(); ?>";
            $.ajax({
                url: base_url + "areas/organigrama/GetLegajosPorArea",
                data: {
                    idArea: data.context.id
                },
                dataType: "json",
                type: "POST",
                success: function(result) {
                    if (Object.keys(result).length !== 0) {
                        showPanel(result, data.context.title);
                    }
                }
            });
        };

        // renderizo el arbol
        control = primitives.orgdiagram.Control(document.getElementById("basicdiagram"), options);

        // Cosas del panel
        let panel = jQuery("#panel");
        let orgchartform = panel.find("[name=orgchartform]");
        let orgchart = panel.find("[name=orgchart]");
        let panelOptions = new primitives.orgdiagram.Config();

        $("#download_organigrama").on("click", function() {
            download(items, "Organigrama");
        });

        // Funcion para setear el dataset del panel y mostrarlo
        function showPanel(areaId, nombreArea) {
            let panelItems = generatePanelItems(areaId);

            // Atributos del arbol del panel
            panelOptions.items = panelItems;
            panelOptions.cursorItem = null;

            // Para que muesre solo 4 columnas en la matriz
            //panelOptions.maximumColumnsInMatrix = 4;

            // Boton de personita
            let personButton = new primitives.orgdiagram.ButtonConfig("detalles", "ui-icon-person", "Detalles");
            personButton.size = new primitives.common.Size(24, 24);
            panelOptions.buttons = new Array(personButton);
            panelOptions.hasButtons = primitives.common.Enabled.True;
            panelOptions.onButtonClick = function(e, data) {
                var base_url = "<?php echo base_url(); ?>";
                //llama al reporte de legajos
                $.ajax({
                    url: "legajos/GetReport/" + data.context.title.split(" ")[1],
                    type: "POST",
                    dataType: "html",
                    success: function(data) {
                        $("#modalReport").modal("show");
                        $("#modalReport .modal-body").html(data);
                    }
                })
            };
            //imprime informe de legajo
            $(document).on("click", ".btn-print", function() {
                $("#modalReport .modal-body").print({});
            });
            // Fin boton de personita

            // Remuevo los checkboxes que no queremos
            panelOptions.hasSelectorCheckbox = primitives.common.Enabled.False;

            // Para q no minimize los nodos
            panelOptions.pageFitMode = primitives.orgdiagram.PageFitMode.None;

            orgchart.orgDiagram(panelOptions);

            orgchartform.dialog({
                autoOpen: false,
                minWidth: 600,
                minHeight: 720,
                modal: true,
                title: nombreArea,
                buttons: [{
                    id: "download_area",
                    text: "Descargar",
                    class: "btn btn-default",
                    click: function() {
                        download(panelItems, nombreArea);
                    }
                }],
                resizeStop: function(event, ui) {
                    let panelSize = new primitives.common.Rect(0, 0, orgchartform.outerWidth(), orgchartform.outerHeight());
                    orgchart.css(panelSize.getCSS());
                    orgchart.orgDiagram("update", primitives.orgdiagram.UpdateMode.Recreate);
                },
                open: function(event, ui) {
                    let panelSize = new primitives.common.Rect(0, 0, orgchartform.outerWidth(), orgchartform.outerHeight());
                    orgchart.css(panelSize.getCSS());
                    /* Chart is already created, so in regular situation we have to use Refresh update mode,
                    but here jQuery removes dialog contents and adds them back, so that procedure
                    invalidates graphics element, so when we open dialog we use full Redraw update mode. */
                    orgchart.orgDiagram("update", primitives.orgdiagram.UpdateMode.Recreate);
                }
            }).dialog("open");
        }

        function generateAreasItems() {
            let areas = <?php echo $areas; ?>;
            let items = [];

            jQuery.each(areas, function() {
                item = new primitives.orgdiagram.ItemConfig({
                    id: this.id,
                    parent: (this.id === this.id_area) ? null : this.id_area,
                    title: this.nombre_area,
                    description: this.descripcion,
                    image: "<?php echo base_url(); ?>front/images/area.jpg"
                });
                items.push(item);
            });
            return items;
        }

        function generatePanelItems(legajos) {
            let panelItems = [
                new primitives.orgdiagram.ItemConfig({
                    id: 0,
                    title: "invisible",
                    parent: null,
                    isVisible: false,
                    childrenPlacementType: primitives.common.ChildrenPlacementType.Matrix,
                })
            ];

            jQuery.each(legajos, function(index, value) {
                item = new primitives.orgdiagram.ItemConfig({
                    id: index + 1,
                    parent: 0,
                    title: "Legajo: " + value.num_legajo,
                    itemTitleColor: colorByIndex(value.id_cargo),
                    description: value.nombre + " " + value.apellido,
                    groupTitle: value.cargo,
                    groupTitleColor: colorByIndex(value.id_cargo),
                    image: "<?php echo base_url(); ?>front/images/avatar.png"
                });
                panelItems.push(item);
            });
            return panelItems;
        }

        function colorByIndex(index) {
            var colors = primitives.common.Colors;
            var keys = Object.keys(colors)
            while (index >= keys.length) {
                index -= keys.length;
            }
            return colors[keys[index]];
        }

        function download(items, titulo) {
            var newItems = [];
            for (var index = 0; index < items.length; index += 1) {
                var item = items[index];
                newItems.push({
                    id: item.id,
                    parent: item.parent,
                    title: item.title,
                    itemTitleColor: item.itemTitleColor,
                    description: item.description,
                    groupTitle: item.groupTitle,
                    groupTitleColor: item.groupTitleColor,
                    isVisible: item.isVisible,
                    childrenPlacementType: item.childrenPlacementType,
                    image: null
                });
            }
            var firstOrganizationalChartSample = primitives.pdf.orgdiagram.Plugin({
                items: newItems,
                cursorItem: null,
                hasSelectorCheckbox: primitives.common.Enabled.False
            });

            var sampleSize = firstOrganizationalChartSample.getSize();
            // create a document and pipe to a blob
            var doc = new PDFDocument({
                size: [sampleSize.width + 100, sampleSize.height + 150]
            });
            var stream = doc.pipe(blobStream());

            doc.fontSize(25)
                .text(titulo, 30, 30);

            firstOrganizationalChartSample.draw(doc, 30, 60);

            doc.restore();

            doc.end();

            if (typeof stream !== 'undefined') {
                stream.on('finish', function() {
                    var string = stream.toBlob('application/pdf');
                    window.saveAs(string, 'organigrama.pdf');
                });
            } else {
                alert('Error: Failed to create file stream.');
            }
        }
    });
</script>

<section class="content">
    <h1>
        Areas
        <small>Organigrama</small>
    </h1>

    <div class="card">
        <div class="body">
            <input type="button" class="btn btn-default" id="download_organigrama" value="Descargar">
            <div id="basicdiagram" style="width: 100%; height: 720px;"></div>
            <div id="panel">
                <div name="orgchartform" class="dialog-form" style="overflow: hidden;">
                    <div name="orgchart" class="bp-item" style="overflow: hidden; padding: 0px; margin: 0px; border: 0px;"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- modal -->
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <hr>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-print">Descargar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

            </div>
        </div>
    </div>
</div>