<section class="content">
    <h1>
        APTITUDES
        <small>Listado</small>
    </h1>
    <div class="card">
        <div class="body">
            <?php if(in_array("Reclutamiento -> Aptitudes", $this->session->userdata("permisos_insertar"))): ?>
            <div class="input-group">
                <div class="input-group">
                    <button type="button" class="btn btn-success waves-effect" data-toggle="modal" data-target="#defaultModal">
                        <i class=material-icons>
                            add_box
                        </i>
                        Agregar Aptitud
                    </button>
                </div>
            </div>
            <hr>
            <?php endif; ?>
            <?php if ($this->session->flashdata("ERROR")) : ?>
                <div class="alert alert-danger">
                    <p><?php echo $this->session->flashdata("ERROR") ?></p>
                </div>
                <hr>
            <?php endif; ?>
            <div>
                <table id="dataTable" class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th class="col-md-1">#</th>
                            <th>Aptitud</th>
                            <th class="col-md-2">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($aptitudes)) : ?>
                            <?php foreach ($aptitudes as $aptitud) : ?>
                                <tr>
                                    <td><?php echo $aptitud->id; ?></td>
                                    <td><?php echo $aptitud->aptitud; ?></td>
                                    <td>
                                        <div class="btn-group">
                                            <?php if(in_array("Reclutamiento -> Aptitudes", $this->session->userdata("permisos_modificar"))): ?>
                                            <button type="button" id="edit-aptitud" class="btn btn-warning waves-effect edit-aptitud" data-toggle="modal" data-target="#defaultModal1" value="<?php echo $aptitud->id; ?>">
                                                <i class=material-icons>mode_edit</i>
                                            </button>
                                            <?php endif; ?>

                                            <?php if(in_array("Reclutamiento -> Aptitudes", $this->session->userdata("permisos_baja"))): ?>
                                            <a href="<?php echo base_url(); ?>reclutamiento/aptitudes/Delete/<?php echo $aptitud->id; ?>" class="btn btn-danger waves-effect"><i class=material-icons>delete_forever</i></a>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Agregar Aptitud</h4>
            </div>
            <div class="modal-body">
                <form class="form-line" action="<?php echo base_url(); ?>reclutamiento/aptitudes/add" method="POST">
                    <label>Descripcion de la aptitud:</label>
                    <input type="text" name="aptitud" class="form-control" required>
                    <br>
                    <button type="submit" class="btn btn-success" value="Guardar">Guardar</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Editar aptitud</h4>
            </div>
            <div class="modal-body">
                <form class="form-line" action="<?php echo base_url(); ?>reclutamiento/aptitudes/Update" method="POST">
                    <label>Descripcion de la aptitud:</label>
                    <div id="body">

                    </div>
                    <br>
                    <button type="submit" class="btn btn-success" value="Guardar">Guardar</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var base_url = "<?php echo base_url(); ?>";
        $(".edit-aptitud").on("click", function() {
            var id = $(this).val();
            $.ajax({
                url: base_url + "reclutamiento/aptitudes/getAptitudEdit",
                data: {
                    idaptitud: id
                },
                type: "POST",
                success: function(resp) {
                    document.getElementById("body").innerHTML = resp;
                }
            });
        });
    });
</script>