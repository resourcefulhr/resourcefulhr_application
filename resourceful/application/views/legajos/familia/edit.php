<section class="content">
    <h1>
        Familiar
        <small>Editar</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>areas/legajos/UpdateFamiliar" method="POST">
                <div class="card">
                    <div class="body">
                        <div>
                            <h2>FAMILIARES</h2>
                            <hr>
                        </div>
                        <input type="hidden" name="idFamiliar" value="<?php echo $id = $familiar->id;?>">
                        <input type="hidden" name="legajoID" value="<?php echo $id = $familiar->id_legajo;?>">
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <label>Nombre</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <div class="form-line focused">
                                        <input type="text" name="nombre_flia" id="nombre_flia" class="form-control" 
                                        value="<?php echo $familiar->nombre;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <label>Apellido</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <div class="form-line focused">
                                        <input type="text" name="apellido_flia" id="apellido_flia" class="form-control" 
                                        value="<?php echo $familiar->apellido;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <label>Parentesco</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <select name="parentesco_flia" id="parentesco_flia" class="form-control">
                                        <option value="Padre">Padre</option>
                                        <option value="Madre">Madre</option>
                                        <option value="Conyuge">Conyuge</option>
                                        <option value="Hijo/a">Hijo/a</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <label>Fecha de Nacimiento</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                    <div class="form-line focused">
                                        <input type="date" name="fecha_nacimiento_flia" id="fecha_nacimiento_flia" class="form-control"
                                        value="<?php echo $familiar->fecha_nacimiento;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <label>Domicilio</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <div class="form-line focused">
                                        <input type="text" name="domicilio_flia" id="domicilio_flia" class="form-control"
                                        value="<?php echo $familiar->domicilio;?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>DNI</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <div class="form-line focused">
                                        <input type="number" name="dni_flia" id="dni_flia" class="form-control" maxlength="8"
                                        value="<?php echo $familiar->dni;?>" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                            <a href="<?php echo base_url(); ?>areas/legajos/getFamilia/<?php echo $id?>" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                                </i> Cancelar</a>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</section>