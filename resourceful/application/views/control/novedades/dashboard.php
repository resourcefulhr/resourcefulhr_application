<section class="content">
    <div class="col-sm-6">
        <div class="info-box-3 bg-blue waves-effect hover-zoom-effect" style="cursor:pointer;" onclick="mostrarNovedades()">
            <div class="icon">
                <i class="material-icons">bookmarks</i>
            </div>
            <div class="content">
                <div class="text">NOVEDADES:</div>
                <div class="number" id="novedades-banner-count"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="info-box-3 bg-orange waves-effect hover-zoom-effect" style="cursor:pointer;" onclick="mostrarNovedadesAlmuerzo()">
            <div class="icon">
                <i class="material-icons">fastfood</i>
            </div>
            <div class="content">
                <div class="text">NOVEDADES ALMUERZO:</div>
                <div class="number" id="novedades-almuerzo-banner-count"></div>
            </div>
        </div>
    </div>
    <hr style="border-top: 1px solid white;">
    <div class="container-fluid row" id="container-novedades" style="display:none;">
        <div class="block-header">
            <h1>NOVEDADES</h1>
            <div class="col-sm-2">
                <label for="estado_novedad">Estado:</label>
                <select tabindex="-98" class="form-control show-tick novedades-filter" multiple="" data-selected-text-format="count" id="estado_novedad" name="estado_novedad">
                    <option selected>ABIERTA</option>
                    <option>JUSTIFICADA</option>
                    <option>NO JUSTIFICADA</option>
                </select>
            </div>
            <div class="col-sm-2">
                <label for="tipo_novedad">Tipo:</label>
                <select tabindex="-98" class="form-control show-tick novedades-filter" multiple="" data-selected-text-format="count" id="tipo_novedad" name="tipo_novedad">
                    <option selected>INASISTENCIA</option>
                    <option selected>REGISTRO INCONSISTENTE</option>
                    <option selected>HORARIO INCONSISTENTE</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label>Legajos:</label>
                <select tabindex="-98" class="form-control show-tick novedades-filter" multiple="" data-live-search="true" data-selected-text-format="count" id="legajos_novedad" name="legajos_novedad">
                    <?php foreach($legajos as $legajo): ?>
                        <option value="<?php echo $legajo->num_legajo?>">
                            <?php echo $legajo->nombre . " " . $legajo->apellido . ", " . $legajo->num_legajo; ?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-3" id="datepicker_novedad">
                <label></label>
                <div class="input-group input-daterange" data-provide="datepicker">
                    <div class="input-group-addon">Desde</div>
                    <input id="novedades-start-date" name="novedades-start-date" type="text" class="form-control novedades-filter" data-date-container='#datepicker_novedad'>
                    <div class="input-group-addon">Hasta</div>
                    <input id="novedades-end-date" name="novedades-end-date" type="text" class="form-control novedades-filter" data-date-container='#datepicker_novedad'>
                </div>
            </div>
            <div class="col-sm-2">
                <label></label>
                <input type="button" class="form-control" value="Imprimir Reporte" onclick="imprimirFiltro()" title="Incluye Novedades, Vacaciones y Licencias para el filtro seleccionado.">
            </div>
        </div>
        <!-- Novedades -->
        <div class="row clearfix" id="tablero_novedades"></div>
    </div>
    <div class="container-fluid row" id="container-novedades-almuerzo" style="display:none;">
        <div class="block-header">
            <h1>NOVEDADES ALMUERZO</h1>
            <div class="col-sm-3">
                <label for="estado_novedad_almuerzo">Estado:</label>
                <select tabindex="-98" class="form-control show-tick novedades-almuerzo-filter" multiple="" data-selected-text-format="count" id="estado_novedad_almuerzo" name="estado_novedad_almuerzo">
                    <option selected>ABIERTA</option>
                    <option>JUSTIFICADA</option>
                    <option>NO JUSTIFICADA</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label for="tipo_novedad_almuerzo">Tipo:</label>
                <select tabindex="-98" class="form-control show-tick novedades-almuerzo-filter" multiple="" data-selected-text-format="count" id="tipo_novedad_almuerzo" name="tipo_novedad_almuerzo">
                    <option selected>INASISTENCIA</option>
                    <option selected>REGISTRO INCONSISTENTE</option>
                    <option selected>HORARIO INCONSISTENTE</option>
                </select>
            </div>
            <div class="col-sm-3">
                <label>Legajos:</label>
                <select tabindex="-98" class="form-control show-tick novedades-almuerzo-filter" multiple="" data-live-search="true" data-selected-text-format="count" id="legajos_novedad_almuerzo" name="legajos_novedad_almuerzo">
                    <?php foreach($legajos as $legajo): ?>
                        <option value="<?php echo $legajo->num_legajo?>">
                            <?php echo $legajo->nombre . " " . $legajo->apellido . ", " . $legajo->num_legajo; ?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="col-sm-3" id="datepicker_novedad_almuerzo">
                <label></label>
                <div class="input-group input-daterange" data-provide="datepicker">
                    <div class="input-group-addon">Desde</div>
                    <input id="novedades-almuerzo-start-date" name="novedades-almuerzo-start-date" type="text" class="form-control novedades-almuerzo-filter" data-date-container='#datepicker_novedad_almuerzo'>
                    <div class="input-group-addon">Hasta</div>
                    <input id="novedades-almuerzo-end-date" name="novedades-almuerzo-end-date" type="text" class="form-control novedades-almuerzo-filter" data-date-container='#datepicker_novedad_almuerzo'>
                </div>
            </div>
        </div>
        <!-- Novedades -->
        <div class="row clearfix" id="tablero_novedades_almuerzo"></div>
    </div>
</section>

<!-- modal para mostrar novedad -->
<div class="modal fade" id="modal-novedades" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
$(function() {
    generarNovedades();
    inicializarVistas();

    $('.novedades-filter').change(function() {
        popularNovedades();
    });

    $('.novedades-almuerzo-filter').change(function() {
        popularNovedadesAlmuerzo();
    });

    $("#modal-novedades").on("hidden.bs.modal", function() {
        popularNovedades();
    });
});

function generarNovedades() {
    let desde = '<?php echo $desde; ?>';
    let desde_date = new Date(Date.parse(desde.replace('-','/','g')));

    let hasta = '<?php echo $hasta; ?>';
    let hasta_date = new Date(Date.parse(hasta.replace('-','/','g')));

    let mensaje;

    <?php if(in_array("Control -> Novedades", $this->session->userdata("permisos_insertar"))): ?>
    if (desde_date.getTime() > hasta_date.getTime()) {
        return false;
    } else if (desde_date.getTime() == hasta_date.getTime()) {
        mensaje = `Esta a punto de generar novedades para el dia ${desde}. Esta operacion es irreversible.\n¿Esta seguro?`;
    } else if (desde_date < hasta_date) {
        mensaje = `Esta a punto de generar novedades desde el dia ${desde} hasta el dia ${hasta} (inclusive). Esta operacion es irreversible.\n¿Esta seguro?`;
    }
    <?php endif; ?>

    if (mensaje) {
        swal({
            title: "Generacion de Novedades",
            text: mensaje,
            type: "warning",
            inputType: "submit",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (isConfirm) {
                let base_url = "<?php echo base_url(); ?>";
                var timeout = setTimeout(function() {
                    swal("Oops", "Timeout generando novedades!", "error");
                }, 119999);
                $.ajax({
                    url: base_url + "control/Novedades/GenerarNovedades",
                    type: "POST",
                    success: function(resp) { }
                }).done(function(data) {
                    inicializarVistas();
                    clearTimeout(timeout);
                    swal("Exito", "Generacion de Novedades Finalizada con Exito", "info");
                }).error(function(data) {
                    swal("Oops", "Error Generando Novedades!", "error");
                });
            }
        });
    }
}

function inicializarVistas() {
    popularNovedades();
    popularNovedadesAlmuerzo();
    mostrarNovedades();
}

function popularNovedades() {
    let base_url = '<?php echo base_url(); ?>'
    let desde_completo = $("#datepicker_novedad input[name=novedades-start-date]").datepicker("getDate");
    let desde;
    if (desde_completo) {
        desde = desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2);
    }

    let hasta_completo = $("#datepicker_novedad input[name=novedades-end-date]").datepicker("getDate");
    let hasta;
    if (hasta_completo) {
        hasta = hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2);
    }
    $.ajax({
        url: base_url + "control/Novedades/GetNovedades",
        data: {
            tipo: JSON.stringify($('#tipo_novedad').val()),
            estado: JSON.stringify($('#estado_novedad').val()),
            legajos: JSON.stringify($('#legajos_novedad').val()),
            desde: desde,
            hasta: hasta
        },
        type: "POST",
        success: function(result) {
            $("#tablero_novedades").html("");
            if ($.trim(result)) {
                $("#tablero_novedades").append(result);
                $("#novedades-banner-count").html($("#novedades-count").val());
            } else {
                $("#tablero_novedades").append("<h4 class='col-xs-12 text-center'>No Existen Novedades para el filtro seleccionado.</h4>");
                $("#novedades-banner-count").html("0");
            }
        }
    });
}

function popularNovedadesAlmuerzo() {
    let base_url = '<?php echo base_url(); ?>'
    let desde_completo = $("#datepicker_novedad_almuerzo input[name=novedades-almuerzo-start-date]").datepicker("getDate");
    let desde;
    if (desde_completo) {
        desde = desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2);
    }

    let hasta_completo = $("#datepicker_novedad_almuerzo input[name=novedades-almuerzo-end-date]").datepicker("getDate");
    let hasta;
    if (hasta_completo) {
        hasta = hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2);
    }
    $.ajax({
        url: base_url + "control/Novedades/GetNovedadesAlmuerzo",
        data: {
            tipo_almuerzo: JSON.stringify($('#tipo_novedad_almuerzo').val()),
            estado_almuerzo: JSON.stringify($('#estado_novedad_almuerzo').val()),
            legajos_almuerzo: JSON.stringify($('#legajos_novedad_almuerzo').val()),
            desde_almuerzo: desde,
            hasta_almuerzo: hasta
        },
        type: "POST",
        success: function(result) {
            $("#tablero_novedades_almuerzo").html("");
            if ($.trim(result)) {
                $("#tablero_novedades_almuerzo").append(result);
                $("#novedades-almuerzo-banner-count").html($("#novedades-almuerzo-count").val());
            } else {
                $("#tablero_novedades_almuerzo").append("<h4 class='col-xs-12 text-center'>No Existen Novedades para el filtro seleccionado.</h4>");
                $("#novedades-almuerzo-banner-count").html("0");
            }
        }
    });
}

function mostrarNovedades() {
    $("#container-novedades").show();
    $("#container-novedades-almuerzo").hide();
}

function mostrarNovedadesAlmuerzo() {
    $("#container-novedades").hide();
    $("#container-novedades-almuerzo").show();
}

function justificarNovedadAlmuerzo(id) {
    swal({
        title: "Justificar Novedad",
        text: "Esta a punto de justificar una Novedad. Esta accion es irreversible, y no generará ningun ajuste para el empleado.\n ¿Desea Continuar?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true,
    }, function (isConfirm) {
        if (isConfirm) {
            let base_url = "<?php echo base_url(); ?>";
            $.ajax({
                url: base_url + "control/Novedades/JustificarNovedadAlmuerzo",
                data: { id: id },
                type: "POST",
            }).done(function(data) {
                popularNovedadesAlmuerzo();
            }).error(function(data) {
                popularNovedadesAlmuerzo();
                swal("Oops", "Error Justificando Novedad!", "error");
            });
        }
    });
}

function injustificarNovedadAlmuerzo(id) {
    swal({
        title: "Injustificar Novedad",
        text: "Esta a punto de NO justificar una Novedad. Esta accion es irreversible, y no generará ningun ajuste para el empleado.\n ¿Desea Continuar?",
        type: "warning",
        showCancelButton: true,
        closeOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {
            let base_url = "<?php echo base_url(); ?>";
            $.ajax({
                url: base_url + "control/Novedades/InjustificarNovedadAlmuerzo",
                data: { id: id },
                type: "POST",
            }).done(function(data) {
                popularNovedadesAlmuerzo();
            }).error(function(data) {
                popularNovedadesAlmuerzo();
                swal("Oops", "Error Injustificando Novedad!", "error");
            });
        }
    });
}

function cerrarNovedad(idNovedad) {
    let base_url = "<?php echo base_url(); ?>";
    $.ajax({
        url: base_url + "control/Novedades/ModalCerrarNovedad",
        data: { id: idNovedad },
        type: "GET",
    }).done(function(data) {
        $("#modal-novedades .modal-content").html(data);
        $("#modal-novedades").modal("show");
    }).error(function(data) {
        swal("Oops", "Error", "error");
    });
}

function infoNovedad(idNovedad) {
    let base_url = "<?php echo base_url(); ?>";
    $.ajax({
        url: base_url + "control/Novedades/ModalInfoNovedad",
        data: { id: idNovedad },
        type: "GET",
    }).done(function(data) {
        $("#modal-novedades .modal-content").html(data);
        $("#modal-novedades").modal("show");
    }).error(function(data) {
        swal("Oops", "Error", "error");
    });
}

function imprimirFiltro() {
    let base_url = "<?php echo base_url(); ?>";
    let desde_completo = $("#datepicker_novedad input[name=novedades-start-date]").datepicker("getDate");
    let desde;
    if (desde_completo) {
        desde = desde_completo.getFullYear() + "-" + ("0" + (desde_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + desde_completo.getDate()).slice(-2);
    }

    let hasta_completo = $("#datepicker_novedad input[name=novedades-end-date]").datepicker("getDate");
    let hasta;
    if (hasta_completo) {
        hasta = hasta_completo.getFullYear() + "-" + ("0" + (hasta_completo.getMonth() + 1)).slice(-2) + "-" + ("0" + hasta_completo.getDate()).slice(-2);
    }
    $.ajax({
        url: base_url + "control/Novedades/ReporteNovedades",
        data: {
            tipo: JSON.stringify($('#tipo_novedad').val()),
            estado: JSON.stringify($('#estado_novedad').val()),
            legajos: JSON.stringify($('#legajos_novedad').val()),
            desde: JSON.stringify(desde),
            hasta: JSON.stringify(hasta)
        },
        type: "POST",
    }).done(function(data) {
        if ($.trim(data)) {
            $("#modal-novedades .modal-content").html(data);
            $("#modal-novedades").modal("show");
        }
    }).error(function(data) {
        swal("Oops", "Error", "error");
    });
}
</script>