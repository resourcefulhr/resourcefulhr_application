<style>
    .centrar {
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>
<section>
    <div class="row">
        <div class="col-xs-12 text-center">
            <div class="bg-light-blue">
                <img src="../front/images/aa/1.png" alt=""><br>
                <b>Alquileres Argentia S.A</b><br>
                <b>Reporte de Legajo</b><br>
                <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
            </div>
        </div>
    </div>
    <hr>
</section>
<section>
    <h1 class="text-center"><b>Informacion Personal</b></h1>
    <hr>
    <div class="row">
        <div class="col-xs-4">
            <label class="input-group ">Nombre: <br> <?php echo $legajo->nombre; ?></label><br>
            <label class="input-group ">Fecha de nacimiento: <br> <?php echo $legajo->fecha_nacimiento; ?> </label><br>
            <label class="input-group ">CUIL: <br> <?php echo $legajo->cuil; ?> </label><br>
            <label class="input-group ">Nacionalidad: <br> <?php echo $nacionalidad->PAIS_NAC; ?></label><br>
            <label class="input-group ">Domicilio: <br> <?php echo $legajo->domicilio; ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Apellido: <br> <?php echo $legajo->apellido; ?> </label><br>
            <label class="input-group ">Tipo de documento: <br> <?php echo $legajo->tipo_documento; ?> </label><br>
            <label class="input-group ">Estado civil: <br> <?php echo $legajo->estado_civil; ?> </label><br>
            <label class="input-group ">Provincia:<br> <?php echo $provincia->provincia_nombre; ?> </label> <br>
            <label class="input-group ">Telefono: <br> <?php echo $legajo->telefono; ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Numero de legajo: <br> <?php echo $legajo->num_legajo; ?> </label><br>
            <label class="input-group ">Numero de documento: <br> <?php echo $legajo->numero_documento; ?> </label><br>
            <label class="input-group ">Sexo: <br> <?php echo $legajo->sexo; ?> </label><br>
            <label class="input-group ">Ciudad: <br> <?php echo $ciudad->ciudad_nombre; ?> </label><br>
            <label class="input-group ">Email: <br> <?php echo $legajo->mail; ?> </label><br>
        </div>
    </div>
</section>
<hr>
<section>
    <h1 class="text-center"><b>Grupo familiar</b></h1>
    <hr>
    <h4 class="text-center"><b>Listado de familiares</b></h4>
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr class="bg-blue-grey">
                <th>Apellido</th>
                <th>Nombre</th>
                <th>DNI</th>
                <th>Parentesco</th>
                <th>Fecha de nacimiento</th>
                <th>Domicilio</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($familia)) : ?>
                <?php foreach ($familia as $family) : ?>
                    <tr>
                        <td><?php echo $family->apellido; ?></td>
                        <td><?php echo $family->nombre; ?></td>
                        <td><?php echo $family->dni; ?></td>
                        <td><?php echo $family->parentesco; ?></td>
                        <td><?php echo $family->fecha_nacimiento; ?></td>
                        <td><?php echo $family->domicilio; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</section>
<hr>
<section>
    <h1 class="text-center"><b>Informacion del empleado</b></h1>
    <hr><br>
    <div class="row">
        <div class="col-xs-4">
            <label class="input-group ">Fecha de ingreso a la empresa: <br> <?php echo $legajo->fecha_ingreso; ?> </label><br>
            <label class="input-group ">Sindicato: <br> <?php echo $legajo->sindicato; ?> </label><br>
            <label class="input-group ">Fecha de egreso: <br> <?php echo $legajo->fecha_egreso == null ? "Actualmente empleado" : $legajo->fecha_egreso; ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Cargo : <br> <?php echo $cargo->cargo; ?> </label><br>
            <label class="input-group ">Obra social: <br> <?php echo $legajo->obra_social; ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Carga Horaria: <br> <?php echo $legajo->carga_horaria; ?> </label><br>
            <label class="input-group ">Carnet de Conducir: <br> <?php echo $legajo->carnet_conducir == 1 ? " Si " : " No " ?> </label><br>
        </div><br>
    </div>
    <div class="row">
        <h3 class="text-center"><b>Datos bancarios</b></h3><br>
        <div class="col-xs-6">
            <label class="input-group centrar">Banco: <br> <?php echo $legajo->banco; ?> </label><br>
        </div>
        <div class="col-xs-6">
            <label class="input-group centrar">CBU: <br> <?php echo $legajo->cbu; ?> </label><br>
        </div>
    </div>
</section>
<hr>
<section>
    <h1 class="text-center"><b>Documentacion</b></h1>
    <hr>
    <div class="row">
        <div class="col-xs-4">
            <label class="input-group ">Fotocopia del DNI: <br> <?php echo $legajo->copia_dni == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Sumario de comvivencia: <br> <?php echo $legajo->sumario_convivencia == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">f649_rig: <br> <?php echo $legajo->f649_rig == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Libreta de trabajo: <br> <?php echo $legajo->libreta_trabajo == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">PS261: <br> <?php echo $legajo->ps261 == 1 ? " Si " : " No " ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Partida de nacimiento: <br><?php echo $legajo->partida_nacimiento == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Contancia de CUIL: <br> <?php echo $legajo->constancia_cuil == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Examen pre-ocupacional: <br> <?php echo $legajo->examen_pre_ocupacional == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Afiliado al sindicato: <br> <?php echo $legajo->afi_sindicato == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Carnet sanitario: <br> <?php echo $legajo->carnet_sanitario == 1 ? " Si " : " No " ?> </label><br>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Acta de matrimonio: <br> <?php echo $legajo->acta_matrimonio == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">f572_rig: <br> <?php echo $legajo->f572_rig == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Seguro de vida: <br> <?php echo $legajo->seguro_vida == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Contrato: <br> <?php echo $legajo->contrato == 1 ? " Si " : " No " ?> </label><br>
            <label class="input-group ">Fecha de carnet sanitario: <br> <?php echo $legajo->fecha_sanitario == 1 ? " Si " : " No " ?> </label><br>
        </div>
    </div>
</section>
<hr>
<section>
    <h1 class="text-center"><b>Horario de Trabajo</b></h1>
    <hr>
    <h3 class="text-center"><b>Informacion de Franja Horaria:</b></h3>
    <div class="row">
        <div class="col-xs-4">
            <label class="input-group ">Nombre: <br><?php echo $horario->nombre; ?></label>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Tolerancia: <br><?php echo $horario->tolerancia; ?> min</label>
        </div>
        <div class="col-xs-4">
            <label class="input-group ">Tiempo de Almuerzo: <br><?php echo $horario->tiempo_almuerzo; ?> min</label>
        </div>
    </div>
    <hr>
    <table class="table table-bordered table-striped table-hover" id="horarioReporte">
        <thead class="bg-blue-grey">
            <tr>
                <th>Lunes</th>
                <th>Martes</th>
                <th>Miercoles</th>
                <th>Jueves</th>
                <th>Viernes</th>
                <th>Sabado</th>
                <th>Domingo</th>
            </tr>
        <thead>

        <tbody>
            <tr>
                <td><?php echo substr($horario->lunes_ingreso, 0, -3) . " - " . substr($horario->lunes_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->martes_ingreso, 0, -3) . " - " . substr($horario->martes_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->miercoles_ingreso, 0, -3) . " - " . substr($horario->miercoles_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->jueves_ingreso, 0, -3) . " - " . substr($horario->jueves_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->viernes_ingreso, 0, -3) . " - " . substr($horario->viernes_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->sabado_ingreso, 0, -3) . " - " . substr($horario->sabado_egreso, 0, -3); ?></td>
                <td><?php echo substr($horario->domingo_ingreso, 0, -3) . " - " . substr($horario->domingo_egreso, 0, -3); ?></td>
            </tr>
        </tbody>
    </table>
    <hr>
</section>