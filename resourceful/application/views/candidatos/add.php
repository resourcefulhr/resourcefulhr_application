<section class="content">
    <h1>
        Candidatos
        <small>Nuevo</small>
    </h1>
    <form action="StoreCV" id="frmFileUpload" method="post" enctype="multipart/form-data">
        <div class="card">
            <div class="body">
                <div>
                    <h2>DATOS DE CONTANCTO</h2>
                    <hr>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-5">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused ">
                                <input type="text" name="nombre" class="form-control" required value="<?php echo set_value("nombre"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label>Apellido</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="apellido" class="form-control" required value="<?php echo set_value("apellido"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Sexo</label>
                        <div class="input-group">
                            <select name="sexo" id="sexo" class="form-control" required>
                                <option value="">Seleccione...</option>
                                <option value="Femenino" <?php echo "Femenino" == set_value("sexo") ? "selected" : "" ?>>Femenino</option>
                                <option value="Masculino" <?php echo "Masculino" == set_value("sexo") ? "selected" : "" ?>>Masculino</option>
                                <option value="Otro" <?php echo "Otro" == set_value("sexo") ? "selected" : "" ?>>Otro</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Fecha de nacimiento</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line focused">

                                <input type="date" name="fecha_nacimiento" class="form-control" required value="<?php echo set_value("fecha_nacimiento"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Telefono</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">phone</i>
                            </span>
                            <div class="form-line focused">
                                <input type="number" name="telefono" class="form-control" required value="<?php echo set_value("telefono"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Puesto/Perfil</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">assignment_ind</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="puesto" class="form-control" required value="<?php echo set_value("puesto"); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <label>Email</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="email" class="form-control email" placeholder="Ex: example@example.com" required value="<?php echo set_value("email"); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">list_alt</i>
                            </span>
                            <div class="form-line focused">
                                <label for="">observaciones</label>
                                <div class="md-form">
                                    <textarea name="observaciones" id="observaciones" class="md-textarea form-control" rows="2"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-14">
            <div class="card">
                <div class="body">
                    <label>Cargar curriculum</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">folder_shared</i>
                        </span>
                        <div class="form-line focused">
                            <input type="file" name="file" class="form-control">
                            <input type="hidden" name="fecha_cv" value="<?php echo date("Y-m-d"); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
            <a href="<?php echo base_url()?>reclutamiento/candidatos/" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                </i> Cancelar</a>
        </div>
    </form>
</section>