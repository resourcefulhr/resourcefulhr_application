<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CalendarioFeriados extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('control/feriados');
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    
    public function GetFeriados() 
    {
        $this->load->model("CalendarioFeriados_model");
        echo json_encode($this->CalendarioFeriados_model->GetFeriados());
    }
    
    public function EliminarFeriado() {
        $this->load->model("CalendarioFeriados_model");
        
        $id = $this->input->POST("id");
        
        $this->CalendarioFeriados_model->EliminarFeriado($id);
        
        $this->GetFeriados();
    }
    
    public function EditarFeriado()
    {
        $this->load->model("CalendarioFeriados_model");
        
        $id = $this->input->POST("id");
        $nombre = $this->input->POST("nombre");
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");
        
        $this->CalendarioFeriados_model->EditarFeriado($id, $nombre, $desde, $hasta);
        
        $this->GetFeriados();
    }
    
    public function GuardarFeriado()
    {
        $this->load->model("CalendarioFeriados_model");
        
        $nombre = $this->input->POST("nombre");
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");
        
        $this->CalendarioFeriados_model->GuardarFeriado($nombre, $desde, $hasta);
        
        $this->GetFeriados();
    }
}