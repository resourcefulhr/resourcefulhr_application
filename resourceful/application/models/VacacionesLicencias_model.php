<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VacacionesLicencias_model extends CI_Model
{
    public function GetEventos($idLegajo)
    {
        return $this->db->select("*")
                        ->where("id_legajo", $idLegajo)
                        ->get("vacaciones_licencias")
                        ->result();
    }

    public function GetEventosConFecha($idLegajo, $desde, $hasta)
    {
        return $this->db->select("*")
                        ->where("id_legajo", $idLegajo)
                        ->where("desde >= '$desde'")
                        ->where("hasta <= '$hasta'")
                        ->get("vacaciones_licencias")
                        ->row();
    }

    public function EliminarEvento($id) {
        return $this->db->delete('vacaciones_licencias', array('id' => $id));
    }

    public function EditarEvento($id, $tipo, $desde, $hasta)
    {
        $info = array(
            "tipo" => $tipo,
            "desde" => DateTime::createFromFormat('Y-m-d', $desde)->format('Y-m-d'),
            "hasta" => DateTime::createFromFormat('Y-m-d', $hasta)->format('Y-m-d')
        );
        $this->db->where("vacaciones_licencias.id", $id);
        return $this->db->update("vacaciones_licencias", $info);
    }

    public function GuardarEvento($idLegajo, $tipo, $desde, $hasta)
    {
        $info = array(
            "tipo" => $tipo,
            "id_legajo" => $idLegajo,
            "desde" => DateTime::createFromFormat('Y-m-d', $desde)->format('Y-m-d'),
            "hasta" => DateTime::createFromFormat('Y-m-d', $hasta)->format('Y-m-d')
        );
        return $this->db->insert('vacaciones_licencias', $info);
    }
}