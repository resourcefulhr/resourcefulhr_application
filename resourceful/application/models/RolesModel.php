<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RolesModel extends CI_Model
{

    public function GetRoles()
    {
        $this->db->where("r.estado", "1");
        $resultado = $this->db->get("roles r");
        return $resultado->result();
    }

    public function Update($id, $info)
    {
        $this->db->where("roles.id", $id);
        return $this->db->update("roles", $info);
    }

    public function GetPermisos($id)
    {
        $this->db->select("p.*,s.link as seccion");
        $this->db->from("permisos p");
        $this->db->join("secciones s", "p.id_seccion = s.id");
        $this->db->where("p.id_rol", $id);
        $resultado = $this->db->get();
        return $resultado->result();
    }

    public function GetSecciones()
    {
        $resultado = $this->db->get("secciones");
        return $resultado->result();
    }

    public function SaveRol($info)
    {
        $this->db->insert("roles", $info);
        return $this->db->insert_id();
    }

    public function SavePermisos($info)
    {
        return $this->db->insert("permisos", $info);
    }

    public function GetRol($id)
    {
        $this->db->where("r.id", $id);
        $resultado = $this->db->get("roles r");
        return $resultado->row();
    }

    public function DeletePermisos($id)
    {
        $this->db->where("id_rol", $id);
        $this->db->delete("permisos");
    }
}
