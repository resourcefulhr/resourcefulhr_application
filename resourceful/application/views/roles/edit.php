<section class="content">
    <h1>
        Rol
        <small>Editar</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>seguridad/roles/Update" method="POST">
                <input type="hidden" value="<?php echo $roles->id; ?>" name="idrol">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('rol') == true ? ' error' : '' ?>">
                        <label>Nombre del rol:</label>
                        <input type="text" class="form-control" id="rol" name="rol" value="<?php echo !empty(form_error("rol")) ? set_value("rol") : $roles->nombre; ?>" required>
                    </div>
                    <?php echo form_error("rol", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <div class="form-line focused">
                        <label>Descripcion del rol:</label>
                        <input type="text" class="form-control" id="descripcion" name="descripcion" value="<?php echo !empty(form_error("descripcion")) ? set_value("descripcion") : $roles->descripcion; ?>">
                    </div>
                </div>
                <hr>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr class="bg-blue-grey">
                            <th>Seccion</th>
                            <th class="col-md-2">Ver</th>
                            <th class="col-md-2">Cargar</th>
                            <th class="col-md-2">Modificar</th>
                            <th class="col-md-2">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($secciones)) : ?>
                            <?php foreach ($secciones as $seccion) : ?>
                                <tr>
                                    <td><?php echo $seccion->link; ?></td>
                                    <td>
                                        <div class="switch">
                                            <label>
                                                <input type="checkbox" name="leer<?php echo $seccion->id; ?>" id="leer" value="1" <?php foreach ($permisos as $permiso) : echo $permiso->id_seccion == $seccion->id ? ($permiso->leer == 1 ? "checked" : "") : "";
                                                                                                                                            endforeach; ?>>
                                                <span class="lever switch-col-black">
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="switch">
                                            <label>
                                                <input type="checkbox" name="insertar<?php echo $seccion->id; ?>" id="insertar" value="1" <?php foreach ($permisos as $permiso) : echo $permiso->id_seccion == $seccion->id ? ($permiso->insertar == 1 ? "checked" : "") : "";
                                                                                                                                                    endforeach; ?>>
                                                <span class="lever switch-col-black">
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="switch">
                                            <label>
                                                <input type="checkbox" name="modificar<?php echo $seccion->id; ?>" id="modificar" value="1" <?php foreach ($permisos as $permiso) : echo $permiso->id_seccion == $seccion->id ? ($permiso->modificar == 1 ? "checked" : "") : "";
                                                                                                                                                    endforeach; ?>>
                                                <span class="lever switch-col-black">
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="switch">
                                            <label>
                                                <input type="checkbox" name="baja<?php echo $seccion->id; ?>" id="baja" value="1" <?php foreach ($permisos as $permiso) : echo $permiso->id_seccion == $seccion->id ? ($permiso->baja == 1 ? "checked" : "") : "";
                                                                                                                                            endforeach; ?>>
                                                <span class="lever switch-col-black">
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>seguridad/roles" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</section>