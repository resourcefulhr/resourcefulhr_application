<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info info-container">
            <img src="<?php echo base_url() ?>front/images/aa/1.png" width="50" height="50" alt="User" />
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <h4><b><?php echo $this->session->userdata("nombre") ?></b></h4>
                </div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a onclick="cambiarContraseña()"> Cambiar Contraseña</a></li>
                        <li><a href="<?php echo base_url(); ?>login/logout"> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header" style="color:currentColor">Barra de navegaciòn</li>

                <?php if (preg_grep('/^Seguridad.*/', $this->session->userdata("permisos_leer"))) : ?>
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">lock</i>
                            <span>Seguridad</span>
                        </a>

                        <ul class="ml-menu">
                            <?php if (in_array("Seguridad -> Usuarios", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>seguridad/usuarios"><i class="material-icons">group</i><span>Usuarios</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Seguridad -> Roles", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>seguridad/roles"><i class="material-icons">extension</i><span>Roles</span></a></li>
                            <?php endif; ?>
                            <?php if (preg_grep('/^Seguridad.*/', $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>dashboard/generateBackupBD">
                                        <i class="material-icons">backup</i><span>Backup de base de datos</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (preg_grep('/^Aplicacion.*/', $this->session->userdata("permisos_leer"))) : ?>
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">accessibility</i>
                            <span>Aplicacion</span>
                        </a>
                        <ul class="ml-menu">
                            <?php if (in_array("Aplicacion -> Cargos", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>areas/cargos"><i class="material-icons">work</i><span>Cargos</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Aplicacion -> Areas", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>areas/area"><i class="material-icons">loyalty</i><span>Areas</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Aplicacion -> Legajos", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>areas/legajos"><i class="material-icons">folder</i><span>Legajos</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Aplicacion -> Organigrama", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>areas/organigrama"><i class="material-icons">extension</i><span>Organigrama</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (preg_grep('/^Control.*/', $this->session->userdata("permisos_leer"))) : ?>
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">alarm_add</i><span>Control</span></a>
                        <ul class="ml-menu">
                            <?php if (in_array("Control -> Novedades", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>control/Novedades"><i class="material-icons">bookmarks</i><span>Novedades</span></a></li>
                            <?php endif; ?>

                            <?php if(in_array("Control -> Novedades", $this->session->userdata("permisos_leer"))): ?>
                            <li><a href="<?php echo base_url(); ?>control/ReportesEstadisticos"><i class="material-icons">gavel</i><span>Informes Estadisticos</span></a></li>
                            <?php endif; ?>

                            <?php if(in_array("Control -> Registrar Ingreso y Egreso", $this->session->userdata("permisos_leer"))): ?>
                            <li><a href="<?php echo base_url(); ?>control/IngresoEgreso"><i class="material-icons">build</i><span>Registrar Ingreso/Egreso</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Control -> Calendario de Feriados", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>control/CalendarioFeriados"><i class="material-icons">calendar_today</i><span>Calendario de Feriados</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Control -> Franjas Horarias", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>control/FranjasHorarias"><i class="material-icons">alarm_add</i><span>Franjas Horarias</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (preg_grep('/^Reclutamiento.*/', $this->session->userdata("permisos_leer"))) : ?>
                    <li class="treeview">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">book</i>
                            <span>Reclutamiento</span>
                        </a>
                        <ul class="ml-menu">
                            <?php if (in_array("Reclutamiento -> Candidatos", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>reclutamiento/candidatos"><i class="material-icons">account_circle</i><span>Candidatos</span></a></li>
                            <?php endif; ?>

                            <?php if (in_array("Reclutamiento -> Aptitudes", $this->session->userdata("permisos_leer"))) : ?>
                                <li><a href="<?php echo base_url(); ?>reclutamiento/aptitudes"><i class="material-icons">grade</i><span>Aptitudes</span></a></li>
                                <li><a href="<?php echo base_url(); ?>reclutamiento/busquedas/buscar"><i class="material-icons">search</i><span>Busquedas</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>


            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
<div class="modal fade" id="modal-contraseña" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <section>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <div class="bg-light-blue">
                                <img src="http://localhost/resourcefulhr_application/resourceful/front/images/aa/1.png" alt=""><br>
                                <b>Alquileres Argentia S.A</b><br>
                                <b>Cambiar Contraseña</b><br>
                            </div>
                        </div>
                    </div>
                    <hr>
                </section>
                <section>
                    <h1 class="text-center"><b>Cambiar Contraseña</b></h1>
                    <form id="cambiar-contraseña" action="<?php echo base_url(); ?>login/CambiarPass" method="POST">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line focused">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Nueva Contraseña" autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line focused">
                                <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirmar Nueva Contraseña" autofocus>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
            <div class="modal-footer">
                <button type="submit" form="cambiar-contraseña" class="btn btn-lg btn-success btn-assign pull-left">Guardar</button>
                <button type="button" class="btn btn-lg btn-danger pull-right" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>front/js/validation-rules/change-password.js"></script>

<script>
    function cambiarContraseña() {
        $("#modal-contraseña").modal("show");
    }
</script>