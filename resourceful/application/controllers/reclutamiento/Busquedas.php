<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Busquedas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Busquedas_model");
        $this->load->model("Cargos_model");
        $this->load->model("Aptitudes_model");
    }
    public function buscar()
    {

        $data = array(
            'cargos' => $this->Cargos_model->GetCargos()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('busquedas/list', $data);
        $this->load->view('layouts/footer');
    }

    public function busquedaCargo()
    {
        $id = $_GET['id'];
        $TipoReclutamiento = $_GET['tipoReclutamiento'];
        $busqueda = "";
        $suma = "";
        $Ex = $this->Busquedas_model->GetAptitudesEXcl($id);
        $cant = sizeof($Ex);
        $To = $this->Busquedas_model->GetAptitudesOp($id);
        $cant2 = sizeof($To) * 3;
        if ($TipoReclutamiento == '1') {
            if ($Ex) {
                //busqyedaa con los dos
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                foreach ($Ex as $E) {
                    $busqueda .= "(`id_aptitud`=$E->id_aptitud AND `nivel`>=$E->nivel)";

                    if (next($Ex)) {
                        $busqueda .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaLegajos($busqueda, $suma, $cant, $cant2);
            } else {
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaLegajosOpci($suma, $cant2);
            }
        } else if ($TipoReclutamiento == '2') {
            if ($Ex) {
                //busqyedaa con los dos
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                foreach ($Ex as $E) {
                    $busqueda .= "(`id_aptitud`=$E->id_aptitud AND `nivel`>=$E->nivel)";

                    if (next($Ex)) {
                        $busqueda .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaCurriculums($busqueda, $suma, $cant, $cant2);
            } else {
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaCurriculumsOpci($suma, $cant2);
            }
        }
        $estadisticas = array(
            'tipo_busqueda' => 'Por Cargo',
            'tipo_reclutamiento' => $TipoReclutamiento == 1 ? "Interno" : "Externo",
            'fecha' => DATE('Y-m-d')
        );
        $this->Busquedas_model->saveEstadisticasBusqueda($estadisticas);
        $ultimoIdGenerado = $this->Busquedas_model->LastID($estadisticas);
        $Tos = $this->Busquedas_model->GetAptitudesTodo($id);
        foreach ($Tos as $T) {
            $detalles  = array(
                'id_generacion_estadisticas_busqueda' => $ultimoIdGenerado,
                'aptitud' => $T->id_aptitud,
                'nivel' => $T->nivel,
                'prioridad' => $T->excluyente
            );
            $this->Busquedas_model->InsertDetallesBusqueda($detalles);
        }
        echo json_encode($buscados);
    }
    ////////////////////////////////////////////////Busqueda Manual/////////////////////////////////////////////////////////////////
    public function buscar2()
    {
        $data = array(
            'aptitudes' => $this->Aptitudes_model->GetAptitudes()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('busquedas/list2', $data);
        $this->load->view('layouts/footer');
    }

    public function Store()
    {
        $tipoBusqueda = $this->input->POST("tipoReclutamiento");
        $idAptitud = $this->input->POST("aptitud_car");
        $nivelAptitud = $this->input->POST("nivel_car");
        $excluyente = $this->input->POST("excluyente_car");

        // agreglo que sirve para guardar informacion necesaria para el modulo de estadisticas
        $estadisticas = array(
            'tipo_busqueda' => 'Manual',
            'tipo_reclutamiento' => $tipoBusqueda == 1 ? "Interno" : "Externo",
            'fecha' => DATE('Y-m-d')
        );

        $this->Busquedas_model->saveEstadisticasBusqueda($estadisticas);
        $ultimoIdGenerado = $this->Busquedas_model->LastID($estadisticas);

        for ($i = 0; $i < count($idAptitud); $i++) {
            $data  = array(
                'id_aptitud' => $idAptitud[$i],
                'id_busqueda_manual' => 19,
                'nivel' => $nivelAptitud[$i],
                'excluyente' => $excluyente[$i],
            );
            $this->Aptitudes_model->saveAptitudManual($data);
        }
        for ($i = 0; $i < count($idAptitud); $i++) {
            $detalles  = array(
                'id_generacion_estadisticas_busqueda' => $ultimoIdGenerado,
                'aptitud' => $idAptitud[$i],
                'nivel' => $nivelAptitud[$i],
                'prioridad' => $excluyente[$i],
            );
            $this->Busquedas_model->InsertDetallesBusqueda($detalles);
        }
        $this->resulta2($tipoBusqueda);
    }
    public function resulta2($busqueda)
    {
        $data = array(
            'aptitudes' => $this->Aptitudes_model->GetAptitudes(),
            'tipoReclutamiento' => $busqueda
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('busquedas/list3', $data);
        $this->load->view('layouts/footer');
    }
    public function BusquedaManual()
    {
        $id = $_GET['id'];
        $TipoReclutamiento = $_GET['tipoReclutamiento'];
        $busqueda = "";
        $suma = "";
        $Ex = $this->Busquedas_model->GetAptitudesEXclBusquedaManual($id);
        $cant = sizeof($Ex);
        $To = $this->Busquedas_model->GetAptitudesOpBusquedaManual($id);
        $cant2 = sizeof($To) * 3;
        if ($TipoReclutamiento == '1') {
            if ($Ex) {
                //busqyedaa con los dos
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                foreach ($Ex as $E) {
                    $busqueda .= "(`id_aptitud`=$E->id_aptitud AND `nivel`>=$E->nivel)";

                    if (next($Ex)) {
                        $busqueda .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaLegajos($busqueda, $suma, $cant, $cant2);
            } else {
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaLegajosOpci($suma, $cant2);
            }
        } else if ($TipoReclutamiento == '2') {
            if ($Ex) {
                //busqyedaa con los dos
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                foreach ($Ex as $E) {
                    $busqueda .= "(`id_aptitud`=$E->id_aptitud AND `nivel`>=$E->nivel)";

                    if (next($Ex)) {
                        $busqueda .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaCurriculums($busqueda, $suma, $cant, $cant2);
            } else {
                foreach ($To as $t) {
                    $suma .= "(`id_aptitud`=$t->id_aptitud AND `nivel`>=$t->nivel)";

                    if (next($To)) {
                        $suma .= " OR ";
                    }
                }
                $buscados = $this->Busquedas_model->GetbusquedaCurriculumsOpci($suma, $cant2);
            }
        }
        echo json_encode($buscados);
    }
    public function consultarCargo()
    {
        $id = $_GET['id'];
        $data = array(
            "aptitudescs" => $this->Aptitudes_model->GetAptitudesCargo($id),
            "cargoID" => $_GET['id']
        );
        $this->load->view('busquedas/modalBody', $data);
    }

    public function clearTableTemp()
    {
        $this->Busquedas_model->clearTableTemp();
    }
    /*******************************************************  Estadisticas de busqueda ************************************************/
    public function estadisticasBusqueda()
    {

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('busquedas/estadisticasBusqueda');
        $this->load->view('layouts/footer');
    }

    public function calcularEstadisticasReclutamiento()
    {
        $data = array(
            'promedioLegajos' => $this->Busquedas_model->GetPromedioAptitudesLegajos(),
            'promedioCandi' => $this->Busquedas_model->GetPromedioAptitudesCandidatos(),
        );
        echo json_encode($data);
    }
    public function GetTop10ApMasBuscadas()
    {
        $data = $this->Busquedas_model->GetTop10ApMasBuscadas();
        echo json_encode($data);
    }
    public function cantidadDeBusquedasRealizadas()
    {
        $data = $this->Busquedas_model->cantidadDeBusquedasRealizadas();
        echo json_encode($data);
    }
    public function GetCandSexo()
    {
        $data = $this->Busquedas_model->GetCandidatosPorSexo();
        echo json_encode($data);
    }
    public function busquedasPorFecha()
    {
        $data = $this->Busquedas_model->busquedasPorFecha();
        echo json_encode($data);
    }
    public function divisionDeBusquedas()
    {
        $data = $this->Busquedas_model->divisionDeBusquedas();
        echo json_encode($data);
    }
}
