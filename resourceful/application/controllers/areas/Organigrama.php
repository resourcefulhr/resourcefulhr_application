<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Organigrama extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Organigrama_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'areas' => json_encode($this->Organigrama_model->GetAreas())
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('organigrama/orgchart', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function GetLegajosPorArea()
    {
        $idArea = $this->input->POST("idArea");
        echo json_encode($this->Organigrama_model->GetLegajosPorArea($idArea));
    }

    public function GetLegajo()
    {
        $numLegajo = $this->input->POST("numLegajo");
        echo json_encode($this->Organigrama_model->GetLegajo($numLegajo));
    }
}
