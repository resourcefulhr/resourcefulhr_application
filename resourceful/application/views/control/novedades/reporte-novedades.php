<?php $legajo = null; ?>
<div class="modal-body">
    <section>
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="bg-light-blue">
                    <img src="../front/images/aa/1.png" alt="">
                    <br>
                    <b>Alquileres Argentia S.A</b><br>
                    <b>Reporte de Novedades</b><br>
                    <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
                    <div class="row text-center">
                        <b>Filtro:</b><br>
                        <div class="col-xs-3">
                            Estado: <?php echo (isset($estado) && sizeOf($estado) != 3) ? implode("<br>", $estado) : "Todas"; ?>
                        </div>
                        <div class="col-xs-3">
                            Tipo: <?php echo (isset($tipo) && sizeOf($tipo) != 3) ? implode("<br>", $tipo) : "Todas"; ?>
                        </div>
                        <div class="col-xs-3">
                            Desde: <?php echo isset($desde) ? $desde : "Siempre"; ?>
                        </div>
                        <div class="col-xs-3">
                            Hasta:<?php echo isset($hasta) ? $hasta : "Siempre"; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <hr>
    <section>
    <?php foreach($info as $persona): ?>
        <div class="row text-center">
            <h3><b><?php echo "Legajo N°: " . $persona->legajo->num_legajo; ?></b></h3>
            <div class="col-xs-4">
                <label class="input-group ">Nombre:
                    <br>
                    <p><?php echo $persona->legajo->nombre; ?></p>
                </label>
                <label class="input-group ">Area:
                    <br>
                    <p><?php echo $persona->legajo->nombre_area; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">Apellido:
                    <br>
                    <p><?php echo $persona->legajo->apellido; ?></p>
                </label>
                <label class="input-group ">Cargo:
                    <br>
                    <p><?php echo $persona->legajo->cargo; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">DNI:
                    <br>
                    <p><?php echo $persona->legajo->numero_documento; ?></p>
                </label>
                <label class="input-group ">Carga Horaria:
                    <br>
                    <p><?php echo $persona->legajo->carga_horaria; ?></p>
                </label>
            </div>
        </div>

        <h5 class="text-center"><b>Vacaciones y Licencias</b></h5>
        <table class="table table-bordered table-striped table-hover">
            <thead class="bg-blue-grey">
                <tr>
                    <th>Tipo</th>
                    <th>Desde</th>
                    <th>Hasta</th>
                </tr>
            </thead>
            <tbody>
                <?php if(empty($persona->vacaciones_licencias)): ?>
                <tr>
                    <td colspan="3" class="text-center"><b>No existen datos para el filtro seleccionado</b></td>
                </tr>
                <?php endif; ?>
                <?php foreach($persona->vacaciones_licencias as $evento): ?>
                <tr>
                    <td><?php echo $evento->tipo ?></td>
                    <td><?php echo $evento->desde ?></td>
                    <td><?php echo $evento->hasta ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <h5 class="text-center"><b>Novedades</b></h5>
        <table class="table table-bordered table-striped table-hover">
            <thead class="bg-blue-grey">
                <tr>
                    <th>Dia</th>
                    <th>Tipo</th>
                    <th>Estado</th>
                    <th>Ajuste</th>
                </tr>
            </thead>
            <tbody>
                <?php $ajuste_acum = 0; ?>
                <?php foreach($persona->novedades as $novedad): ?>
                <?php
                    $ajuste = 0;
                    $ingreso_teorico_datetime = DateTime::createFromFormat("H:i:s", $novedad->ingreso_teorico);
                    $egreso_teorico_datetime = DateTime::createFromFormat("H:i:s", $novedad->egreso_teorico);
                    if ($novedad->descuenta_dia == 1) {
                        $ajuste = ($ingreso_teorico_datetime->GetTimestamp() - $egreso_teorico_datetime->GetTimestamp()) / 3600;
                    } else if ($novedad->ingreso_ajustado != NULL && $novedad->egreso_ajustado != NULL) {
                        $ingreso_ajustado_datetime = DateTime::createFromFormat("H:i:s", $novedad->ingreso_ajustado);
                        $egreso_ajustado_datetime = DateTime::createFromFormat("H:i:s", $novedad->egreso_ajustado);
                        $hrs_teoricas = ($egreso_teorico_datetime->GetTimestamp() - $ingreso_teorico_datetime->GetTimestamp());
                        $hrs_ajustadas = ($egreso_ajustado_datetime->GetTimestamp() - $ingreso_ajustado_datetime->GetTimestamp());
                        $ajuste = ($hrs_ajustadas - $hrs_teoricas) / 3600;
                    }
                    $ajuste_acum += $ajuste;
                ?>
                <tr>
                    <td><?php echo $novedad->dia ?></td>
                    <td><?php echo $novedad->tipo ?></td>
                    <td><?php echo $novedad->estado ?></td>
                    <td class="<?php echo $ajuste < 0 ? "text-danger" : "text-success"; ?>"><?php echo number_format($ajuste, 2) . "hrs"; ?></td>
                </tr>
                <?php endforeach; ?>

                <?php if(empty($persona->novedades)): ?>
                <tr>
                    <td colspan="4" class="text-center"><b>No existen datos para el filtro seleccionado</b></td>
                </tr>
                <?php else: ?>
                <tr>
                    <td colspan="2"></td>
                    <td>AJUSTE ACUMULADO:</td>
                    <td class="<?php echo $ajuste_acum < 0 ? "text-danger" : "text-success"; ?>"><?php echo number_format($ajuste_acum, 2) . "hrs"; ?></td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <hr>
    <?php endforeach; ?>
    </section>
</div>
<div class="modal-footer">
    <button type="submit" onclick="imprimir()" class="btn btn-lg btn-success btn-assign pull-left">Imprimir</button>
    <button type="button" class="btn btn-lg btn-danger pull-right" data-dismiss="modal">Cancelar</button>
</div>

<script>
function imprimir() {
    $("#modal-novedades .modal-body").print({});
    $("#modal-novedades").modal("hide");
}
</script>