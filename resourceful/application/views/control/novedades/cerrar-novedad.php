<div class="modal-body">
    <section>
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="bg-light-blue">
                    <img src="../front/images/aa/1.png" alt="">
                    <br>
                    <b>Alquileres Argentia S.A</b><br>
                    <b>Cierre de Novedad</b><br>
                    <b>Fecha de emision : <?php echo date('y-m-d'); ?></b><br>
                    <br>
                </div>
            </div>
        </div>
        <hr>
    </section>
    <section>
        <!-- COMIENZO INFORMACION DE NOVEDAD -->
        <div class="row text-center">
            <h3><b>Informacion</b></h3>
            <div class="col-xs-4">
                <label class="input-group ">Numero de Legajo:
                    <br>
                    <p><?php echo $novedad->num_legajo; ?></p>
                </label>
                <label class="input-group ">DNI:
                    <br>
                    <p><?php echo $novedad->numero_documento; ?></p>
                </label>
                <label class="input-group ">Tipo de Novedad:
                    <br>
                    <p><?php echo $novedad->tipo; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">Nombre:
                    <br>
                    <p><?php echo $novedad->nombre; ?></p>
                </label>
                <label class="input-group ">Area:
                    <br>
                    <p><?php echo $novedad->nombre_area; ?></p>
                </label>
                <label class="input-group ">Estado:
                    <br>
                    <p><?php echo $novedad->estado; ?></p>
                </label>
            </div>
            <div class="col-xs-4">
                <label class="input-group ">Apellido:
                    <br>
                    <p><?php echo $novedad->apellido; ?></p>
                </label>
                <label class="input-group ">Cargo:
                    <br>
                    <p><?php echo $novedad->cargo; ?></p>
                </label>
                <label class="input-group ">Dia:
                    <br>
                    <p><?php echo $novedad->dia; ?></p>
                </label>
            </div>
        </div>
        <!-- FIN INFORMACION DE NOVEDAD -->
        <hr>
        <!-- COMIENZO HORARIOS -->
        <div class="row text-center">
            <div class="col-xs-6 text-center" style="border-right: 1px solid #eee;">
                <h3><b>Horario Teorico</b></h3>
                <div class="row">
                    <div class="col-xs-6">
                        <label for="ingreso_teorico">Ingreso
                            <br>
                            <p id="ingreso_teorico"><?php echo $novedad->ingreso_teorico; ?></p>
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <label for="egreso_teorico">Egreso
                            <br>
                            <p id="egreso_teorico"><?php echo $novedad->egreso_teorico; ?></p>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 text-center">
                <h3><b>Horario Real</b></h3>
                <div class="row">
                    <div class="col-xs-6">
                        <label for="ingreso_real">Ingreso
                            <br>
                            <?php foreach($registros as $registro): ?>
                            <?php if ($registro->ingreso != NULL): ?>
                            <p><?php echo $registro->ingreso; ?></p>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </label>
                    </div>
                    <div class="col-xs-6">
                        <label for="egreso_real">Egreso
                            <br>
                            <?php foreach($registros as $registro): ?>
                            <?php if ($registro->egreso != NULL): ?>
                            <p><?php echo $registro->egreso; ?></p>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN HORARIOS -->
        <hr>
        <!-- INICIO DE RESOLUCION -->
        <div class="row">
            <h3 class="text-center"><b>Resolucion</b></h3>
            <div class="col-xs-6">
                <label for="resolucion">Resolucion: </label>
                <select id="resolucion" class="form-control">
                    <option value="JUSTIFICADA">JUSTIFICADA</option>
                    <option value="NO JUSTIFICADA">NO JUSTIFICADA</option>
                </select>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <label for="ajuste">Ajuste: </label>
                    <select id="ajuste" class="form-control">
                        <option>Seleccione...</option>
                        <?php if($novedad->tipo == "HORARIO INCONSISTENTE"): ?>
                        <option>COMPUTAR HORARIO REAL</option>
                        <?php endif; ?>
                        <option>COMPUTAR HORARIO TEORICO</option>
                        <option>INGRESAR HORARIO AJUSTADO</option>
                        <option>DESCONTAR DIA</option>
                    </select>
                </div>
                <div class="row" id="ajuste-horario" style="display: none;">
                    <div class="col-xs-6 text-center">
                        <label>Ingreso</label>
                        <input id="ingreso-ajutado" type="time" class="form-control"></input>
                    </div>
                    <div class="col-xs-6 text-center">
                        <label>Egreso</label>
                        <input id="egreso-ajutado" type="time" class="form-control"></input>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN DE RESOLUCION -->
    </section>
</div>

<hr>

<!-- COMIENZO HISTORIAL DE FALTAS -->
<a class="btn btn-block btn-primary text-center" data-toggle="collapse" href="#collapsediv" role="button" aria-expanded="false" aria-controls="collapsediv"><h5>Historial de Novedades</h5></a>
<div class="collapse" id="collapsediv">
<table class="table table-bordered table-striped table-hover" id="dataTable">
    <thead class="bg-blue-grey">
        <tr>
            <th>Dia</th>
            <th>Tipo</th>
            <th>Estado</th>
        </tr>
    <thead>
    <tbody>
        <?php foreach($historial as $nov): ?>
        <tr>
            <td class="<?php echo GetTextColor($nov->estado); ?>"><?php echo $nov->dia; ?></td>
            <td class="<?php echo GetTextColor($nov->estado); ?>"><?php echo $nov->tipo; ?></td>
            <td class="<?php echo GetTextColor($nov->estado); ?>"><?php echo $nov->estado; ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
<!-- FIN HISTORIAL DE FALTAS -->

<hr>

<div class="modal-footer">
    <button type="submit" onclick="guardar()" class="btn btn-lg btn-success btn-assign pull-left">Guardar</button>
    <button type="button" class="btn btn-lg btn-danger pull-right" data-dismiss="modal">Cancelar</button>
</div>


<script>
$(function() {
    $("#ajuste").change(function() {
        let value = $("#ajuste").val();
        switch (value) {
            case "COMPUTAR HORARIO REAL":
                $("#ingreso-ajutado").val(`<?php echo ($novedad->tipo == "HORARIO INCONSISTENTE") ? $registros[0]->ingreso : ""; ?>`);
                $("#egreso-ajutado").val(`<?php echo ($novedad->tipo == "HORARIO INCONSISTENTE") ? $registros[1]->egreso : ""; ?>`);
                $("#ajuste-horario").hide();
                break;
            case "COMPUTAR HORARIO TEORICO":
                $("#ingreso-ajutado").val(`<?php echo $novedad->ingreso_teorico; ?>`);
                $("#egreso-ajutado").val(`<?php echo $novedad->egreso_teorico; ?>`);
                $("#ajuste-horario").hide();
                break;
            case "INGRESAR HORARIO AJUSTADO":
                $("#ingreso-ajutado").val(`<?php echo $novedad->ingreso_teorico; ?>`);
                $("#egreso-ajutado").val(`<?php echo $novedad->egreso_teorico; ?>`);
                $("#ajuste-horario").show();
                break;
            case "DESCONTAR DIA":
                $("#ingreso-ajutado").val("");
                $("#egreso-ajutado").val("");
                $("#ajuste-horario").hide();
                break;
        }
    });

    $('#dataTable').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por pagina",
                "zeroRecords": "No se encontraron resultados en su busqueda",
                "searchPlaceholder": "Buscar registros",
                "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
                "infoEmpty": "No existen registros",
                "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                "search": "Buscar:",
                "paginate": {
                    "first": "Primero",
                    "last": "Último",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
    });
    $('#dataTable_filter').addClass('pull-right');
});

function guardar() {
    if ($("#ajuste").val() == "Seleccione...") {
        swal("Oops", "Debe ingresar una resolucion para cerrar la novedad!", "error");
        return;
    }

    let base_url = "<?php echo base_url(); ?>";
    $.ajax({
        url: base_url + "control/Novedades/CerrarNovedad",
        data: {
            id: <?php echo $novedad->id; ?>,
            resolucion: $("#resolucion").val(),
            ingreso_ajustado: $("#ingreso-ajutado").val(),
            egreso_ajustado: $("#egreso-ajutado").val()
        },
        type: "POST",
    }).done(function(data) {
        $("#modal-novedades .modal-body").print({});
        $("#modal-novedades").modal("hide");
    }).error(function(data) {
        swal("Oops", "Error", "error");
    });
}
</script>

<?php
function GetTextColor($estado)
{
    switch($estado) {
        case "ABIERTA":
            return "text-info";
        case "JUSTIFICADA":
            return "text-success";
        case "NO JUSTIFICADA":
            return "text-danger";
    }
}