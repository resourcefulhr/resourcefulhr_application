<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Novedades_model extends CI_Model
{
    public function GetNovedad($id)
    {
        $this->db->select("n.*, l.nombre, l.apellido, l.numero_documento, l.numero_documento, a.nombre_area, c.cargo")
        ->from("novedades n")
        ->where("n.id", $id)
        ->join("legajos l", "n.num_legajo = l.num_legajo")
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id");

        return $this->db->get()->row();
    }

    public function CerrarNovedad($id, $estado, $ingreso_ajustado, $egreso_ajustado)
    {
        $descuenta_dia = 0;
        if ($ingreso_ajustado == "" && $egreso_ajustado == "") {
            $ingreso_ajustado = NULL;
            $egreso_ajustado = NULL;
            $descuenta_dia = 1;
        }

        $info = array(
            "estado" => $estado,
            "ingreso_ajustado" => $ingreso_ajustado,
            "egreso_ajustado" => $egreso_ajustado,
            "descuenta_dia" => $descuenta_dia
        );

        $this->db->where("id", $id)
                ->update("novedades", $info);
    }

    public function GetRegistros($idNovedad)
    {
        $this->db->select("*")
        ->from("novedades_registros")
        ->where("id_novedad", $idNovedad);

        return $this->db->get()->result();
    }

    public function GetNovedades($tipos, $estados, $legajos, $desde, $hasta)
    {
        $this->db->select("n.*, l.nombre, l.apellido, a.nombre_area, c.cargo")
        ->from("novedades n")
        ->join("legajos l", "n.num_legajo = l.num_legajo")
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id");

        if (isset($tipos)) {
            $tipos_string = "'" . implode("','", $tipos) . "'";
            $this->db->where("n.tipo IN($tipos_string)");
        }

        if (isset($estados)) {
            $estados_string = "'" . implode("','", $estados) . "'";
            $this->db->where("n.estado IN($estados_string)");
        }

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("n.num_legajo IN($legajos_string)");
        }

        if (isset($desde) && isset($hasta)) {
            $this->db->where("dia >= CAST('$desde' as DATE)");
            $this->db->where("dia <= CAST('$hasta' as DATE)");
        }

        $this->db->order_by("dia", 'DESC');

        return $this->db->get()->result();
    }

    public function GetDatosReporte($tipos, $estados, $legajos, $desde, $hasta)
    {
        $this->db->select("l.num_legajo, l.nombre, l.apellido, l.numero_documento, a.nombre_area, l.carga_horaria , c.cargo")
        ->from("legajos l")
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id");;

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("l.num_legajo IN($legajos_string)");
        }

        $legajos_array = $this->db->get()->result();

        $info = array();

        foreach ($legajos_array as $legajo) {
            //Novedades
            $this->db->select("n.dia, n.tipo, n.estado, n.ingreso_teorico, n.egreso_teorico, n.ingreso_ajustado, n.egreso_ajustado, n.descuenta_dia")
            ->from("novedades n")
            ->where("n.num_legajo", $legajo->num_legajo);

            if (isset($tipos)) {
                $tipos_string = "'" . implode("','", $tipos) . "'";
                $this->db->where("n.tipo IN($tipos_string)");
            }

            if (isset($estados)) {
                $estados_string = "'" . implode("','", $estados) . "'";
                $this->db->where("n.estado IN($estados_string)");
            }

            if (isset($desde) && isset($hasta)) {
                $this->db->where("dia >= CAST($desde as DATE)");
                $this->db->where("dia <= CAST($hasta as DATE)");
            }

            $novedades = $this->db->get()->result();

            $this->db->select("tipo, desde, hasta")
            ->from("vacaciones_licencias")
            ->where("id_legajo", $legajo->num_legajo);

            if (isset($desde) && isset($hasta)) {
                $this->db->where("desde >= CAST($desde as DATE)");
                $this->db->where("hasta <= CAST($hasta as DATE)");
            }

            $vacaciones_licencias = $this->db->get()->result();

            array_push($info, (object)["legajo" => $legajo, "novedades" => $novedades, "vacaciones_licencias" => $vacaciones_licencias]);
        }
        return $info;
    }

    public function GetNovedadesAlmuerzo($tipos, $estados, $legajos, $desde, $hasta)
    {
        $this->db->select("n.*, l.nombre, l.apellido, a.nombre_area, c.cargo")
        ->from("novedades_almuerzo n")
        ->join("legajos l", "n.num_legajo = l.num_legajo")
        ->join("cargos c", "l.id_cargo = c.id")
        ->join("areas a", "c.id_area = a.id");

        if (isset($tipos)) {
            $tipos_string = "'" . implode("','", $tipos) . "'";
            $this->db->where("n.tipo IN($tipos_string)");
        }

        if (isset($estados)) {
            $estados_string = "'" . implode("','", $estados) . "'";
            $this->db->where("n.estado IN($estados_string)");
        }

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("n.num_legajo IN($legajos_string)");
        }

        if (isset($desde) && isset($hasta)) {
            $this->db->where("dia >= CAST($desde as DATE)");
            $this->db->where("dia <= CAST($hasta as DATE)");
        }

        $this->db->order_by("dia", 'ASC');
        return $this->db->get()->result();
    }

    public function CountNovedadesAbiertas()
    {
        return $this->db->select("COUNT(id)")
                ->where("estado", "ABIERTA")
                ->from("novedades")
                ->get()
                ->row();
    }

    public function CountNovedadesAlmuerzoAbiertas()
    {
        return $this->db->select("COUNT(id)")
                ->where("estado", "ABIERTA")
                ->from("novedades_almuerzo")
                ->get()
                ->row();
    }

    public function GetLegajos()
    {
        return $this->db->select("num_legajo, fecha_ingreso, fecha_egreso, id_franja_horaria, nombre, apellido")
        ->from("legajos")
        ->where("estado = 1")
        ->get()
        ->result();
    }

    public function GetLegajosConNumero($legajos)
    {
        $this->db->select("num_legajo, fecha_ingreso, fecha_egreso, id_franja_horaria, nombre, apellido")
        ->from("legajos")
        ->where("estado = 1");

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("num_legajo IN($legajos_string)");
        }

        return $this->db->get()
        ->result();
    }

    public function GetUltimaGeneracion()
    {
        return $this->db->select("MAX(fecha) as fecha")
        ->from("novedades_ultima_generacion")
        ->get()
        ->row();
    }

    public function CleanUltimaGeneracion()
    {
        $ultima_generacion = $this->GetUltimaGeneracion();

        $this->db->truncate("novedades_ultima_generacion");

        $this->ActualizarUltimaGeneration($ultima_generacion->fecha);
    }

    public function IsFeriado($dia)
    {
        $feriado = $this->db->query("SELECT *
        FROM feriados
        WHERE NOT (desde > DATE '$dia' OR hasta < DATE '$dia')")->result();

        return isset($feriado) && !empty($feriado);
    }

    public function IsDiaExcepcion($dia, $legajoId)
    {
        $dia_excepcion = $this->db->query("SELECT *
        FROM vacaciones_licencias
        WHERE NOT (desde > DATE '$dia' OR hasta < DATE '$dia')
        AND id_legajo = '$legajoId'")
        ->result();

        return isset($dia_excepcion) && !empty($dia_excepcion);
    }

    public function GetTolerancia($franja_horaria_id)
    {
        return $this->db->select("tolerancia")
        ->from("franjas_horarias")
        ->where("id", $franja_horaria_id)
        ->get()
        ->row();
    }

    public function GetTiempoAlmuerzo($franja_horaria_id)
    {
        return $this->db->select("tiempo_almuerzo")
        ->from("franjas_horarias")
        ->where("id", $franja_horaria_id)
        ->get()
        ->row();
    }

    public function GetIngreso($dia, $legajo) {
        return $this->db->select("CAST(ingreso as TIME)")
        ->from("registroshorarios")
        ->where("CAST(ingreso as DATE) = DATE '$dia'")
        ->where("id_legajo", $legajo)
        ->get()
        ->result();
    }

    public function GetEgreso($dia, $legajo) {
        return $this->db->select("CAST(egreso as TIME)")
        ->from("registroshorarios")
        ->where("CAST(egreso as DATE) = DATE '$dia'")
        ->where("id_legajo", $legajo)
        ->get()
        ->result();
    }

    public function GetAlmuerzos($dia, $legajo)
    {
        return $this->db->select("CAST(almuerzo as TIME)")
        ->from("registroshorarios")
        ->where("CAST(almuerzo as DATE) = DATE '$dia'")
        ->where("id_legajo", $legajo)
        ->get()
        ->result();
    }

    public function GetIngresoTeorico($dia, $id_franja_horaria) {
        return $this->db->select($dia . "_ingreso")
        ->from("franjas_horarias")
        ->where("id", $id_franja_horaria)
        ->get()
        ->row();
    }

    public function GetEgresoTeorico($dia, $id_franja_horaria) {
        return $this->db->select($dia . "_egreso")
        ->from("franjas_horarias")
        ->where("id", $id_franja_horaria)
        ->get()
        ->row();
    }

    public function GenerarNovedad($num_legajo, $dia, $ingresos, $egresos, $ingreso_teorico, $egreso_teorico, $tolerancia, $tipo)
    {
        $info = array(
            "num_legajo" => $num_legajo,
            "dia" => $dia,
            "ingreso_teorico" => $ingreso_teorico,
            "egreso_teorico" => $egreso_teorico,
            "tolerancia" => $tolerancia,
            "tipo" => $tipo,
            "estado" => "ABIERTA"
        );

        $this->db->insert("novedades", $info);

        $id_novedad = $this->db->insert_id();

        foreach ($ingresos as $ingreso) {
            $info = array(
                "id_novedad" => $id_novedad,
                "ingreso" => current($ingreso)
            );

            $this->db->insert("novedades_registros", $info);
        }

        foreach ($egresos as $egreso) {
            $info = array(
                "id_novedad" => $id_novedad,
                "egreso" => current($egreso)
            );

            $this->db->insert("novedades_registros", $info);
        }
    }

    public function GenerarNovedadAlmuerzo($num_legajo, $dia, $registros_almuerzo, $tiempo_almuerzo, $tipo, $tiempo_almuerzo_tomado = NULL)
    {
        $info = array(
            "num_legajo" => $num_legajo,
            "dia" => $dia,
            "tiempo_almuerzo" => $tiempo_almuerzo,
            "tipo" => $tipo,
            "estado" => "ABIERTA",
            "tiempo_almuerzo_tomado" => $tiempo_almuerzo_tomado
        );

        $this->db->insert("novedades_almuerzo", $info);

        $id_novedad = $this->db->insert_id();

        foreach ($registros_almuerzo as $registro_almuerzo) {
            $info = array(
                "id_novedad_almuerzo" => $id_novedad,
                "hora" => current($registro_almuerzo)
            );

            $this->db->insert("novedades_almuerzo_registros", $info);
        }
    }

    public function ActualizarUltimaGeneration($dia_actualizado)
    {
        $info = array(
            "fecha" => $dia_actualizado
        );

        $this->db->insert("novedades_ultima_generacion", $info);
    }

    public function JustificarNovedadAlmuerzo($id) {
        $info = array(
            "estado" => "JUSTIFICADA"
        );

        $this->db->where("id", $id)
                ->update("novedades_almuerzo", $info);
    }

    public function InustificarNovedadAlmuerzo($id, $ajuste) {
        $info = array(
            "estado" => "NO JUSTIFICADA",
        );

        $this->db->where("id", $id)
                ->update("novedades_almuerzo", $info);
    }

    public function GetDistribucionNovedades($legajos, $desde, $hasta)
    {
        $this->db->select("dia,
                        COUNT(CASE `tipo` WHEN 'INASISTENCIA' THEN 1 ELSE NULL END) as `inasistencias`,
                        COUNT(CASE `tipo` WHEN 'REGISTRO INCONSISTENTE' THEN 1 ELSE NULL END) as `registros_inconsistentes`,
                        COUNT(CASE `tipo` WHEN 'HORARIO INCONSISTENTE' THEN 1 ELSE NULL END) as `horarios_inconsistentes`")
        ->from("novedades");

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("num_legajo IN($legajos_string)");
        }

        $this->db->where("dia >= CAST('$desde' as DATE)")
                ->where("dia <= CAST('$hasta' as DATE)")
                ->group_by("dia");

        return $this->db->get()->result();
    }

    public function GetEstadosPorPeriodo($legajos, $desde, $hasta)
    {
        $this->db->select("COUNT(CASE estado WHEN 'ABIERTA' THEN 1 ELSE NULL END) as abierta,
        COUNT(CASE estado WHEN 'JUSTIFICADA' THEN 1 ELSE NULL END) as justificada,
        COUNT(CASE estado WHEN 'NO JUSTIFICADA' THEN 1 ELSE NULL END) as no_justificada")
        ->from("novedades");

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("num_legajo IN($legajos_string)");
        }

        $this->db->where("dia >= CAST('$desde' as DATE)")
                ->where("dia <= CAST('$hasta' as DATE)");

        return $this->db->get()->row();
    }

    public function IndiceJustificacion($tipo, $legajos, $desde, $hasta)
    {
        $this->db->select("COUNT(CASE estado WHEN 'JUSTIFICADA' THEN 1 ELSE NULL END) as justificadas,
        COUNT(CASE estado WHEN 'NO JUSTIFICADA' THEN 1 ELSE NULL END) as no_justificadas")
        ->from("novedades")
        ->where("tipo", $tipo);

        if (isset($legajos)) {
            $legajos_string = "'" . implode("','", $legajos) . "'";
            $this->db->where("num_legajo IN($legajos_string)");
        }

        $this->db->where("dia >= CAST('$desde' as DATE)")
                ->where("dia <= CAST('$hasta' as DATE)");

        return $this->db->get()->row();
    }
}