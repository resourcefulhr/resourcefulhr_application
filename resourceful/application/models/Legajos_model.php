<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Legajos_model extends CI_Model
{

    public function GetLegajos()
    {

        $this->db->select("l.id,l.num_legajo,l.apellido,l.nombre,l.mail,l.domicilio,l.telefono,c.cargo as cargo");
        $this->db->from("legajos l");
        $this->db->join("cargos c", "l.id_cargo = c.id");
        $this->db->where("l.estado", "1");
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function Update($id, $info)
    {
        $this->db->where("legajos.num_legajo", $id);
        return $this->db->update("legajos", $info);
    }
    public function GetNacionalidades()
    {
        $this->db->select("n.id,n.PAIS_NAC");
        $this->db->from("nacionalidad n");
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function GetProvincias()
    {
        $this->db->select("p.id,p.provincia_nombre");
        $this->db->from("provincia p");
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function GetCiudades($id_provincia)
    {
        $this->db->select("c.id,c.ciudad_nombre");
        $this->db->from("ciudad c");
        $this->db->where("provincia_id", $id_provincia);
        $resultado = $this->db->get();
        return $resultado->result();
    }
    public function Save($info)
    {
        return $this->db->insert('legajos', $info);
    }
    public function save_flia($data)
    {
        return $this->db->insert('grupo_familiar', $data);
    }
    public function GetLegajo($id)
    {
        $this->db->where("l.num_legajo", $id);
        $resultado = $this->db->get("legajos l");
        return $resultado->row();
    }
    /*********************************************Reporte de legajo***************************************************/
    public function GetNacionalid($id)
    {
        return $resultados = $this->db->select("PAIS_NAC")
            ->from("nacionalidad")
            ->where("id", $id)
            ->get()
            ->row();
    }

    public function GetProvincia($id)
    {
        return $resultados = $this->db->select("provincia_nombre")
            ->from("provincia")
            ->where("id", $id)
            ->get()
            ->row();
    }
    public function GetCiudad($id)
    {
        return $resultados = $this->db->select("ciudad_nombre")
            ->from("ciudad")
            ->where("id", $id)
            ->get()
            ->row();
    }
    public function GetCargo($id)
    {
        return $resultados = $this->db->select("cargo")
            ->from("cargos")
            ->where("id", $id)
            ->get()
            ->row();
    }


    /******************************************Grupo familia *********************************************************/
    public function GetFamilia($id)
    {
        $this->db->where("g.id_legajo", $id);
        $resultado = $this->db->get("grupo_familiar g");
        return $resultado->result();
    }
    public function SaveFamiliar($info)
    {
        return $this->db->insert('grupo_familiar', $info);
    }

    public function EditFamiliar($id)
    {
        $this->db->where('g.id', $id);
        $resultado = $this->db->get("grupo_familiar g");
        return $resultado->row();
    }
    public function UpdateFamiliar($idFamiliar, $info)
    {
        $this->db->where("g.id", $idFamiliar);
        return $this->db->update("grupo_familiar g", $info);
    }
    public function DeleteFamiliar($id)
    {
        $this->db->where('grupo_familiar.id', $id);
        $this->db->delete('grupo_familiar');
    }
}
