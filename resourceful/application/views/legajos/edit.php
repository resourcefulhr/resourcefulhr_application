<section class="content">
    <h1>
        Legajo
        <small>Editar</small>
    </h1>
    <form class="form-line" action="<?php echo base_url(); ?>areas/legajos/Update" method="POST">
    <input type="hidden" name="legajo_oculto" class="form-control"  value="<?php echo !empty(set_value("legajo")) ? set_value("legajo") : $legajo->num_legajo; ?>">
        <div class="card">
            <div class="body">
                <div>
                    <h2>INFORMACION PERSONAL</h2>
                    <hr>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Legajo</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused<?php echo form_error('legajo') == true ? ' error' : '' ?>">
                                <input type="number" name="legajo" class="form-control" required value="<?php echo !empty(set_value("legajo")) ? set_value("legajo") : $legajo->num_legajo; ?>">
                            </div>
                            <?php echo form_error("legajo", "<span class='help-block'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Documento</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused<?php echo form_error('numero_documento') == true ? ' error' : '' ?>">
                                <input type="number" name="numero_documento" class="form-control" required value="<?php echo !empty(set_value("numero_documento")) ? set_value("numero_documento") : $legajo->numero_documento; ?>">
                            </div>
                            <?php echo form_error("numero_documento", "<span class='help-block'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Tipo documento</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="tipo_documento" id="tipo_documento" class="form-control" required>
                                <option value="">Seleccione...</option>
                                <option value="dni" <?php echo "dni" == $legajo->tipo_documento ? "selected" : "" ?>>DNI</option>
                                <option value="pasaporte" <?php echo "pasaporte" == $legajo->tipo_documento ? "selected" : "" ?>>Pasaporte</option>
                                <option value="Otro" <?php echo "Otro" == $legajo->tipo_documento ? "selected" : "" ?>>Otro</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-2">
                        <label>Sexo</label>
                        <div class="input-group">
                            <select name="sexo" id="sexo" class="form-control">
                                <option value="Femenino" <?php echo "Femenino" == $legajo->sexo ? "selected" : "" ?>>Femenino</option>
                                <option value="Masculino" <?php echo "Masculino" == $legajo->sexo ? "selected" : "" ?>>Masculino</option>
                                <option value="Otro" <?php echo "Otro" == $legajo->sexo ? "selected" : "" ?>>Otro</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused ">
                                <input type="text" name="nombre" class="form-control" required value="<?php echo !empty(set_value("nombre")) ? set_value("nombre") : $legajo->nombre; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <label>Apellido</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="apellido" class="form-control" required value="<?php echo !empty(set_value("apellido")) ? set_value("apellido") : $legajo->apellido; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Fecha de nacimiento</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line focused">

                                <input type="date" name="fecha_nacimiento" class="form-control" required value="<?php echo !empty(set_value("fecha_nacimiento")) ? set_value("fecha_nacimiento") : $legajo->fecha_nacimiento; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>CUIL</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused<?php echo form_error('cuil') == true ? ' error' : '' ?>">

                                <input type="number" name="cuil" class="form-control" required value="<?php echo !empty(set_value("cuil")) ? set_value("cuil") : $legajo->cuil; ?>">
                            </div>
                            <?php echo form_error("cuil", "<span class='help-block'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Telefono</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">phone</i>
                            </span>
                            <div class="form-line focused">
                                <input type="number" name="telefono" class="form-control" required value="<?php echo !empty(set_value("telefono")) ? set_value("telefono") : $legajo->telefono; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-8">
                        <label>Mail</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="mail" class="form-control email" placeholder="Ex: example@example.com" required value="<?php echo !empty(set_value("mail")) ? set_value("mail") : $legajo->mail; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Estado civil</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="estado_civil" id="estado_civil" class="form-control">
                                <option value="Soltero/a" <?php echo "Soltero/a" == $legajo->estado_civil ? "selected" : "" ?>>Soltero/a</option>
                                <option value="Casado/a" <?php echo "Casado/a" == $legajo->estado_civil ? "selected" : "" ?>>Casado/a</option>
                                <option value="Divorciado/a" <?php echo "Divorciado/a" == $legajo->estado_civil ? "selected" : "" ?>>Divorciado/a</option>
                                <option value="Viudo/a" <?php echo "Viudo/a" == $legajo->estado_civil ? "selected" : "" ?>>Viudo/a</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Nacionalidad</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="nacionalidad" id="nacionalidad" class="form-control">
                                <?php foreach ($nacionalidades as $nacionalidad) : ?>
                                    <?php if ($nacionalidad->id == $legajo->id_nacionalidad) : ?>
                                        <option value="<?php echo $nacionalidad->id ?>" selected><?php echo $nacionalidad->PAIS_NAC; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $nacionalidad->id ?>"><?php echo $nacionalidad->PAIS_NAC; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Provincia</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="provincia" id="provincia" data-live-search="true" class="form-control show-tick">
                                <?php foreach ($provincias as $provincia) : ?>
                                    <?php if ($provincia->id == $legajo->id_provincia) : ?>
                                        <option value="<?php echo $provincia->id ?>" selected><?php echo $provincia->provincia_nombre; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $provincia->id ?>"><?php echo $provincia->provincia_nombre; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Ciudad</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="ciudad" id="ciudad" data-live-search="true" class="form-control show-tick">
                                <?php foreach ($ciudades as $ciudad) : ?>
                                    <?php if ($ciudad->id == $legajo->id_ciudad) : ?>
                                        <option value="<?php echo $ciudad->id ?>" selected><?php echo $ciudad->ciudad_nombre; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $ciudad->id ?>"><?php echo $ciudad->ciudad_nombre; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-8">
                        <label>Domicilio</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="domicilio" class="form-control" required value="<?php echo !empty(set_value("domicilio")) ? set_value("domicilio") : $legajo->domicilio; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div>
                    <h2>INFORMACION DEL EMPLEADO</h2>
                    <hr>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Fecha de ingreso</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line focused">
                                <input type="date" name="fecha_ingreso" class="form-control" required value="<?php echo !empty(set_value("fecha_ingreso")) ? set_value("fecha_ingreso") : $legajo->fecha_ingreso; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Cargo</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <select name="cargo" id="cargo" class="form-control">
                                <?php foreach ($cargos as $cargo) : ?>
                                    <?php if ($cargo->id == $legajo->id_cargo) : ?>
                                        <option value="<?php echo $cargo->id ?>" selected><?php echo $cargo->cargo; ?></option>
                                    <?php else : ?>
                                        <option value="<?php echo $cargo->id ?>"><?php echo $cargo->cargo; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label>Carga Horaria</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="number" name="carga_horaria" class="form-control" required value="<?php echo !empty(set_value("carga_horaria")) ? set_value("carga_horaria") : $legajo->carga_horaria; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <label>Sindicato</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="sindicato" class="form-control" value="<?php echo !empty(set_value("sindicato")) ? set_value("sindicato") : $legajo->sindicato; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Obra social</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="obra_social" class="form-control" required value="<?php echo !empty(set_value("obra_social")) ? set_value("obra_social") : $legajo->obra_social; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>Carnet de conducir</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="carnet_conducir" id="carnet_conducir" value="1" <?php echo !empty(set_value("carnet_conducir")) ?  "checked" : $legajo->carnet_conducir == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Banco</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused">
                                <input type="text" name="banco" class="form-control" required value="<?php echo !empty(set_value("banco")) ? set_value("banco") : $legajo->banco; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label>CBU</label>
                        <div class="input-group ">
                            <span class="input-group-addon">
                                <i class="material-icons">keyboard_arrow_right</i>
                            </span>
                            <div class="form-line focused<?php echo form_error('cbu') == true ? ' error' : '' ?>">
                                <input type="text" name="cbu" class="form-control" required value="<?php echo !empty(set_value("cbu")) ? set_value("cbu") : $legajo->cbu; ?>">
                            </div>
                            <?php echo form_error("cbu", "<span class='help-block'>", "</span>"); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Fecha de egreso</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line focused">
                                <input type="date" name="fecha_egreso" class="form-control" value="<?php echo !empty(set_value("fecha_egreso")) ? set_value("fecha_egreso") : $legajo->fecha_egreso; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="body">
                <div>
                    <h2>DOCUMENTACION</h2>
                    <hr>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Fotocopia del DNI</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="copia_dni" id="copia_dni" value="1" <?php echo  $legajo->copia_dni == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Partida de nacimiento</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="partida_naci" id="partida_naci" value="1" <?php echo !empty(set_value("partida_naci")) ?  set_value("partida_naci") == "1" ?  "checked" : "" : $legajo->partida_nacimiento == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Acta de matrimonio</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="acta_matri" id="acta_matri" value="1" <?php echo set_value("acta_matri") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Sumario de comvivencia</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="sumario" id="sumario" value="1" <?php echo set_value("sumario") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Contancia de CUIL</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="contancia_cuil" id="contancia_cuil" value="1" <?php echo set_value("contancia_cuil") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>f572_rig</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="f572_rig" id="f572_rig" value="1" <?php echo set_value("f572_rig") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>f649_rig</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="f649_rig" id="f649_rig" value="1" <?php echo set_value("f649_rig") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Examen pre-ocupacional</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="examen_pre_ocupacional" id="examen_pre_ocupacional" value="1" <?php echo set_value("examen_pre_ocupacional") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>Contrato</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="contrato" id="contrato" value="1" <?php echo set_value("contrato") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Afiliado al sindicato</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="afi_sindicato" id="afi_sindicato" value="1" <?php echo set_value("afi_sindicato") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Libreta de trabajo</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="librta_trabajo" id="librta_trabajo" value="1" <?php echo set_value("librta_trabajo") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Seguro de vida</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="seguro_vida" id="seguro_vida" value="1" <?php echo set_value("seguro_vida") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-3">
                        <label>PS261</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="ps261" id="ps261" value="1" <?php echo set_value("ps261") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Carnet sanitario</label>
                        <div class="switch">
                            <label>NO<input type="checkbox" name="carnet_sanitario" id="carnet_sanitario" value="1" <?php echo set_value("carnet_sanitario") == "1" ?  "checked" : ""; ?>>
                                <span class="lever switch-col-black"></span>SI
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Fecha del carnet sanitario</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line focused">
                                <input disabled type="date" id="fecha_sanitario" name="fecha_sanitario" class="form-control" required value="<?php echo !empty(set_value("fecha_sanitario")) ? set_value("fecha_sanitario") : $legajo->fecha_sanitario; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
            <a href="<?php echo base_url(); ?>areas/legajos" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                </i> Cancelar</a>
        </div>
    </form>
</section>
<script>
    $(document).ready(function() {
        $("#carnet_sanitario").on("click", function() {
            if ($('#fecha_sanitario').prop('disabled') == false) {
                $('#fecha_sanitario').attr("disabled", true);
                $('#fecha_sanitario').val(null);
            } else {
                $('#fecha_sanitario').attr("disabled", false);
            }
        });

        $("#provincia").change(function() {
            var base_url = "<?php echo base_url(); ?>";
            let id_provincia = $(this).val();
            $.ajax({
                url: base_url + "areas/legajos/GetCiudades/" + id_provincia,
                dataType: "json",
                type: "POST",
                success: function(result) {
                    $("#ciudad").html("");
                    $("#ciudad").append(new Option("Seleccione...", ""));
                    result.forEach(function(element, index, array) {
                        $("#ciudad").append(new Option(element.ciudad_nombre, element.id));
                    });
                    $("#ciudad").selectpicker('refresh');
                }
            });
        });
    });
</script>