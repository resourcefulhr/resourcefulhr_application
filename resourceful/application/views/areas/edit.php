<section class="content">
    <h1>
        Area
        <small>Editar</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>areas/area/Update" method="POST">
                <input type="hidden" value="<?php echo $area->id; ?>" name="idarea">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('area') == true ? ' error' : '' ?>">
                        <label>Nombre del area:</label>
                        <input type="text" name="area" class="form-control" value="<?php echo !empty(form_error("area")) ? set_value("area") : $area->nombre_area; ?>" required>
                    </div>
                    <?php echo form_error("area", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <div class="form-line focused">
                        <label>Descripcion del area:</label>
                        <input type="text" name="descripcion" class="form-control" value="<?php echo !empty(form_error("descripcion")) ? set_value("descripcion") : $area->descripcion; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Area dependiente:</label>
                    <select name="depencdencia" id="depencdencia" class="form-control" required>
                        <?php foreach ($areas as $ares) : ?>
                            <?php if ($ares->id == $area->id_area) : ?>
                                <option value="<?php echo $ares->id ?>" selected><?php echo $ares->nombre_area; ?></option>
                            <?php else : ?>
                                <?php if ($ares->id != $area->id) : ?>
                                    <option value="<?php echo $ares->id ?>"><?php echo $ares->nombre_area; ?></option>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>areas/area" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>

        </div>
    </div>
</section>