<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Usuarios_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                'usuarios' => $this->Usuarios_model->GetUsuarios()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('usuarios/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }

    public function Delete($id)
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $info  = array(
            'estado' => "0"
        );
        $this->Usuarios_model->Update($id, $info);
        echo "seguridad/usuarios";
    }

    public function Edit($id)
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $info = array(
            'usuario' => $this->Usuarios_model->GetUsuario($id),
            'roles' => $this->Usuarios_model->GetRoles()
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('usuarios/edit', $info);
        $this->load->view('layouts/footer');
    }

    public function Update()
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $usuarioID = $this->input->POST("usuarioID");
            $info = array(
                'id_rol' => $this->input->POST("rol")
            );
            if ($this->Usuarios_model->Update($usuarioID, $info)) {
                redirect(base_url() . "seguridad/usuarios");
            } else {
                $this->session->set_flashdata("error", "No se pudo guardar la informacion");
                redirect(base_url() . "seguridad/usuarios/edit");
            }
    }
}
