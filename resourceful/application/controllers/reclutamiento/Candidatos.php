<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Candidatos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // $this->load->model("Legajos_model");
        $this->load->model("Candidatos_model");
    }

    public function index()
    {
        if ($this->session->userdata('login')) {
            $info = array(
                "curriculums" => $this->Candidatos_model->GetCVs()
            );
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('candidatos/list', $info);
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function Add()
    {
        if ($this->session->userdata('login')) {
            $this->load->view('layouts/header');
            $this->load->view('layouts/aside');
            $this->load->view('candidatos/add');
            $this->load->view('layouts/footer');
        } else {
            $this->load->view('login');
        }
    }
    public function StoreCV()
    {
        $guardado = $_FILES['file']['tmp_name'];
        $nombreConEspacios = $this->input->post("nombre");
        $apellidoConEspacios = $this->input->post("apellido");
        //cambio los espacios en blanco por "_" para que asi no me rompa la url de un archivo y evite problemas al buscarlo
        //a travez de su ruta
        $nombre = str_replace(" ", "_", $nombreConEspacios);
        $apellido = str_replace(" ", "_", $apellidoConEspacios);

        //si tengo una url, es porque me cargaron un curriculum
        if ($guardado != "") {
            //si no existe la carpeta, crearla
            if (!file_exists('archivos/curriculums/')) {
                mkdir('archivos/curriculums/', 0777, true);
                //una vez creada, compruebo nuevamente que exista
                if (file_exists('archivos/curriculums/')) {
                    /*subo el archivo, primer parametro => donde esta almacenado=> La variable $guardado contiene una ruta temporal que me proporciona el array $_FILES
                Segundo parametro(Ruta donde se va a guardar y como ultimo tramo el nombre del archivo)*/
                    if (move_uploaded_file($guardado, 'archivos/curriculums/' . $nombre . "_" . $apellido . ".pdf")) {
                        //genero la ruta del registro
                        $rutaCV = 'archivos/curriculums/' . $nombre . "_" . $apellido . ".pdf";
                        $info = array(
                            'nombre' =>  $nombreConEspacios,
                            'apellido' => $apellidoConEspacios,
                            'sexo' => $this->input->post("sexo"),
                            'fecha_nacimiento' => $this->input->post("fecha_nacimiento"),
                            'fecha_cv' => $this->input->post('fecha_cv'),
                            'puesto' => $this->input->post("puesto"),
                            'telefono' => $this->input->post("telefono"),
                            'email' => $this->input->post("email"),
                            'url_cv' => $rutaCV,
                            'observaciones' => $this->input->post("observaciones")
                        );
                        //Guardo el registro
                        if ($this->Candidatos_model->AddCV($info)) {
                            redirect(base_url() . "reclutamiento/candidatos/");
                        }
                    }
                }
            } else {
                if (move_uploaded_file($guardado, 'archivos/curriculums/' . $nombre . "_" . $apellido . ".pdf")) {
                    //Genero la ruta
                    $rutaCV = 'archivos/curriculums/' . $nombre . "_" . $apellido . ".pdf";
                    $info = array(
                        'nombre' =>  $nombreConEspacios,
                        'apellido' => $apellidoConEspacios,
                        'sexo' => $this->input->post("sexo"),
                        'fecha_nacimiento' => $this->input->post("fecha_nacimiento"),
                        'fecha_cv' => $this->input->post('fecha_cv'),
                        'puesto' => $this->input->post("puesto"),
                        'telefono' => $this->input->post("telefono"),
                        'email' => $this->input->post("email"),
                        'url_cv' => $rutaCV,
                        'observaciones' => $this->input->post("observaciones")
                    );
                    //Guardo el archivo
                    if ($this->Candidatos_model->AddCV($info)) {
                        redirect(base_url() . "reclutamiento/candidatos/");
                    }
                }
            }
            //si no me cargaron un archivo pero si un registro
        } else {
            //No voy a tener una ruta
            $rutaCV = "";
            //Tampoco una fecha de subida de cv
            $fechaSub = null;
            $info = array(
                'nombre' =>  $nombreConEspacios,
                'apellido' => $apellidoConEspacios,
                'sexo' => $this->input->post("sexo"),
                'fecha_nacimiento' => $this->input->post("fecha_nacimiento"),
                'fecha_cv' => $fechaSub,
                'puesto' => $this->input->post("puesto"),
                'telefono' => $this->input->post("telefono"),
                'email' => $this->input->post("email"),
                'url_cv' => $rutaCV,
                'observaciones' => $this->input->post("observaciones")
            );
            //Guardo el registro sin apuntar a un archivo
            if ($this->Candidatos_model->AddCV($info)) {
                redirect(base_url() . "reclutamiento/candidatos/");
            }
        }
    }
    public function UpdateCV()
    {
        //recibo el archivo y guardo su url temporal en $guardar
        $guardado = $_FILES['file']['tmp_name'];
        $id = $this->input->post('idCV');
        $url = $this->input->post('urlCV');
        $fecha = date('Y-m-d');
        $cv =  $this->Candidatos_model->GetCV($id);

        //tengo o no tengo url??
        if ($url != "") {
            //busco los datos para generar el nombre del archivo que estoy por subir(nombre y apellido)
            //me aseguro que exista la carpeta donde quiero guardar dichio archivo
            if (!file_exists('archivos/curriculums/')) {
                //si no existe la creo
                mkdir('archivos/curriculums/', 0777, true);
                //una vez creada, compruebo nuevamente que exista
                if (file_exists('archivos/curriculums/')) {
                    //borro el curriculum local
                    if (unlink($url->url_cv)) {
                        // guardar el nuevo CV
                        if (move_uploaded_file($guardado, 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf")) {
                            //genero su url
                            $rutaCV = 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf";
                            $info = array(
                                'fecha_cv' => $fecha,
                                'url_cv' => $rutaCV,
                            );
                            //actualizamos datos y redireccionamos
                            if ($this->Candidatos_model->UpdateCV($id, $info)) {
                                redirect(base_url() . "reclutamiento/candidatos/");
                            }
                        }
                        //si por alguna razon no lo puede borrar, ya que la url si existe, entonces lo crea 
                        //porque al no encontrarlo supongo que no existe...
                    } else {
                        //guardar el CV
                        if (move_uploaded_file($guardado, 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf")) {
                            //genero su url
                            $rutaCV = 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf";
                            $info = array(
                                'fecha_cv' => $fecha,
                                'url_cv' => $rutaCV,
                            );
                            //actualizamos datos y redireccionamos
                            if ($this->Candidatos_model->UpdateCV($id, $info)) {
                                redirect(base_url() . "reclutamiento/candidatos/");
                            }
                        }
                    }
                }
                //si la carpeta existe...
            } else {
                // borro el archivo
                if (unlink($url)) {
                    if (move_uploaded_file($guardado, 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf")) {
                        $rutaCV = 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf";
                        $info = array(
                            'fecha_cv' => $fecha,
                            'url_cv' => $rutaCV,
                        );
                        //actualizamos datos y redireccionamos
                        if ($this->Candidatos_model->UpdateCV($id, $info)) {
                            redirect(base_url() . "reclutamiento/candidatos/");
                        }
                    }
                    //si por alguna razon no lo puede borrar, ya que la url si existe, entonces lo crea 
                    //porque al no encontrarlo supongo que no existe...
                } else {
                    //guardar el CV
                    if (move_uploaded_file($guardado, 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf")) {
                        //genero su url
                        $rutaCV = 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf";
                        $info = array(
                            'fecha_cv' => $fecha,
                            'url_cv' => $rutaCV,
                        );
                        //actualizamos datos y redireccionamos
                        if ($this->Candidatos_model->UpdateCV($id, $info)) {
                            redirect(base_url() . "reclutamiento/candidatos/");
                        }
                    }
                }
            }
            //si la url esta vacia, es porque no existe el archivo local, por lo tanto lo doy de alta.
        } else {
            $cv =  $this->Candidatos_model->GetCV($id);
            //guardar el CV
            if (move_uploaded_file($guardado, 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf")) {
                //genero su url
                $rutaCV = 'archivos/curriculums/' . $cv->nombre . "_" . $cv->apellido . ".pdf";
                $info = array(
                    'fecha_cv' => $fecha,
                    'url_cv' => $rutaCV,
                );
                //actualizamos datos y redireccionamos
                if ($this->Candidatos_model->UpdateCV($id, $info)) {
                    redirect(base_url() . "reclutamiento/candidatos/");
                }
            }
        }
    }
    public function Edit($id)
    {
        $info = array(
            "curriculum" => $this->Candidatos_model->GetCV($id)
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('candidatos/edit', $info);
        $this->load->view('layouts/footer');
    }
    public function updateDatosCV()
    {
        $id = $this->input->post('idCvToUpdate');
        $info = array(
            'nombre' =>  $this->input->post("nombre"),
            'apellido' => $this->input->post("apellido"),
            'sexo' => $this->input->post("sexo"),
            'fecha_nacimiento' => $this->input->post("fecha_nacimiento"),
            'puesto' => $this->input->post("puesto"),
            'telefono' => $this->input->post("telefono"),
            'email' => $this->input->post("email"),
            'observaciones' => $this->input->post("observaciones")
        );
        if ($this->Candidatos_model->UpdateCV($id, $info)) {
            redirect(base_url() . "reclutamiento/candidatos/");
        }
    }
    public function deleteCV($id)
    {
        $url = $this->Candidatos_model->GetRutaLocalCV($id);
        // si el archivo existe
        if (file_exists($url->url_cv)) {
            //intenta borrarlo
            if (unlink($url->url_cv)) {
                //si borró el archivo local, ahora borro el registro de la base ded atos
                $this->Candidatos_model->DeleteCV($id);
                redirect(base_url() . "reclutamiento/candidatos/");
            }
            //si el cv no existe localmente
        } else if (!file_exists($url->url_cv)) {
            //borro el registro de curriculum de la base de datos
            if ($this->Candidatos_model->DeleteCV($id)) {
                //nunca se le subio el archivo(curriculum) pero si existia el registro
                redirect(base_url() . "reclutamiento/candidatos/");
            }
        }
    }
}
