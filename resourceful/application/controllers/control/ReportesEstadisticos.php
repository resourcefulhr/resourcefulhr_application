<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ReportesEstadisticos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!$this->session->userdata('login')) {
            $this->load->view('login');
            return;
        }

        $this->load->model("Novedades_model");

        $info = array(
            "legajos" => $this->Novedades_model->GetLegajos()
        );

        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('control/ReportesEstadisticos', $info);
        $this->load->view('layouts/footer');
    }

    public function IndiceInasistencia()
    {
        $this->load->model("Novedades_model");

        $legajos_input = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $dias_laboles = 0;
        $desde_datetime = DateTime::createFromFormat('Y-m-d', $desde);
        $hasta_datetime = DateTime::createFromFormat('Y-m-d', $hasta);

        $legajos = $this->Novedades_model->GetLegajosConNumero($legajos_input);
        while ($desde_datetime <= $hasta_datetime) {
            foreach ($legajos as $legajo) {
                $dias_laboles = $this->isDiaLaboral($legajo, $desde_datetime->format("Y-m-d")) ? $dias_laboles + 1 : $dias_laboles;
            }
            $desde_datetime = $desde_datetime->add(new DateInterval('P1D'));
        }

        $inasistencias = $this->Novedades_model->GetNovedades(array("INASISTENCIA"), NULL, $legajos_input, $desde, $hasta);
        $horario_inconsistente = $this->Novedades_model->GetNovedades(array("HORARIO INCONSISTENTE"), NULL, $legajos_input, $desde, $hasta);

        $info = array(
            "dias_laborales" => $dias_laboles,
            "inasistencias" => count($inasistencias),
            "horario_inconsistente" => count($horario_inconsistente)
        );
        echo json_encode($info);
    }

    private function isDiaLaboral($legajo, $dia)
    {
        $this->load->model("Legajos_model");
        $this->load->model("VacacionesLicencias_model");
        $this->load->model("CalendarioFeriados_model");
        $this->load->model("Novedades_model");

        if ($this->CalendarioFeriados_model->isFeriado($dia)) {
            return false;
        }

        $vacacion_licencia = $this->VacacionesLicencias_model->GetEventosConFecha($legajo->num_legajo, $dia, $dia);
        if ($vacacion_licencia) {
            return false;
        }

        $fecha_ingreso_legajo = DateTime::createFromFormat('Y-m-d', $legajo->fecha_ingreso);
        $hoy = DateTime::createFromFormat('Y-m-d', $dia);
        if ($hoy < $fecha_ingreso_legajo) {
            return false;
        }

        $dia_de_la_semana = $this->traducirDia($hoy->format("l"));
        $ingreso_teorico = current($this->Novedades_model->GetIngresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
        $egreso_teorico = current($this->Novedades_model->GetEgresoTeorico($dia_de_la_semana, $legajo->id_franja_horaria));
        if (($ingreso_teorico == NULL) && ($egreso_teorico == NULL)) {
            return false;
        }

        return true;
    }

    private function traducirDia($dia)
    {
        switch ($dia) {
            case "Monday":
                return "lunes";
            case "Tuesday":
                return "martes";
            case "Wednesday":
                return "miercoles";
            case "Thursday":
                return "jueves";
            case "Friday":
                return "viernes";
            case "Saturday":
                return "sabado";
            case "Sunday":
                return "domingo";
        }
    }

    public function DistribucionNovedades()
    {
        $this->load->model("Novedades_model");

        $legajos_input = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $distribucion = $this->Novedades_model->GetDistribucionNovedades($legajos_input, $desde, $hasta);
        echo json_encode($distribucion);
    }

    public function EstadoDeNovedades()
    {
        $this->load->model("Novedades_model");

        $legajos_input = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $estados = $this->Novedades_model->GetEstadosPorPeriodo($legajos_input, $desde, $hasta);
        echo json_encode($estados);
    }

    public function IndiceJustificacion()
    {
        $this->load->model("Novedades_model");

        $tipo = $this->input->post("tipo");
        $legajos_input = json_decode($this->input->POST("legajos"));
        $desde = $this->input->POST("desde");
        $hasta = $this->input->POST("hasta");

        $estados = $this->Novedades_model->IndiceJustificacion($tipo, $legajos_input, $desde, $hasta);
        echo json_encode($estados);
    }
}
