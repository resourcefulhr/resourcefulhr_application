<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Bienvenido a Resourceful</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>/front/images/favicon.png" type="image/png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(); ?>front/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(); ?>front/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url(); ?>front/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url(); ?>front/css/style.css" rel="stylesheet">
</head>

<body class="fp-page login-bg">
    <div class="fp-box">
        <div class="logo">
            <a href="#">Bienvendidos a <br><b>Resourceful</b></a>
            <small>sistema de gestion de recursos humanos</small>
        </div>
        <div class="card">
            <div class="body">
                <div class="msg">Recuperar Password</div>
                <div class="input-group" id="div-usuario">
                    <div class="form-line focused">
                        <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario">
                    </div>

                    <div id="error-usuario"></div>

                    <div class="input-group">
                        <button class="btn btn-block btn-login waves-effect" onclick="validateUser()"><b>Siguiente</b></button>
                    </div>
                </div>

                <form id="form-pregunta" style="display:none;" method="POST">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">loop</i>
                        </span>
                        <div class="form-line focused">
                            <input type="text" class="form-control" id="pregunta" name="pregunta" disabled>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">question_answer</i>
                        </span>
                        <div class="form-line focused">
                            <input type="text" class="form-control" id="respuesta" name="respuesta" placeholder="Respuesta" autofocus>
                        </div>

                        <div id="error-respuesta"></div>
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line focused">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line focused">
                            <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Confirmar contraseña" autofocus>
                        </div>
                    </div>

                    <div class="input-group">
                        <button class="btn btn-block btn-login waves-effect" type="submit"><b>Cambiar Contraseña</b></button>
                    </div>
                </form>

                <div class="m-t-25 m-b--5 align-center">
                        <a href="<?php echo base_url(); ?>login">Ya tengo una cuenta</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(); ?>front/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(); ?>front/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(); ?>front/js/admin.js"></script>
    <script src="<?php echo base_url(); ?>front/js/validation-rules/forgot-password.js"></script>
</body>

<script>
function validateUser() {
    var base_url = "<?php echo base_url(); ?>";
    $.ajax({
        url: base_url + "Login/GetPregunta",
        data: {
            usuario: $("#usuario").val()
        },
        type: "POST",
        success: function(result) {
            if (result) {
                $("#pregunta").val(result);

                $("#div-usuario").hide();
                $("#form-pregunta").show();
            } else {
                $("#usuario").parents('.form-line').addClass('error focused');
                $("#error-usuario").html('<label class="error" for="usuario">Nombre de usuario incorrecto.</label>');
            }
        }
    });
}

$(function() {
    $("#form-pregunta").submit(function(e) {
        if (e.isDefaultPrevented()) {
            return;
        }

        e.preventDefault(); // avoid to execute the actual submit of the form.

        var base_url = "<?php echo base_url(); ?>";
        $.ajax({
            url: base_url + "Login/RecuperarPass",
            data: {
                usuario: $("#usuario").val(),
                respuesta: $("#respuesta").val(),
                password: $("#password").val()
            },
            type: "POST",
            success: function(result) {
                if (result) {
                    window.location.replace(base_url);
                } else {
                    $("#respuesta").parents('.form-line').addClass('error focused');
                    $("#error-respuesta").html('<label class="error" for="respuesta">Respuesta de recuperacion incorrecta.</label>');
                }
            }
        });
    });
});
</script>
</html>