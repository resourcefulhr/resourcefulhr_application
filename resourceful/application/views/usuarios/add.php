<section class="content">
    <h1>
        Usuarios
        <small>Nuevo</small>
    </h1>
    <div class="card">
        <div class="body">
            <form class="form-line" action="<?php echo base_url(); ?>seguridad/usuarios/Store" method="POST">
                <div class="form-group">
                    <div class="form-line focused<?php echo form_error('legajo') == true ? ' error' : '' ?>">
                        <label>Legajo:</label>
                        <input type="number" name="legajo" class="form-control" required value="<?php echo set_value("legajo"); ?>">
                    </div>
                    <?php echo form_error("legajo", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <label>Contraseña:</label>
                    <div class="form-line focused">
                        <input type="text" name="password" class="form-control" required value="<?php echo set_value("password"); ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label>Roles:</label>
                    <select name="rol" id="rol" class="form-control" required>
                        <option value="">Seleccione...</option>
                        <?php foreach ($roles as $rol) : ?>
                            <option value="<?php echo $rol->id ?>" <?php echo set_select("rol", $rol->id) ?>>
                                <?php echo $rol->nombre; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_error("nombre", "<span class='help-block'>", "</span>"); ?>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success waves-effect" value="Guardar"><i class="material-icons">done_all</i> Guardar</button>
                    <a href="<?php echo base_url(); ?>seguridad/usuarios" class="btn btn-danger  waves-effect pull-right"><i class=material-icons>cancel
                        </i> Cancelar</a>
                </div>
            </form>

        </div>
    </div>
</section>